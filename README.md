# PA165 - Movie Recommendation Project

## About Project

Our team project in Java Spring Boot creates a set of microservices that handle movie recommendation.
Each microservice performs a specific function such as recommending movies based on genre, user
preferences or ratings. The microservices making the system scalable and easy to maintain.

### Developers:

    - Daniel Múčka (@xmucka) - Team leader
    - Petr Wehrenberg (@xwehrenb)
    - Vojtěch Malý (@xmaly1)
    - Marek Mišík (@xmisik)

### Assignment:

The web application is a catalogue of movies of different genres. Each movie has a title, description, 
film director, and list of actors, and image(s) from the main playbill. Each movie is categorized in 
one or more genres. Only the administrator can create, remove and update the list of movies. Users 
can rate the movies according to different criteria (e.g how novel are the ideas of the movie, their 
final score, etc…). The main feature of the system is that users can pick one movie and get the list
of similar movies and / or movies that were liked the most by other users watching the same movie 
(no need of complex algorithms, some simple recommendation is enough!).

## Project structure

Project contains modules as microservices.

### module-movie

`mvn -pl module-movie spring-boot:run`

Description: This module manages the movie data and provides information such 
as movie titles, genres, and release dates.


### module-rating

`mvn -pl module-rating spring-boot:run`

Description: This module handles the user rating data and stores it in a database.
It provides an interface to rate movies.


### module-recommendation

`mvn -pl module-recommendation spring-boot:run`

Description: This module contains the algorithms responsible for generating movie recommendations.
It uses user rating and movie data from other modules to create personalized recommendations for each user.


### module-user

`mvn -pl module-user spring-boot:run`

Description: This module manages user data such as name, email, and password.
It provides an interface for user registration, login, and authentication. It communicates with other modules.

## Keycloak setup

To access the authentication server, you need to have setup the following DNS route in your host machine:
```
127.0.0.1 keycloak
```

For example, in Windows add this line into `C:\Windows\System32\drivers\etc\hosts` and then run `ipconfig /flushdns`.
On Linux, edit the `/etc/hosts` file and flush your DNS records.

Keycloak then runs on `http://keycloak:8080/auth`.

## Authentication and authorization flow

Keycloak is an open-source identity and access management solution that allows you to manage user authentication
and authorization for your applications. The setup is automatically imported in the initialization phase but to 
test out application you need to add new user. To create a new user in Keycloak, follow the steps below:

1. Access the Keycloak Administration Console: Open your web browser and enter the URL for your Keycloak instance
followed by /auth/admin. For example, http://localhost:8080/auth/admin. Provide your administrator credentials to
log in.
2. Keycloak organizes users and applications into realms. Choose the realm JavaProject to create the new user from
the drop-down menu on the top left corner of the page. The default realm is usually named "Master."
3. Navigate to the Users Section: In the left-hand navigation menu, click on "Users" to open the user 
management section. 
4. Click on "Add User": On the "Users" page, click on the "Add User" button to start creating a new user. 
5. Enter User Details: Provide the necessary details for the new user, including the username, email, and password.
Please provide here at least email, username password and first name with last name to be able to register
to the application.
You can also configure additional user attributes, such as first name, last name, etc., as per your requirements.
6. Define User Roles: Roles control the user's access to different resources or features within your applications. 
To assign roles, click on the "Role Mappings" tab and select the desired roles from the available options. 
You can choose the `app-admin` or `app-user`. 
7. Set User Credentials: If you want to manually set the user's password, switch to the "Credentials" tab. Here,
you can either choose to set a temporary password or update the password for the user. Keycloak also provides options
for password policies and password expiration. 

Congratulations! You have successfully created a new user in Keycloak. You can now use this user to authenticate
and authorize access to your applications.


## Podman deployment
For manual deployment, you need to have [Podman](https://podman.io/) installed on your machine.
1. Make sure you have **built all the modules** using `mvn clean install` command in the root directory of the project.
2. Pull the Temurin image using `podman pull registry.hub.docker.com/library/eclipse-temurin:17-jdk-alpine` command.
3. Pull the Prometheus image using `podman pull docker.io/prom/prometheus` command.
4. Pull the Grafana image using `podman pull docker.io/grafana/grafana` command.
5. Run everything using `podman-compose up --build` command in the root directory of the project.

**NEW** Since complexity of the project increased, we decided to include scripts for easier automated deployment of the project.
For building and running the project, you need to have [Podman](https://podman.io/) installed on your machine (Docker for Windows).
Then just run `./build-and-run.sh` script in the root directory of the project.
In case of Windows, use `./build-and-run.ps1`.  
**Disclaimer:** This script works only to demonstrate prometheus and grafana functionality. Not to run the whole project. For this follow other steps in this README.

## Scenario, Locust
More info in the `locust` folder.

## Service port list
- user-module - 8090
- movie-module - 8089
- rating-module - 8087
- recommendation-module - 8085
- prometheus - 9090
- grafana - 3000
- keycloak - 8080

## Grafana
Grafana is available on `localhost:3000` with credentials `admin:admin`.
Main (and only one) provided dashboard is available on [main dashboard](http://localhost:3000/d/c524dcd5-fa74-4885-98b9-e11fd0fb7b65/main-dashboard?orgId=1).

###### Manual deployment only of services
Before you start, make sure you have **built all the modules** using `mvn clean install` command and 
pulled the Temurin image using `podman pull registry.hub.docker.com/library/eclipse-temurin:17-jdk-alpine` command.

Build all the images using these commands executed in the root directory of the project:
```
podman build -t pa165-movie-recommendation-project/module-user ./module-user
podman build -t pa165-movie-recommendation-project/module-movie ./module-movie
podman build -t pa165-movie-recommendation-project/module-recommendation ./module-recommendation
podman build -t pa165-movie-recommendation-project/module-rating ./module-rating
```

OR Build and run all the containers using command:
```
podman-compose up --build
```
Alternatively, exchange `podman` to `docker` where applicable.

## Diagrams
### Use case

![Use case diagram](docs/usecase.png)


### Component diagram

![Use case diagram](docs/component-diagram.png)
