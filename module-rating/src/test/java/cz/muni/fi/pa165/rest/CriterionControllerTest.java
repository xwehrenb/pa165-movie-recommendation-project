package cz.muni.fi.pa165.rest;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.containing;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.authentication;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import cz.muni.fi.pa165.data.api.user.AuthResponse;
import cz.muni.fi.pa165.data.api.user.UserViewDto;
import cz.muni.fi.pa165.data.model.auth.Role;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;

import java.time.Instant;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
class CriterionControllerTest {



    @Autowired
    private MockMvc mockMvc;



    @Test
    void getAllTest() throws Exception {
        this.mockMvc.perform(get("/api/criterion"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json("""
                        [
                        	{
                        		"id": 1,
                        		"name": "Criterion 1",
                        		"description": "Description 1"
                        	},
                        	{
                        		"id": 2,
                        		"name": "Criterion 2",
                        		"description": "Description 2"
                        	},
                        	{
                        		"id": 3,
                        		"name": "Criterion 3",
                        		"description": "Description 3"
                        	}
                        ]
                        """));
    }

    @Test
    void getOneTest() throws Exception {
        this.mockMvc.perform(get("/api/criterion/1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json("""
                        {
                         	"id": 1,
                         	"name": "Criterion 1",
                         	"description": "Description 1"
                         }
                        """));
    }

    @Test
    void addTest() throws Exception {
        this.mockMvc.perform(post("/api/criterion")
                        .with(authentication(produceAdminUser()))
                        .content("""
                        {
                          "name": "Acting",
                          "description": "Acting of the actors"
                        }
                        """)
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json("""
                        {
                            "id": 4,
                            "name": "Acting",
                            "description": "Acting of the actors"
                        }
                        """));
    }

    @Test
    void deleteTest() throws Exception {
        this.mockMvc.perform(
                delete("/api/criterion/1")
                        .with(authentication(produceAdminUser()))
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json("""
                        {
                        	"id": 1,
                        	"name": "Criterion 1",
                        	"description": "Description 1"
                        }
                        """));
    }

    @Test
    void editTest() throws Exception {
        this.mockMvc.perform(
                patch("/api/criterion")
                        .with(authentication(produceAdminUser()))
                        .content("""
                        {
                          "id": 1,
                          "name": "Sound effects",
                          "description": "Various sound effects"
                        }
                        """)
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json("""
                        {
                          "id": 1,
                          "name": "Sound effects",
                          "description": "Various sound effects"
                        }
                        """));
    }
    private JwtAuthenticationToken produceAdminUser() throws Exception {
        String value = "placeholder";
        Instant issuedAt = Instant.now();
        Instant expiresAt = issuedAt.plusSeconds(3600);

        Map<String, Object> headers = new HashMap<>();
        headers.put("alg", "HS256");
        headers.put("typ", "JWT");

        Map<String, Object> claims = new HashMap<>();
        claims.put("sub", "0e6f108c-ec1e-11ed-a05b-0242ac120003");
        claims.put("custom_claim", "custom_value");
        claims.put("typ", "Bearer");
        claims.put("given_name", "John");
        claims.put("family_name", "Watson");
        claims.put("name", "john.watson");
        claims.put("email", "john.watson@doctor.com");
        claims.put("scope", "email profile");
        Jwt jwt = new Jwt(value, issuedAt, expiresAt, headers, claims);

        Collection<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_app-admin"); // Add the required authorities
        return new JwtAuthenticationToken(jwt, authorities);
    }

}
