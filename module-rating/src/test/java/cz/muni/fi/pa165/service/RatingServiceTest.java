package cz.muni.fi.pa165.service;

import cz.muni.fi.pa165.data.model.Criterion;
import cz.muni.fi.pa165.data.model.Movie;
import cz.muni.fi.pa165.data.model.Rating;
import cz.muni.fi.pa165.data.model.User;
import cz.muni.fi.pa165.data.repository.CriterionRepository;
import cz.muni.fi.pa165.data.repository.MovieRepository;
import cz.muni.fi.pa165.data.repository.RatingRepository;
import cz.muni.fi.pa165.data.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.Mockito;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

public class RatingServiceTest {

    private RatingService ratingService;

    private RatingRepository ratingRepository;
    private MovieRepository movieRepository;
    private UserRepository userRepository;
    private CriterionRepository criterionRepository;
    private MovieApiService movieApiService;

    @BeforeEach
    public void setup() {
        ratingRepository = Mockito.mock(RatingRepository.class);
        movieRepository = Mockito.mock(MovieRepository.class);
        userRepository = Mockito.mock(UserRepository.class);
        criterionRepository = Mockito.mock(CriterionRepository.class);

        // mock movie api service
        movieApiService = Mockito.mock(MovieApiService.class);
        when(movieApiService.findById(anyInt())).thenReturn(Optional.of(new Movie()));

        ratingService = new RatingService(ratingRepository, criterionRepository, movieRepository, userRepository, movieApiService);
    }

    @Test
    public void getAllByCriteriaTest() {
        UUID uuid = UUID.fromString("752e2652-ecc1-11ed-a05b-0242ac120003");
        when(ratingRepository.findAllByUserIdAndMovieIdAndCriterionId(any(), any(), any(), any()))
                .thenReturn(new PageImpl<>(List.of(getRating(10, -1, -1, uuid))));

        var found = ratingService.getAllByCriteria(Optional.of(uuid), Optional.of(-1), Optional.of(-1), Pageable.unpaged());

        assertEquals(1, found.getTotalElements());
        assertEquals(-1, found.getContent().get(0).getCriterion().getId());
        assertEquals(-1, found.getContent().get(0).getMovie().getId());
        assertEquals(uuid, found.getContent().get(0).getUser().getUuid());
        assertEquals(10, found.getContent().get(0).getValue());
    }

    @Test
    public void createTest() {
        UUID uuid = UUID.fromString("752e2652-ecc1-11ed-a05b-0242ac120003");
        when(userRepository.findById(any())).thenReturn(Optional.of(new User("752e2652-ecc1-11ed-a05b-0242ac120003") {
            @Override
            public UUID getUuid() {
                return uuid;
            }
        }));

        when(movieRepository.findById(any())).thenReturn(Optional.of(new Movie() {
            @Override
            public int getId() {
                return -1;
            }
        }));

        when(criterionRepository.findById(any())).thenReturn(Optional.of(new Criterion() {
            @Override
            public int getId() {
                return -1;
            }
        }));

        when(ratingRepository.findByUserIdAndMovieIdAndCriterionId(any(UUID.class), anyInt(), anyInt()))
                .thenReturn(Optional.empty());
        when(ratingRepository.save(any()))
                .thenReturn(getRating(10, -1, -1, uuid));

        var rating = ratingService.upsert(getRating(10, -1, -1, uuid), uuid);

        assertEquals(-1, rating.getCriterion().getId());
        assertEquals(-1, rating.getMovie().getId());
        assertEquals(uuid, rating.getUser().getUuid());
        assertEquals(10, rating.getValue());
    }

    @Test
    public void updateTest() {
        UUID uuid = UUID.fromString("752e2652-ecc1-11ed-a05b-0242ac120003");

        when(userRepository.findById(any())).thenReturn(Optional.of(new User("752e2652-ecc1-11ed-a05b-0242ac120003") {
            @Override
            public UUID getUuid() {
                return uuid;
            }
        }));

        when(movieRepository.findById(any())).thenReturn(Optional.of(new Movie() {
            @Override
            public int getId() {
                return -1;
            }
        }));

        when(criterionRepository.findById(any())).thenReturn(Optional.of(new Criterion() {
            @Override
            public int getId() {
                return -1;
            }
        }));

        when(ratingRepository.findByUserIdAndMovieIdAndCriterionId(any(UUID.class), anyInt(), anyInt()))
                .thenReturn(Optional.of(getRating(5, -1, -1, uuid)));
        when(ratingRepository.save(any()))
                .thenReturn(getRating(10, -1, -1, uuid));

        var rating = ratingService.upsert(getRating(10, -1, -1, uuid), uuid);

        assertEquals(-1, rating.getCriterion().getId());
        assertEquals(-1, rating.getMovie().getId());
        assertEquals(uuid, rating.getUser().getUuid());
        assertEquals(10, rating.getValue());
    }

    @Test
    public void deleteTest() {
        UUID uuid = UUID.fromString("752e2652-ecc1-11ed-a05b-0242ac120003");
        when(ratingRepository.findByUserIdAndMovieIdAndCriterionId(any(UUID.class), anyInt(), anyInt()))
                .thenReturn(Optional.of(getRating(10, -1, -1, uuid)));

        var rating = ratingService.delete( UUID.randomUUID(), -1, -1); //TODO

        assertEquals(-1, rating.getCriterion().getId());
        assertEquals(-1, rating.getMovie().getId());
        assertEquals(uuid, rating.getUser().getUuid());
        assertEquals(10, rating.getValue());
    }

    private Rating getRating(int value, int criterionId, int movieId, UUID userId) {
        var criterionMock = new Criterion();
        criterionMock.setId(criterionId);

        var movieMock = new Movie();
        movieMock.setId(movieId);

        var userMock = new User();
        userMock.setUuid(userId);

        return new Rating(value, criterionMock, movieMock, userMock);
    }
}