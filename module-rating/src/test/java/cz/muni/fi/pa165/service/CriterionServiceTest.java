package cz.muni.fi.pa165.service;

import cz.muni.fi.pa165.data.model.Criterion;
import cz.muni.fi.pa165.data.repository.CriterionRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

public class CriterionServiceTest {

    private CriterionService criterionService;

    private CriterionRepository criterionRepository;

    @BeforeEach
    public void setup() {
        criterionRepository = Mockito.mock(CriterionRepository.class);

        criterionService = new CriterionService(criterionRepository);
    }

    @Test
    public void getAllTest() {
        when(criterionRepository.findAll(any(Pageable.class)))
                .thenReturn(new PageImpl<>(List.of(getCriterion())));

        var found = criterionService.getAll(Pageable.unpaged());

        assertEquals(1, found.getTotalElements());
        assertEquals(-1, found.getContent().get(0).getId());
        assertEquals("Criterion", found.getContent().get(0).getName());
        assertEquals("Description", found.getContent().get(0).getDescription());
    }

    @Test
    public void getByIdTest() {
        when(criterionRepository.findById(anyInt()))
                .thenReturn(Optional.of(getCriterion()));

        var found = criterionService.getById(-1);

        assertEquals(-1, found.getId());
        assertEquals("Criterion", found.getName());
        assertEquals("Description", found.getDescription());
    }

    @Test
    public void createTest() {
        when(criterionRepository.save(any())).thenReturn(getCriterion());

        var created = criterionService.create(getCriterion());

        assertEquals(-1, created.getId());
        assertEquals("Criterion", created.getName());
        assertEquals("Description", created.getDescription());
    }

    @Test
    public void updateTest() {
        when(criterionRepository.findById(anyInt())).thenReturn(Optional.of(getCriterion()));

        Criterion toUpdate = new Criterion();
        toUpdate.setId(-1);
        toUpdate.setName("C");
        toUpdate.setDescription("D");
        when(criterionRepository.save(any())).thenReturn(toUpdate);

        var updated = criterionService.update(toUpdate);

        assertEquals(toUpdate.getId(), updated.getId());
        assertEquals(toUpdate.getName(), updated.getName());
        assertEquals(toUpdate.getDescription(), updated.getDescription());
    }

    @Test
    public void deleteTest() {
        when(criterionRepository.findById(anyInt())).thenReturn(Optional.of(getCriterion()));

        var deleted = criterionService.delete(-1);

        assertEquals(-1, deleted.getId());
        assertEquals("Criterion", deleted.getName());
        assertEquals("Description", deleted.getDescription());
    }

    private Criterion getCriterion() {
        Criterion criterion = new Criterion();
        criterion.setId(-1);
        criterion.setName("Criterion");
        criterion.setDescription("Description");
        return criterion;
    }
}
