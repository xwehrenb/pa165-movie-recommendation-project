package cz.muni.fi.pa165.rest;

import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.authentication;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import cz.muni.fi.pa165.data.api.movie.MovieResponseDto;
import cz.muni.fi.pa165.data.api.movie.MovieDto;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;

import java.time.Instant;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
class RatingControllerTest {

    @Autowired
    private MockMvc mockMvc;

    private static final WireMockServer movieServer = new WireMockServer(wireMockConfig().port(8089));

    @BeforeAll
    static void setupMock() {
        try {
            var objectMapper = new ObjectMapper();

            var dto = new MovieResponseDto();
            dto.setMessage("Success");
            var movieDto = new MovieDto();
            movieDto.setId(1L);
            dto.setItem(movieDto);
            String resp = objectMapper.writeValueAsString(dto);

            movieServer.stubFor(WireMock.get(WireMock.urlPathMatching("/api/movies/([0-9]*)"))
                    .willReturn(WireMock.aResponse()
                            .withHeader("Content-Type", "application/json")
                            .withBody(resp)));

            movieServer.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @AfterAll
    static void tearDownMock() {
        movieServer.stop();
    }

    @Test
    void getAllTest() throws Exception {
        this.mockMvc.perform(get("/api/rating"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json("""
                        [
                          	{
                          		"value": 1,
                          		"criterionId": 1,
                          		"movieId": 1,
                          		"userId": "9ef7470e-ecbf-11ed-a05b-0242ac120003"
                          	},
                          	{
                          		"value": 5,
                          		"criterionId": 2,
                          		"movieId": 2,
                          		"userId": "9ef7470e-ecbf-11ed-a05b-0242ac120004"
                          	},
                          	{
                          		"value": 10,
                          		"criterionId": 3,
                          		"movieId": 3,
                          		"userId": "9ef7470e-ecbf-11ed-a05b-0242ac120005"
                          	},
                          	{
                          		"value": 5,
                          		"criterionId": 2,
                          		"movieId": 1,
                          		"userId": "9ef7470e-ecbf-11ed-a05b-0242ac120003"
                          	},
                          	{
                          		"value": 6,
                          		"criterionId": 1,
                          		"movieId": 2,
                          		"userId": "9ef7470e-ecbf-11ed-a05b-0242ac120004"
                          	},
                          	{
                          		"value": 1,
                          		"criterionId": 3,
                          		"movieId": 3,
                          		"userId": "9ef7470e-ecbf-11ed-a05b-0242ac120005"
                          	},
                          	{
                          		"value": 6,
                          		"criterionId": 3,
                          		"movieId": 1,
                          		"userId": "9ef7470e-ecbf-11ed-a05b-0242ac120003"
                          	},
                          	{
                          		"value": 3,
                          		"criterionId": 2,
                          		"movieId": 2,
                          		"userId": "9ef7470e-ecbf-11ed-a05b-0242ac120004"
                          	},
                          	{
                          		"value": 0,
                          		"criterionId": 1,
                          		"movieId": 3,
                          		"userId": "9ef7470e-ecbf-11ed-a05b-0242ac120005"
                          	}
                          ]
                        """));
    }

    @Test
    void getAllByUserTest() throws Exception {
        this.mockMvc.perform(get("/api/rating?userId=9ef7470e-ecbf-11ed-a05b-0242ac120003"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json("""
                        [
                         	{
                         		"value": 1,
                         		"criterionId": 1,
                         		"movieId": 1,
                         		"userId": "9ef7470e-ecbf-11ed-a05b-0242ac120003"
                         	},
                         	{
                         		"value": 5,
                         		"criterionId": 2,
                         		"movieId": 1,
                         		"userId": "9ef7470e-ecbf-11ed-a05b-0242ac120003"
                         	},
                         	{
                         		"value": 6,
                         		"criterionId": 3,
                         		"movieId": 1,
                         		"userId": "9ef7470e-ecbf-11ed-a05b-0242ac120003"
                         	}
                         ]
                        """));
    }

    @Test
    void getAllByMovieTest() throws Exception {
        this.mockMvc.perform(get("/api/rating?movieId=1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json("""
                        [
                          	{
                          		"value": 1,
                          		"criterionId": 1,
                          		"movieId": 1,
                          		"userId": "9ef7470e-ecbf-11ed-a05b-0242ac120003"
                          	},
                          	{
                          		"value": 5,
                          		"criterionId": 2,
                          		"movieId": 1,
                          		"userId": "9ef7470e-ecbf-11ed-a05b-0242ac120003"
                          	},
                          	{
                          		"value": 6,
                          		"criterionId": 3,
                          		"movieId": 1,
                          		"userId": "9ef7470e-ecbf-11ed-a05b-0242ac120003"
                          	}
                          ]
                        """));
    }

    @Test
    void getAllByCriterionTest() throws Exception {
        this.mockMvc.perform(get("/api/rating?criterionId=1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json("""
                        [
                           	{
                           		"value": 1,
                           		"criterionId": 1,
                           		"movieId": 1,
                           		"userId": "9ef7470e-ecbf-11ed-a05b-0242ac120003"
                           	},
                           	{
                           		"value": 6,
                           		"criterionId": 1,
                           		"movieId": 2,
                           		"userId": "9ef7470e-ecbf-11ed-a05b-0242ac120004"
                           	},
                           	{
                           		"value": 0,
                           		"criterionId": 1,
                           		"movieId": 3,
                           		"userId": "9ef7470e-ecbf-11ed-a05b-0242ac120005"
                           	}
                           ]
                        """));
    }

    @Test
    void getAllByMovieCriterionUserTest() throws Exception {
        this.mockMvc.perform(get("/api/rating?userId=9ef7470e-ecbf-11ed-a05b-0242ac120003&movieId=1&criterionId=1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json("""
                        [
                         	{
                         		"value": 1,
                         		"criterionId": 1,
                         		"movieId": 1,
                         		"userId": "9ef7470e-ecbf-11ed-a05b-0242ac120003"
                         	}
                         ]
                        """));
    }

    @Test
    void deleteTest() throws Exception {
        this.mockMvc.perform(delete("/api/rating?userId=9ef7470e-ecbf-11ed-a05b-0242ac120003&movieId=1&criterionId=1")
                        .with(authentication(produceAdminUser())))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json("""
                        {
                           "value": 1,
                           "criterionId": 1,
                           "movieId": 1,
                           "userId": "9ef7470e-ecbf-11ed-a05b-0242ac120003"
                        }
                        """));
    }

    @Test
    void addTest() throws Exception {
        this.mockMvc.perform(put("/api/rating").content("""
                        {
                           "value": 5,
                           "criterionId": 1,
                           "movieId": 1
                        }
                        """)
                        .with(authentication(produceAdminUser()))
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json("""
                        {
                           "value": 5,
                           "criterionId": 1,
                           "movieId": 1,
                           "userId": "9ef7470e-ecbf-11ed-a05b-0242ac120003"
                         }
                        """));
    }

    @Test
    void editTest() throws Exception {
        this.mockMvc.perform(put("/api/rating").content("""
                        {
                           "value": 5,
                           "criterionId": 1,
                           "movieId": 1
                        }
                        """)
                        .with(authentication(produceAdminUser()))
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json("""
                        {
                           "value": 5,
                           "criterionId": 1,
                           "movieId": 1,
                           "userId": "9ef7470e-ecbf-11ed-a05b-0242ac120003"
                         }
                        """))
                .andDo(result -> this.mockMvc.perform(
                        put("/api/rating")
                                .content("""
                        {
                           "value": 2,
                           "criterionId": 1,
                           "movieId": 1
                        }
                        """)
                                .with(authentication(produceAdminUser()))
                                .contentType(MediaType.APPLICATION_JSON_VALUE))
                        .andDo(print())
                        .andExpect(status().isOk())
                        .andExpect(content().json("""
                        {
                           "value": 2,
                           "criterionId": 1,
                           "movieId": 1,
                           "userId": "9ef7470e-ecbf-11ed-a05b-0242ac120003"
                         }
                        """)));
    }

    private JwtAuthenticationToken produceAdminUser() throws Exception {
        String value = "placeholder";
        Instant issuedAt = Instant.now();
        Instant expiresAt = issuedAt.plusSeconds(3600);

        Map<String, Object> headers = new HashMap<>();
        headers.put("alg", "HS256");
        headers.put("typ", "JWT");

        Map<String, Object> claims = new HashMap<>();
        claims.put("sub", "9ef7470e-ecbf-11ed-a05b-0242ac120003");
        claims.put("custom_claim", "custom_value");
        claims.put("typ", "Bearer");
        claims.put("given_name", "John");
        claims.put("family_name", "Watson");
        claims.put("name", "john.watson");
        claims.put("email", "john.watson@doctor.com");
        claims.put("scope", "email profile");
        Jwt jwt = new Jwt(value, issuedAt, expiresAt, headers, claims);

        Collection<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_app-admin"); // Add the required authorities
        return new JwtAuthenticationToken(jwt, authorities);
    }

}
