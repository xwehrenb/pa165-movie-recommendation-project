package cz.muni.fi.pa165.data.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import org.hibernate.annotations.Cascade;

@Entity
public class Rating implements GenericEntity {

    public Rating() {
    }

    public Rating(int value, Criterion criterion, Movie movie, User user) {
        this.value = value;
        this.criterion = criterion;
        this.movie = movie;
        this.user = user;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @NotNull
    int id;

    @NotNull
    @Min(0)
    @Max(10)
    int value;

    @ManyToOne(cascade = CascadeType.REMOVE)
    Criterion criterion;

    @ManyToOne(cascade = CascadeType.REMOVE)
    Movie movie;

    @ManyToOne(cascade = CascadeType.REMOVE)
    User user;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public Criterion getCriterion() {
        return criterion;
    }

    public void setCriterion(Criterion criterion) {
        this.criterion = criterion;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
