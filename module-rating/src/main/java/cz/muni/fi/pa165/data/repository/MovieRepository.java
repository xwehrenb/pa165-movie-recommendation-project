package cz.muni.fi.pa165.data.repository;

import cz.muni.fi.pa165.data.model.Movie;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MovieRepository extends JpaRepository<Movie, Integer> {
}
