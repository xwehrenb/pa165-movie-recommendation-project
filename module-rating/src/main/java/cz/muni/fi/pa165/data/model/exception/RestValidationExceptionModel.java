package cz.muni.fi.pa165.data.model.exception;

import io.swagger.v3.oas.annotations.media.Schema;

import java.util.Dictionary;

public class RestValidationExceptionModel extends RestExceptionModel {

    @Schema(description = "Exception message", example = "Validation failed")
    String message;

    @Schema(description = "Validation errors", example = "{ \"value\": \"Value is required\" }")
    Dictionary<String, String> validation;

    public RestValidationExceptionModel(Dictionary<String, String> validation) {
        super("Validation failed");

        this.validation = validation;
    }

    public Dictionary<String, String> getValidation() {
        return validation;
    }

    public void setValidation(Dictionary<String, String> validation) {
        this.validation = validation;
    }
}
