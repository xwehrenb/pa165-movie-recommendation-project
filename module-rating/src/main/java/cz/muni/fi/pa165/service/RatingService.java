package cz.muni.fi.pa165.service;

import cz.muni.fi.pa165.data.exception.EntityNotFoundException;
import cz.muni.fi.pa165.data.exception.EntityValidationException;
import cz.muni.fi.pa165.data.model.Rating;
import cz.muni.fi.pa165.data.model.User;
import cz.muni.fi.pa165.data.repository.CriterionRepository;
import cz.muni.fi.pa165.data.repository.MovieRepository;
import cz.muni.fi.pa165.data.repository.RatingRepository;
import cz.muni.fi.pa165.data.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
public class RatingService {

    private final RatingRepository ratingRepository;
    private final CriterionRepository criterionRepository;
    private final MovieRepository movieRepository;
    private final UserRepository userRepository;
    private final MovieApiService movieApiService;

    @Autowired
    public RatingService(RatingRepository ratingRepository, CriterionRepository criterionRepository, MovieRepository movieRepository, UserRepository userRepository, MovieApiService movieApiService) {
        this.ratingRepository = ratingRepository;
        this.criterionRepository = criterionRepository;
        this.movieRepository = movieRepository;
        this.userRepository = userRepository;
        this.movieApiService = movieApiService;
    }

    public Page<Rating> getAllByCriteria(Optional<UUID> uuid, Optional<Integer> movieId, Optional<Integer> criterionId, Pageable pageable) {
        return ratingRepository.findAllByUserIdAndMovieIdAndCriterionId(uuid, movieId, criterionId, pageable);
    }

    public Rating upsert(Rating rating, UUID uuid) throws EntityNotFoundException, EntityValidationException {
        if (rating.getValue() < 0 || rating.getValue() > 10) {
            throw new EntityValidationException("Rating must be between 0 and 10.");
        }

        // check if user exists locally
        // if not, get the user from the authenticated request
        var user = userRepository.findById(uuid);
        if (user.isEmpty()) {
            var newUser = userRepository.save(new User(uuid));
            rating.setUser(newUser);
        } else {
            rating.setUser(user.get());
        }

        // check movie microservice, if movie exists
        if (movieApiService.findById(rating.getMovie().getId()).isEmpty()) {
            throw new EntityNotFoundException("Movie", rating.getMovie().getId());
        }

        // check local criterion repository, if criterion exists
        if (criterionRepository.findById(rating.getCriterion().getId()).isEmpty()) {
            throw new EntityNotFoundException("Criterion", rating.getCriterion().getId());
        }

        // if movie does not exist locally, save it
        if (movieRepository.findById(rating.getMovie().getId()).isEmpty()) {
            movieRepository.save(rating.getMovie());
        }

        Optional<Rating> exists = ratingRepository.findByUserIdAndMovieIdAndCriterionId(rating.getUser().getUuid(), rating.getMovie().getId(), rating.getCriterion().getId());

        exists.ifPresent(value -> rating.setId(value.getId()));

        return ratingRepository.save(rating);
    }

    public Rating delete(UUID userId, int movieId, int criterionId) throws EntityNotFoundException {
        Rating entity = ratingRepository.findByUserIdAndMovieIdAndCriterionId(userId, movieId, criterionId)
                .orElseThrow(() -> new EntityNotFoundException("Matching rating was not found."));

        ratingRepository.delete(entity);

        return entity;
    }
}
