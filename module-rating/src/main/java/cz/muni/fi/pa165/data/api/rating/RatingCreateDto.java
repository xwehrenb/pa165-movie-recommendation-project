package cz.muni.fi.pa165.data.api.rating;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;

public class RatingCreateDto {

    @NotNull
    @Min(0)
    @Max(10)
    @Schema(description = "Rating value", example = "5")
    int value;

    @NotNull
    @Schema(description = "Criterion ID", example = "1")
    int criterionId;

    @NotNull
    @Schema(description = "Movie ID", example = "1")
    int movieId;


    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getCriterionId() {
        return criterionId;
    }

    public void setCriterionId(int criterionId) {
        this.criterionId = criterionId;
    }

    public int getMovieId() {
        return movieId;
    }

    public void setMovieId(int movieId) {
        this.movieId = movieId;
    }
}
