package cz.muni.fi.pa165.service;

import cz.muni.fi.pa165.data.api.movie.MovieResponseDto;
import cz.muni.fi.pa165.data.exception.ApiServiceException;
import cz.muni.fi.pa165.data.model.Movie;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Objects;
import java.util.Optional;

@Service
public class MovieApiService {

    @Value("${services.movie.url}")
    private String url;

    @Value("${services.movie.port}")
    private String port;

    @Value("${services.movie.path}")
    private String path;

    public Optional<Movie> findById(int id) {
        var restTemplate = new RestTemplate();

        StringBuilder sb = new StringBuilder();
        sb.append(url).append(":").append(port).append(path).append("/").append(id);

        MovieResponseDto response;
        try {
            response = restTemplate.getForObject(sb.toString(), MovieResponseDto.class);
        } catch (Exception e) {
            throw new ApiServiceException("Movie microservice is not available.");
        }

        if (response == null) {
            throw new ApiServiceException("Movie microservice returned empty body");
        }

        if (!Objects.equals(response.getMessage(), "Success")) {
            throw new ApiServiceException("Movie microservice returned invalid response.");
        }

        var mapper = new ModelMapper();

        var parsed = mapper.map(response.getItem(), Movie.class);
        return Optional.of(parsed);
    }
}
