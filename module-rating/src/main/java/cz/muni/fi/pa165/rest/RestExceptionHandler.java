package cz.muni.fi.pa165.rest;

import cz.muni.fi.pa165.data.exception.EntityNotFoundException;
import cz.muni.fi.pa165.data.exception.EntityValidationException;
import cz.muni.fi.pa165.data.exception.ApiServiceException;
import cz.muni.fi.pa165.data.model.exception.RestExceptionModel;
import cz.muni.fi.pa165.data.model.exception.RestValidationExceptionModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.Dictionary;
import java.util.Hashtable;

@RestControllerAdvice
public class RestExceptionHandler {

    static final Logger logger = LoggerFactory.getLogger(RestExceptionHandler.class);

//    @ExceptionHandler(Exception.class)
//    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
//    public ResponseEntity<RestException> handleUnhandledException(Exception e) {
//        logger.error("Unhandled exception", e);
//        return new ResponseEntity<>(new RestException("Internal server error"), HttpStatus.INTERNAL_SERVER_ERROR);
//    }

    @ExceptionHandler(org.hibernate.query.SemanticException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<RestExceptionModel> handleQueryException(org.hibernate.query.SemanticException e) {
        logger.error("Unhandled exception", e);
        return new ResponseEntity<>(new RestExceptionModel("Query could not be parsed"), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(EntityNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity<RestExceptionModel> handleEntityNotFoundException(EntityNotFoundException e) {
        return new ResponseEntity<>(new RestExceptionModel(e.getMessage()), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(EntityValidationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<RestExceptionModel> handleEntityValidationException(EntityValidationException e) {
        return new ResponseEntity<>(new RestExceptionModel(e.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(jakarta.validation.ConstraintViolationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<RestValidationExceptionModel> handleConstraintViolationException(jakarta.validation.ConstraintViolationException e) {
        Dictionary<String, String> validation = new Hashtable<>();
        e.getConstraintViolations().forEach(violation -> {
            validation.put(violation.getPropertyPath().toString(), violation.getMessage());
        });
        return new ResponseEntity<>(new RestValidationExceptionModel(validation), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ApiServiceException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity<RestExceptionModel> handleMovieNotFoundException(ApiServiceException e) {
        return new ResponseEntity<>(new RestExceptionModel(e.getMessage()), HttpStatus.NOT_FOUND);
    }
}
