package cz.muni.fi.pa165.data.exception;

public class EntityValidationException extends RuntimeException {
    public EntityValidationException() {
        super("Entity validation failed");
    }

    public EntityValidationException(String message) {
        super(message);
    }
}
