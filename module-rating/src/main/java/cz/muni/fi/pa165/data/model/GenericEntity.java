package cz.muni.fi.pa165.data.model;

public interface GenericEntity {

        int getId();
        void setId(int id);
}
