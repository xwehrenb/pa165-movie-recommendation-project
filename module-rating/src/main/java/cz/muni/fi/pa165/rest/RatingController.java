package cz.muni.fi.pa165.rest;

import cz.muni.fi.pa165.data.api.rating.RatingCreateDto;
import cz.muni.fi.pa165.data.api.rating.RatingViewDto;
import cz.muni.fi.pa165.data.model.Rating;
import cz.muni.fi.pa165.data.model.User;
import cz.muni.fi.pa165.service.RatingService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.modelmapper.ModelMapper;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping(path = "/api/rating", produces = MediaType.APPLICATION_JSON_VALUE)
public class RatingController {

    private final RatingService ratingService;
    private final ModelMapper modelMapper;

    @Autowired
    public RatingController(RatingService ratingService, ModelMapper modelMapper) {
        this.ratingService = ratingService;
        this.modelMapper = modelMapper;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    @Operation(
            summary = "Find ratings",
            description = "Gets all ratings according to the specified query parameters.",
            parameters = {
                    @Parameter(name = "userId", description = "User ID", example = "", allowEmptyValue = true),
                    @Parameter(name = "movieId", description = "Movie ID", example = "1", allowEmptyValue = true),
                    @Parameter(name = "criterionId", description = "Criterion ID", example = "1", allowEmptyValue = true)
            },
            responses = {
                    @ApiResponse(responseCode = "200", description = "Ratings found")
            },
            tags = {"Rating"}
    )
    public ResponseEntity<List<RatingViewDto>> getAll(@RequestParam Optional<UUID> userId, @RequestParam Optional<Integer> movieId, @RequestParam Optional<Integer> criterionId, @ParameterObject Pageable pageable) {
        Page<Rating> ratings = ratingService.getAllByCriteria(userId, movieId, criterionId, pageable);
        List<RatingViewDto> view = ratings.getContent().stream().map(rating -> {
            RatingViewDto dto = modelMapper.map(rating, RatingViewDto.class);
            dto.setUserId(rating.getUser().getUuid());
            return dto;
        }).toList();
        return ResponseEntity.ok(view);
    }

    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    @Operation(
            summary = "Create or update a rating",
            description = "Create a new rating with specified value or edit existing one.",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Rating created or updated"),
                    @ApiResponse(responseCode = "400", description = "Rating value is not between 0 and 10, user, movie or criterion does not exist")
            },
            tags = {"Rating"}
    )
    @SecurityRequirement(name = "OAuth2")
    public ResponseEntity<RatingViewDto> add(@RequestBody RatingCreateDto rating) {
        var uuid = getUUID();
        Rating entity = modelMapper.map(rating, Rating.class);

        Rating updated = ratingService.upsert(entity, uuid);
        RatingViewDto view = modelMapper.map(updated, RatingViewDto.class);
        view.setUserId(updated.getUser().getUuid());

        return ResponseEntity.ok(view);
    }

    @DeleteMapping
    @ResponseStatus(HttpStatus.OK)
    @Operation(
            summary = "Delete rating",
            description = "Delete a specific rating.",
            parameters = {
                    @Parameter(name = "movieId", description = "Movie ID", example = "1"),
                    @Parameter(name = "criterionId", description = "Criterion ID", example = "1")
            },
            responses = {
                    @ApiResponse(responseCode = "200", description = "Rating deleted"),
                    @ApiResponse(responseCode = "404", description = "Rating not found")
            },
            tags = {"Rating"}
    )
    @SecurityRequirement(name = "OAuth2")
    public ResponseEntity<RatingViewDto> delete(@RequestParam int movieId, @RequestParam int criterionId) {
        var uuid = getUUID();
        Rating entityToDelete = ratingService.delete(uuid, movieId, criterionId);

        RatingViewDto view = modelMapper.map(entityToDelete, RatingViewDto.class);
        view.setUserId(entityToDelete.getUser().getUuid());

        return ResponseEntity.ok(view);
    }

    private UUID getUUID() {
        JwtAuthenticationToken token = (JwtAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
        String stringUuid = token.getTokenAttributes().get("sub").toString();
        return UUID.fromString(stringUuid);
    }
}
