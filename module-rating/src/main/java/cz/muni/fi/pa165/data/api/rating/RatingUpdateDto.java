package cz.muni.fi.pa165.data.api.rating;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;

public class RatingUpdateDto extends RatingCreateDto {

    @NotNull
    @Schema(description = "Rating ID", example = "1")
    int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
