package cz.muni.fi.pa165.data.api.user;

import cz.muni.fi.pa165.data.model.auth.Role;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;

public class UserViewDto {

    @NotBlank
    @Schema(description = "Id", example = "1")
    int id;

    @NotBlank
    @Schema(description = "User name", example = "John Doe")
    String name;

    @NotBlank
    @Schema(description = "User email", example = "john.doe@example.com")
    String email;

    @NotBlank
    @Schema(description = "User role", example = "USER")
    Role role;

    public UserViewDto() {
    }

    public UserViewDto(int id, String name, String email, Role role) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.role = role;
    }

    public long getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}
