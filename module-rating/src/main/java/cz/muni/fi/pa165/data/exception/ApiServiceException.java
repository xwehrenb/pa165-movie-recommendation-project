package cz.muni.fi.pa165.data.exception;

public class ApiServiceException extends RuntimeException {
    public ApiServiceException(String message) {
        super(message);
    }
}

