package cz.muni.fi.pa165.config;

import cz.muni.fi.pa165.App;
import cz.muni.fi.pa165.data.model.auth.Role;
import io.swagger.v3.oas.models.security.OAuthFlow;
import io.swagger.v3.oas.models.security.OAuthFlows;
import io.swagger.v3.oas.models.security.Scopes;
import io.swagger.v3.oas.models.security.SecurityScheme;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springdoc.core.customizers.OpenApiCustomizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@EnableWebSecurity
@Configuration
@RequiredArgsConstructor
public class SecurityConfig {

    private final JwtAuthConverter jwtAuthConverter;

    @Bean
    public AuthenticationFailureHandler authenticationFailureHandler() {
        return new AuthFailureHandler();
    }

    @Bean
    protected SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http
                .authorizeHttpRequests(x -> x
                        .requestMatchers(HttpMethod.POST, "/api/criterion/**")
                        .hasRole(Role.ADMIN)
                        .requestMatchers(HttpMethod.PATCH, "/api/criterion/**")
                        .hasRole(Role.ADMIN)
                        .requestMatchers(HttpMethod.DELETE, "/api/criterion/**")
                        .hasRole(Role.ADMIN)
                        .requestMatchers(HttpMethod.PUT, "/api/rating")
                        .hasAnyRole(Role.ADMIN, Role.USER)
                        .requestMatchers(HttpMethod.DELETE, "/api/rating")
                        .hasAnyRole(Role.ADMIN, Role.USER)
                        .anyRequest()
                        .permitAll()
                )
                .csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
                .oauth2ResourceServer()
                .jwt()
                .jwtAuthenticationConverter(jwtAuthConverter);

        return http.build();
    }
}
