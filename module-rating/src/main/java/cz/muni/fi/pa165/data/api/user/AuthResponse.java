package cz.muni.fi.pa165.data.api.user;

public class AuthResponse {
    UserViewDto item;
    String message;

    public AuthResponse() {
    }

    public AuthResponse(UserViewDto item, String message) {
        this.item = item;
        this.message = message;
    }
    public UserViewDto getItem() {
        return item;
    }

    public void setItem(UserViewDto item) {
        this.item = item;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
