package cz.muni.fi.pa165.data.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;

import java.util.Set;
import java.util.UUID;

@Entity
public class User {

    @Id
    @NotNull
    UUID uuid;

    @OneToMany(mappedBy = "user", cascade = CascadeType.REMOVE, orphanRemoval = true)
    Set<Rating> ratings;

    public User(String uuid) {
        this.uuid = UUID.fromString(uuid);
    }

    public User(UUID uuid) {
        this.uuid = uuid;
    }

    public User() {
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID id) {
        this.uuid = id;
    }

    public Set<Rating> getRatings() {
        return ratings;
    }
}
