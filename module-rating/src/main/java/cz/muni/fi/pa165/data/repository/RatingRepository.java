package cz.muni.fi.pa165.data.repository;

import cz.muni.fi.pa165.data.model.Rating;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface RatingRepository extends JpaRepository<Rating, Integer> {

    @Query("SELECT r FROM Rating r WHERE r.user.uuid = :userId AND r.movie.id = :movieId AND r.criterion.id = :criterionId")
    Optional<Rating> findByUserIdAndMovieIdAndCriterionId(@Param("userId") UUID userId, @Param("movieId") int movieId, @Param("criterionId") int criterionId);

    @Query("SELECT r FROM Rating r WHERE (:userId is null OR r.user.uuid= :userId) AND (:movieId is null OR r.movie.id = :movieId) AND (:criterionId is null OR r.criterion.id = :criterionId)")
    Page<Rating> findAllByUserIdAndMovieIdAndCriterionId(@Param("userId") Optional<UUID> userId, @Param("movieId") Optional<Integer> movieId, @Param("criterionId") Optional<Integer> criterionId, Pageable pageable);
}
