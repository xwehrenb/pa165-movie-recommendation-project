package cz.muni.fi.pa165.config;

import cz.muni.fi.pa165.data.model.Criterion;
import cz.muni.fi.pa165.data.model.Movie;
import cz.muni.fi.pa165.data.model.Rating;
import cz.muni.fi.pa165.data.model.User;
import cz.muni.fi.pa165.data.repository.CriterionRepository;
import cz.muni.fi.pa165.data.repository.MovieRepository;
import cz.muni.fi.pa165.data.repository.RatingRepository;
import cz.muni.fi.pa165.data.repository.UserRepository;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class Seed {

    private final UserRepository userRepository;
    private final MovieRepository movieRepository;
    private final CriterionRepository criterionRepository;
    private final RatingRepository ratingRepository;

    @Value("${seed.mode}") // Set the seed mode so the tests can be more atomic
    private String seedMode;

    @Autowired
    public Seed(UserRepository userRepository, MovieRepository movieRepository, CriterionRepository criterionRepository, RatingRepository ratingRepository) {
        this.userRepository = userRepository;
        this.movieRepository = movieRepository;
        this.criterionRepository = criterionRepository;
        this.ratingRepository = ratingRepository;
    }

    @PostConstruct
    public void seed() {
        System.out.println("Seed mode " + seedMode);
        if (seedMode.equals("test")) {
            var user1 = userRepository.save(new User("9ef7470e-ecbf-11ed-a05b-0242ac120003"));
            var user2 = userRepository.save(new User("9ef7470e-ecbf-11ed-a05b-0242ac120004"));
            var user3 = userRepository.save(new User("9ef7470e-ecbf-11ed-a05b-0242ac120005"));

            var criterion1 = criterionRepository.save(new Criterion("Criterion 1", "Description 1"));
            var criterion2 = criterionRepository.save(new Criterion("Criterion 2", "Description 2"));
            var criterion3 = criterionRepository.save(new Criterion("Criterion 3", "Description 3"));

            var movie1 = movieRepository.save(new Movie());
            var movie2 = movieRepository.save(new Movie());
            var movie3 = movieRepository.save(new Movie());

            ratingRepository.save(new Rating(1, criterion1, movie1, user1));
            ratingRepository.save(new Rating(5, criterion2, movie2, user2));
            ratingRepository.save(new Rating(10, criterion3, movie3, user3));

            ratingRepository.save(new Rating(5, criterion2, movie1, user1));
            ratingRepository.save(new Rating(6, criterion1, movie2, user2));
            ratingRepository.save(new Rating(1, criterion3, movie3, user3));

            ratingRepository.save(new Rating(6, criterion3, movie1, user1));
            ratingRepository.save(new Rating(3, criterion2, movie2, user2));
            ratingRepository.save(new Rating(0, criterion1, movie3, user3));
        } else if (seedMode.equals("prod")) {
            List<Criterion> criteria = Arrays.asList(
                    new Criterion("Acting Quality", "How well did the actors deliver their lines and embody their characters?"),
                    new Criterion("Plot Complexity", "How intricate and engaging was the storyline?"),
                    new Criterion("Cinematography", "How visually stunning was the film?"),
                    new Criterion("Sound Design", "How well-crafted were the sound effects and soundtrack?"),
                    new Criterion("Emotional Impact", "How much of an emotional response did the movie elicit from the audience?")
            );
            criterionRepository.saveAll(criteria);
        }
    }
}
