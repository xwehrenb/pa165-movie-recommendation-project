package cz.muni.fi.pa165.rest;

import cz.muni.fi.pa165.data.api.criterion.CriterionCreateDto;
import cz.muni.fi.pa165.data.api.criterion.CriterionViewDto;
import cz.muni.fi.pa165.data.model.Criterion;
import cz.muni.fi.pa165.service.CriterionService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.modelmapper.ModelMapper;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(path = "/api/criterion", produces = MediaType.APPLICATION_JSON_VALUE)
public class CriterionController {

    private final CriterionService criterionService;
    private final ModelMapper modelMapper;

    @Autowired
    public CriterionController(CriterionService criterionService, ModelMapper modelMapper) {
        this.criterionService = criterionService;
        this.modelMapper = modelMapper;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    @Operation(
            summary = "Find all criteria",
            description = "Gets all the criteria without any specific querying.",
            responses = {
                    @ApiResponse(responseCode = "200", description = "All criteria were found.")
            },
            tags = {"Criterion"}
    )
    public ResponseEntity<List<CriterionViewDto>> getAll(@ParameterObject Pageable pageable) {
        Page<Criterion> criteria = criterionService.getAll(pageable);
        List<CriterionViewDto> view = criteria.getContent().stream().map(criterion -> modelMapper.map(criterion, CriterionViewDto.class)).toList();

        return ResponseEntity.ok(view);
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @Operation(
            summary = "Find criterion",
            description = "Gets a specific criterion according to the ID.",
            parameters = {
                    @Parameter(name = "id", description = "Criterion ID", example = "1")
            },
            responses = {
                    @ApiResponse(responseCode = "200", description = "Criterion was found."),
                    @ApiResponse(responseCode = "404", description = "Criterion was not found.")
            },
            tags = {"Criterion"}
    )
    public ResponseEntity<CriterionViewDto> get(@PathVariable int id) {
        Criterion criterion = criterionService.getById(id);
        CriterionViewDto view = modelMapper.map(criterion, CriterionViewDto.class);

        return ResponseEntity.ok(view);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    @Operation(
            summary = "Create criterion",
            description = "Create a new criterion with specified name and description.",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Criterion was created.")
            },
            tags = {"Criterion"}
    )
    @SecurityRequirement(name = "OAuth2")
    public ResponseEntity<CriterionViewDto> add(@RequestBody CriterionCreateDto criterion) {
        Criterion entity = modelMapper.map(criterion, Criterion.class);
        entity = criterionService.create(entity);
        CriterionViewDto view = modelMapper.map(entity, CriterionViewDto.class);

        return ResponseEntity.ok(view);
    }

    @PatchMapping
    @ResponseStatus(HttpStatus.OK)
    @Operation(
            summary = "Edit criterion",
            description = "Edit specific criterion's name and description.",
            tags = {"Criterion"}
    )
    @SecurityRequirement(name = "OAuth2")
    public ResponseEntity<CriterionViewDto> update(@RequestBody CriterionViewDto criterion) {
        Criterion entity = modelMapper.map(criterion, Criterion.class);
        entity = criterionService.update(entity);
        CriterionViewDto view = modelMapper.map(entity, CriterionViewDto.class);

        return ResponseEntity.ok(view);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @Operation(
            summary = "Delete criterion",
            description = "Delete a specific criterion.",
            parameters = {
                    @Parameter(name = "id", description = "Criterion ID", example = "1")
            },
            responses = {
                    @ApiResponse(responseCode = "200", description = "Criterion was deleted."),
                    @ApiResponse(responseCode = "404", description = "Criterion was not found.")
            },
            tags = {"Criterion"}
    )
    @SecurityRequirement(name = "OAuth2")
    public ResponseEntity<CriterionViewDto> delete(@PathVariable int id) {
        var entity = criterionService.delete(id);
        CriterionViewDto view = modelMapper.map(entity, CriterionViewDto.class);

        return ResponseEntity.ok(view);
    }
}
