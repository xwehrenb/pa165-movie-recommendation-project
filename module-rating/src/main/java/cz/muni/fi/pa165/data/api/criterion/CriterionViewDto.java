package cz.muni.fi.pa165.data.api.criterion;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

public class CriterionViewDto {

    @NotNull
    @Schema(description = "Criterion ID", example = "1")
    int id;

    @NotBlank
    @Size(min = 3, max = 50)
    @Schema(description = "Criterion name", example = "Acting")
    String name;

    @NotBlank
    @Size(min = 3, max = 50)
    @Schema(description = "Criterion description", example = "Acting of the actors")
    String description;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
