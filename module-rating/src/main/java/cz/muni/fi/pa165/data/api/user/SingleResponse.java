package cz.muni.fi.pa165.data.api.user;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema(title = "Single response", description = "Response of a single item")
public class SingleResponse<T> {

    T item;

    @Schema(description = "Message", example = "Success")
    String message;

    public T getItem() {
        return item;
    }

    public void setItem(T item) {
        this.item = item;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
