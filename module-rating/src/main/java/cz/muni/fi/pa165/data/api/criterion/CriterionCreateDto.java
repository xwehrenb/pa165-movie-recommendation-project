package cz.muni.fi.pa165.data.api.criterion;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;

public class CriterionCreateDto {

    @NotBlank
    @Size(min = 3, max = 50)
    @Schema(description = "Criterion name", example = "Acting")
    String name;

    @NotBlank
    @Size(min = 3, max = 50)
    @Schema(description = "Criterion description", example = "Acting of the actors")
    String description;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
