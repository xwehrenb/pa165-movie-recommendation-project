package cz.muni.fi.pa165.service;

import cz.muni.fi.pa165.data.exception.EntityNotFoundException;
import cz.muni.fi.pa165.data.model.Criterion;
import cz.muni.fi.pa165.data.repository.CriterionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CriterionService {

    private final CriterionRepository criterionRepository;

    @Autowired
    public CriterionService(CriterionRepository criterionRepository) {
        this.criterionRepository = criterionRepository;
    }

    public Page<Criterion> getAll(Pageable pageable) {
        return criterionRepository.findAll(pageable);
    }

    public Criterion getById(int id) throws EntityNotFoundException {
        return criterionRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Criterion", id));
    }

    public Criterion create(Criterion criterion) {
        return criterionRepository.save(criterion);
    }

    public Criterion update(Criterion criterion) throws EntityNotFoundException {
        Optional<Criterion> entityToPatch = criterionRepository.findById(criterion.getId());
        if (entityToPatch.isEmpty()) {
            throw new EntityNotFoundException(criterion);
        }

        entityToPatch.get().setName(criterion.getName());
        entityToPatch.get().setDescription(criterion.getDescription());

        return criterionRepository.save(entityToPatch.get());
    }

    public Criterion delete(int id) throws EntityNotFoundException {
        Criterion entity = criterionRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Criterion", id));

        criterionRepository.delete(entity);

        return entity;
    }
}
