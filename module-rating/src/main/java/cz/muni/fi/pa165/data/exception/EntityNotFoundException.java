package cz.muni.fi.pa165.data.exception;

import cz.muni.fi.pa165.data.model.GenericEntity;

import java.util.UUID;

public class EntityNotFoundException extends RuntimeException {

    public EntityNotFoundException() {
        super("Entity not found");
    }

    public EntityNotFoundException(int id) {
        super("Entity with id " + id + " not found");
    }

    public EntityNotFoundException(String name, int id) {
        super("Entity " + name + " with id " + id + " not found");
    }

    public EntityNotFoundException(GenericEntity entity) {
        super("Entity " + entity.getClass().getSimpleName() + " with id " + entity.getId() + " not found");
    }

    public EntityNotFoundException(String message) {
        super(message);
    }

    public EntityNotFoundException(String user, UUID uuid) {
        super("Entity " + user + " with UUID " + uuid.toString() + " not found");

    }
}
