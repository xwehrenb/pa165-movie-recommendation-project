package cz.muni.fi.pa165.data.model.exception;

import io.swagger.v3.oas.annotations.media.Schema;

public class RestExceptionModel {

    @Schema(description = "Exception message", example = "Entity not found")
    String message;

    public RestExceptionModel(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
