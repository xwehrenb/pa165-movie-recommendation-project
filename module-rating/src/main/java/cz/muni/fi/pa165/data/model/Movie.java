package cz.muni.fi.pa165.data.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;

import java.util.Set;

@Entity
public class Movie implements GenericEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @NotNull
    int id;

    @OneToMany(mappedBy = "movie", cascade = CascadeType.REMOVE, orphanRemoval = true)
    Set<Rating> ratings;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Set<Rating> getRatings() {
        return ratings;
    }
}
