package cz.muni.fi.pa165.data.api.movie;

public class MovieResponseDto {
    private MovieDto item;
    private String message;

    public MovieDto getItem() {
        return item;
    }

    public void setItem(MovieDto item) {
        this.item = item;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
