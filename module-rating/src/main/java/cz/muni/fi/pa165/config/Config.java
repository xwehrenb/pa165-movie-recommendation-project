package cz.muni.fi.pa165.config;

import cz.muni.fi.pa165.data.api.rating.RatingCreateDto;
import cz.muni.fi.pa165.data.model.Rating;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Config {

    @Bean
    public ModelMapper modelMapper() {
        ModelMapper mapper = new ModelMapper();
        mapper.addMappings(new PropertyMap<RatingCreateDto, Rating>() {
            @Override
            protected void configure() {
                skip(destination.getId());
            }
        });

        return mapper;
    }
}
