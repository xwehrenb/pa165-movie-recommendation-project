package cz.muni.fi.pa165.api.auth.registration;

import cz.muni.fi.pa165.api.user.UserBasicViewDto;
import io.swagger.v3.oas.annotations.media.Schema;

import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Schema(
        title = "User registration response",
        description = "User registration response DTO with created user"
)
public class RegisterResponseDto {
    UserBasicViewDto item;

    @NotBlank
    @Schema(description = "Response message", example = "User registered successfully")
    private String message;
}
