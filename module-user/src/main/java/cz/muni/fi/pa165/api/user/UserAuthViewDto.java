package cz.muni.fi.pa165.api.user;

import cz.muni.fi.pa165.data.model.User;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;

import java.util.List;
import java.util.UUID;

@Schema(title = "Basic User", description = "Basic user")
public record UserAuthViewDto (
        @NotBlank
        @Schema(description = "DB id", example = "1")
        UUID id,

        @NotBlank
        @Schema(description = "Username", example = "john_doe")
        String username,

        @NotBlank
        @Schema(description = "User name", example = "John Doe")
        String name,

        @NotBlank
        @Schema(description = "User email", example = "john.doe@example.com")
        String email,

        @NotBlank
        @Schema(description = "User description", example = "I am John Doe and I am a user.")
        String description,

        @NotBlank
        @Schema(description = "User roles", example = "[USER, ADMIN]")
        List<String> roles
) {

    public static UserAuthViewDto from(User user, List<String> roles) {
        return new UserAuthViewDto(
                user.getId(),
                user.getUsername(),
                user.getName(),
                user.getEmail(),
                user.getDescription(),
                roles
        );
    }

}
