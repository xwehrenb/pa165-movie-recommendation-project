package cz.muni.fi.pa165.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UtilRestController {

    @GetMapping("/healthcheck")
    public String all() {
        return "OK";
    }
}