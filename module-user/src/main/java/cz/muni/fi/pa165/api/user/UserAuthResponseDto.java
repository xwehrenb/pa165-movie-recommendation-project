package cz.muni.fi.pa165.api.user;

import io.swagger.v3.oas.annotations.media.Schema;

import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Schema(
        title = "User authentication response",
        description = "User authentication response DTO with user"
)
public class UserAuthResponseDto {
    UserAuthViewDto item;

    @NotBlank
    @Schema(description = "Response message", example = "User john.doe@email.com is authenticated")
    private String message;
}
