package cz.muni.fi.pa165.api.user;

import cz.muni.fi.pa165.data.model.User;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;

import java.util.UUID;

@Schema(title = "Basic User", description = "Basic user")
public record UserBasicViewDto (
        @NotBlank
        @Schema(description = "DB id", example = "1")
        UUID id,

        @NotBlank
        @Schema(description = "Username", example = "john_doe")
        String username,

        @NotBlank
        @Schema(description = "User name", example = "John Doe")
        String name,

        @NotBlank
        @Schema(description = "User email", example = "john.doe@example.com")
        String email,

        @NotBlank
        @Schema(description = "User description", example = "USER")
        String description
) {

    public static UserBasicViewDto from(User user) {
        return new UserBasicViewDto(
            user.getId(),
            user.getUsername(),
            user.getName(),
            user.getEmail(),
            user.getDescription()
    );
}

}
