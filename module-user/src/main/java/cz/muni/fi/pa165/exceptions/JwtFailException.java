package cz.muni.fi.pa165.exceptions;

import org.springframework.http.HttpStatus;

import javax.naming.AuthenticationException;

public class JwtFailException extends AuthenticationException {
    private HttpStatus httpStatus;

    public JwtFailException(HttpStatus status, String message) {
        super(message);
        this.httpStatus = status;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }
}
