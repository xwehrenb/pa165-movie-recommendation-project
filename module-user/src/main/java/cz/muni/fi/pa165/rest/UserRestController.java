package cz.muni.fi.pa165.rest;

import cz.muni.fi.pa165.api.user.UserAuthResponseDto;
import cz.muni.fi.pa165.api.user.UserListResponseDto;
import cz.muni.fi.pa165.facade.UserFacade;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.info.*;

import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.security.oauth2.server.resource.introspection.OAuth2IntrospectionAuthenticatedPrincipal;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@OpenAPIDefinition(
    info = @Info(
        title = "User repository",
        version = "1.0",
        description = "Managing users, authentication and authorization",
        contact = @Contact(name = "Petr Wehrenberg", email = "485059@mail.muni.cz"),
        license = @License(name = "Apache 2.0", url = "https://www.apache.org/licenses/LICENSE-2.0.html")
    )
)
@RestController
@RequiredArgsConstructor
@RequestMapping(path="/api/users")
public class UserRestController {

    private final UserFacade userFacade;

    @GetMapping(path = "")
    @Operation(
        summary = "User list",
        description = "Return list of all users.",
        security = @SecurityRequirement(name = "OAuth2", scopes = {"test_2"}),
        responses = {
                @ApiResponse(responseCode = "200", description = "List all users"),
                @ApiResponse(responseCode = "401", description = "Unauthorized access"),
        }
    )
    public ResponseEntity<UserListResponseDto> getAllUsers() {
        var responseDto = userFacade.getAllUsers();
        return ResponseEntity.ok(responseDto);
    }

    @GetMapping(path = "/authenticate")
    @Operation(
        summary = "Authenticate user",
        description = "Return information about authenticated user",
        security = @SecurityRequirement(name = "OAuth2", scopes = {"test_1"}),
        responses = {
                @ApiResponse(responseCode = "200", description = "Get auth information about user"),
                @ApiResponse(responseCode = "401", description = "Unauthorized access"),
        }
    )
    public ResponseEntity<UserAuthResponseDto> authenticate(Principal principal) {

        var responseDto = userFacade.userAuth(principal.getName());
        return ResponseEntity.ok(responseDto);
    }
}