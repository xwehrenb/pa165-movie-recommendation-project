package cz.muni.fi.pa165.config;

import cz.muni.fi.pa165.exceptions.ApiException;
import cz.muni.fi.pa165.api.common.ErrorMessage;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ApiExceptionHandler {

    @ExceptionHandler(value = { ApiException.class })
    public ResponseEntity<Object> handleApiException(ApiException e) {
        ErrorMessage errorMessage = new ErrorMessage(
                e.getHttpStatus().getReasonPhrase(),
                e.getMessage()
        );
        return ResponseEntity.status(e.getHttpStatus()).body(errorMessage);
    }
}
