package cz.muni.fi.pa165.api.user;

import io.swagger.v3.oas.annotations.media.Schema;

import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Schema(
        title = "User list response",
        description = "User list response DTO with list of users"
)
public class UserListResponseDto {
    List<UserBasicViewDto> item;

    @NotBlank
    @Schema(description = "Response message", example = "Listed 3 users")
    private String message;
}
