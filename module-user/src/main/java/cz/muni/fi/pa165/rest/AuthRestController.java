package cz.muni.fi.pa165.rest;

import cz.muni.fi.pa165.api.auth.registration.RegisterRequestDto;
import cz.muni.fi.pa165.api.auth.registration.RegisterResponseDto;
import cz.muni.fi.pa165.facade.AuthFacade;
import io.swagger.v3.oas.annotations.*;
import io.swagger.v3.oas.annotations.info.*;
import io.swagger.v3.oas.annotations.media.*;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.UUID;

@RestController
@OpenAPIDefinition(
    info = @Info(
        title = "Auth management",
        version = "1.0",
        description = "Manage user authorization and authentication",
        contact = @Contact(name = "Petr Wehrenberg", email = "485059@mail.muni.cz"),
        license = @License(name = "Apache 2.0", url = "https://www.apache.org/licenses/LICENSE-2.0.html")
    )
)
@RequestMapping("/api/auth")
@RequiredArgsConstructor
public class AuthRestController {

    private final AuthFacade authFacade;

    @PostMapping("register")
    @Operation(
        summary = "User registration",
        description = "Register authenticate user to Movie Recommender",
        security = @SecurityRequirement(name = "OAuth2", scopes = {"test_1"}),
        responses = {
            @ApiResponse(
                responseCode = "200",
                content = @Content(schema = @Schema(implementation = RegisterResponseDto.class))
            )
        }
    )
    public ResponseEntity<RegisterResponseDto> register(Principal principal) {

        JwtAuthenticationToken token = (JwtAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
        System.out.println("--- REGISTRATION ---");
        System.out.println("NAME: " + principal.getName());
        System.out.println("PRIN: " + principal.toString());
        System.out.println("Id: " + token.getTokenAttributes().get("sub"));
        System.out.println("--- ------------ ---");

        var roles = SecurityContextHolder.getContext().getAuthentication().getAuthorities();

        System.out.println("ROLES: " + roles);

        var registerRes = authFacade.register(
                UUID.fromString(token.getTokenAttributes().get("sub").toString()),
                token.getTokenAttributes().get("name").toString(),
                token.getTokenAttributes().get("given_name").toString() + " " + token.getTokenAttributes().get("family_name").toString(),
                token.getTokenAttributes().get("email").toString(),
                ""
                );
        return ResponseEntity.ok(registerRes);
    }
}
