package cz.muni.fi.pa165.config;

import cz.muni.fi.pa165.App;
import cz.muni.fi.pa165.data.model.Role;
import io.swagger.v3.oas.models.security.OAuthFlow;
import io.swagger.v3.oas.models.security.OAuthFlows;
import io.swagger.v3.oas.models.security.Scopes;
import io.swagger.v3.oas.models.security.SecurityScheme;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springdoc.core.customizers.OpenApiCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class SecurityConfiguration {

    private static final Logger log = LoggerFactory.getLogger(App.class);
    private final JwtAuthConverter jwtAuthConverter;

    @Bean
    public AuthenticationFailureHandler authenticationFailureHandler() {
        return new AuthFailureHandler();
    }

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http
            .authorizeHttpRequests(x -> x
                .requestMatchers(HttpMethod.GET, "/api/auth/register").hasRole(Role.USER)
                .requestMatchers(HttpMethod.GET, "/api/users").hasRole(Role.ADMIN)
                .requestMatchers(HttpMethod.GET, "/api/users/authenticate").hasAnyRole(Role.ADMIN, Role.USER)
                .anyRequest().permitAll()
            )
            .csrf().disable()
            .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
            .oauth2ResourceServer()
                .jwt()
                .jwtAuthenticationConverter(jwtAuthConverter);

        return http.build();
    }




    private final SecurityScheme swaggerSecurityScheme = new SecurityScheme()
            .type(SecurityScheme.Type.OAUTH2)
            .description("OAuth2 KeyCloak")
            .flows(new OAuthFlows()
                    .authorizationCode(new OAuthFlow()
                            .authorizationUrl("http://keycloak:8080/auth/realms/JavaProject/protocol/openid-connect/auth")
                            .tokenUrl("http://keycloak:8080/auth/realms/JavaProject/protocol/openid-connect/token")
                            .scopes(new Scopes())
                    )
            );

    @Bean
    public OpenApiCustomizer openAPICustomizer() {
        return openApi -> {
            log.info("adding security to OpenAPI description");
            openApi.getComponents().addSecuritySchemes("OAuth2", swaggerSecurityScheme);
        };
    }

}
