package cz.muni.fi.pa165.facade;

import cz.muni.fi.pa165.api.user.UserAuthResponseDto;
import cz.muni.fi.pa165.api.user.UserAuthViewDto;
import cz.muni.fi.pa165.api.user.UserBasicViewDto;
import cz.muni.fi.pa165.api.user.UserListResponseDto;
import cz.muni.fi.pa165.service.AuthService;
import cz.muni.fi.pa165.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserFacade {

    private final UserService userService;
    private final AuthService authService;

    public UserListResponseDto getAllUsers() {
        var users = this.userService.getAllUsers().stream().map(UserBasicViewDto::from).toList();

        return UserListResponseDto.builder()
                .item(users)
                .message(String.format("Listed %x users", users.size()))
                .build();
    }

    public UserAuthResponseDto userAuth(String username) {
        var user = this.userService.getUserByUsername(username);
        var userRoles = authService.getCurrentUserRoles();

        return UserAuthResponseDto.builder()
                .item(UserAuthViewDto.from(user, userRoles))
                .message(String.format("User %s is authenticated", user.getEmail()))
                .build();
    }
}
