package cz.muni.fi.pa165.facade;

import cz.muni.fi.pa165.api.auth.registration.RegisterRequestDto;
import cz.muni.fi.pa165.api.auth.registration.RegisterResponseDto;
import cz.muni.fi.pa165.service.AuthService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class AuthFacade {

    private final AuthService authService;

    public RegisterResponseDto register(UUID uuid, String username, String name, String email, String description) {
        var resp = authService.register(uuid, username, name, email, description);
        return RegisterResponseDto.builder()
                .item(resp)
                .message(String.format("User %s is registered", resp.email()))
                .build();
    }

}
