package cz.muni.fi.pa165.config;

import cz.muni.fi.pa165.data.model.Role;
import cz.muni.fi.pa165.data.model.User;
import cz.muni.fi.pa165.data.repository.UserRepository;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Transactional
@Service
public class MockDb {

    private final UserRepository userRepository;

    @Autowired
    public MockDb(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @PostConstruct
    public void insertInitialData() {
        mockUsers();
    }

    private void mockUsers() {
        User userA = new User();
        userA.setId(UUID.fromString("30e1a9ac-ec25-11ed-a05b-0242ac120003"));
        userA.setName("Inspector Lestrade");
        userA.setUsername("inspector_lestrade");
        userA.setEmail("inpector@mail.com");
        userA.setDescription("I am a detective inspector at Scotland Yard.");
        userRepository.save(userA);

        User userB = new User();
        userB.setId(UUID.fromString("30e1a9ac-ec25-11ed-a05b-0242ac120004"));
        userB.setName("Jim Moriarty");
        userB.setUsername("jim_moriarty");
        userB.setEmail("jim.moriarty@evil.com");
        userB.setDescription("I am a consulting criminal.");
        userRepository.save(userB);
    }

}
