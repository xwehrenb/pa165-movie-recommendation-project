package cz.muni.fi.pa165.service;

import cz.muni.fi.pa165.api.auth.registration.RegisterRequestDto;
import cz.muni.fi.pa165.api.user.UserBasicViewDto;
import cz.muni.fi.pa165.data.model.User;
import cz.muni.fi.pa165.data.repository.UserRepository;
import cz.muni.fi.pa165.exceptions.ApiException;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionSystemException;

import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class AuthService {

    private final UserRepository userRepository;

    public UserBasicViewDto register(UUID uuid, String username, String name, String email, String description) {
        User user = User.builder()
                .id(uuid)
                .username(username)
                .name(name)
                .email(email)
                .description(description)
                .build();

        try {
            var dbUser = userRepository.save(user);
            return UserBasicViewDto.from(dbUser);
        } catch (DataIntegrityViolationException e) {
            throw new ApiException(HttpStatus.CONFLICT, "User with this email or username already exists");
        } catch (TransactionSystemException e) {
            throw new ApiException(HttpStatus.BAD_REQUEST, "User is not valid");
        } catch (Exception e) {
            throw new ApiException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
        }
    }

    public List<String> getCurrentUserRoles() {
        var authorities = SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        var roles = authorities.stream().filter(auth -> auth.getAuthority().startsWith("ROLE_")).toList();
        return roles.stream().map(auth -> auth.getAuthority().substring(5)).toList();
    }

}
