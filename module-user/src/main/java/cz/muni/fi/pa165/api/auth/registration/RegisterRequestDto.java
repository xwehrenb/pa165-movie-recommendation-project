package cz.muni.fi.pa165.api.auth.registration;

import io.swagger.v3.oas.annotations.media.Schema;

import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Schema(
        title = "User registration request",
        description = "User registration request DTO with name, email, password and role"
)
public class RegisterRequestDto {
    @NotBlank
    @Schema(description = "User name", example = "Sherlock Holmes")
    private String name;

    @NotBlank
    @Schema(description = "User email", example = "john.doe@email.com")
    private String email;

    @Schema(description = "User role", example = "USER")
    private String description;
}
