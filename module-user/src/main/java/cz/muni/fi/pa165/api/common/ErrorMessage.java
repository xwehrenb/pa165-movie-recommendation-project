package cz.muni.fi.pa165.api.common;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema(title = "Error message", description = "Response body for HTML statuses")

public record ErrorMessage (
        @Schema(description = "HTTP status text", example = "Not Found")
        String error,

        @Schema(description = "reason for error", example = "entity not found")
        String description
) {}
