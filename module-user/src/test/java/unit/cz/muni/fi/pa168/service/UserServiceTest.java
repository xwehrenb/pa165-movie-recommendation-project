package unit.cz.muni.fi.pa168.service;

import cz.muni.fi.pa165.data.model.User;
import cz.muni.fi.pa165.data.repository.UserRepository;
import cz.muni.fi.pa165.exceptions.ApiException;
import cz.muni.fi.pa165.service.UserService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.List;
import java.util.Optional;

public class UserServiceTest {

    private UserService userService;
    private UserRepository userRepository;

    @BeforeEach
    public void setup() {
        userRepository = Mockito.mock(UserRepository.class);
        userService = new UserService(userRepository);
    }

    @Test
    public void getAllUsers() {
        User userA = User.builder().name("Sherlock Holmes").email("sherlock.holmes@mail.com").build();
        User userB = User.builder().name("Jim Moriarty").email("jim.moriarty@mail.com").build();

        Mockito.when(userRepository.findAll()).thenReturn(List.of(userA, userB));
        List<User> users = userService.getAllUsers();
        Assertions.assertArrayEquals(List.of(userA, userB).toArray(), users.toArray());
    }

    @Test
    public void getAllUsersWithNoUsers() {
        Mockito.when(userRepository.findAll()).thenReturn(List.of());
        List<User> users = userService.getAllUsers();
        Assertions.assertTrue(users.isEmpty());
    }

    @Test
    public void getAllUsersWithOneUser() {
        User userA = User.builder().name("Sherlock Holmes").email("sherlock.holmes@mail.com").build();

        Mockito.when(userRepository.findAll()).thenReturn(List.of(userA));
        List<User> users = userService.getAllUsers();
        Assertions.assertArrayEquals(List.of(userA).toArray(), users.toArray());
    }

    @Test
    public void getByUsernameWhenExist() {
        User userB = User.builder().name("Jim Moriarty").email("jim.moriarty@mail.com").build();

        Mockito.when(userRepository.findByUsername("jimmy")).thenReturn(Optional.of(userB));
        User user = userService.getUserByUsername("jimmy");
        Assertions.assertEquals(user, userB);
    }

    @Test
    public void getByEmailWhenNotExist() {
        Mockito.when(userRepository.findByUsername("jimmy")).thenReturn(Optional.empty());
        var exception = Assertions.assertThrows(ApiException.class, () -> userService.getUserByUsername("jimmy"));
        Assertions.assertEquals(exception.getMessage(), "User with this username does not exist");
    }
}
