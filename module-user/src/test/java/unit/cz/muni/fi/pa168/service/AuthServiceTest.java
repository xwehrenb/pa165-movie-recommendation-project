package unit.cz.muni.fi.pa168.service;

import cz.muni.fi.pa165.api.user.UserBasicViewDto;
import cz.muni.fi.pa165.service.AuthService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import java.util.Optional;
import java.util.UUID;

import cz.muni.fi.pa165.api.auth.registration.RegisterRequestDto;
import cz.muni.fi.pa165.data.model.Role;
import cz.muni.fi.pa165.data.model.User;
import cz.muni.fi.pa165.data.repository.UserRepository;
import cz.muni.fi.pa165.exceptions.ApiException;

class AuthServiceTest {
    private AuthService authService;
    private UserRepository userRepository;
    private AuthenticationManager authenticationManager;

    @BeforeEach
    public void setup() {

        userRepository = Mockito.mock(UserRepository.class);
        authService = new AuthService(userRepository);
    }

    @Test
    void registerTest() {
        var user = User
                .builder()
                .id(UUID.fromString("0e6f108c-ec1e-11ed-a05b-0242ac120003"))
                .username("john.watson")
                .name("John Watson")
                .email("john.watson@doctor.com")
                .description("")
                .build();

        var userBasic = new UserBasicViewDto(
                UUID.fromString("0e6f108c-ec1e-11ed-a05b-0242ac120003"),
                "john.watson",
                "John Watson",
                "john.watson@doctor.com",
                ""
        );

        Mockito.when(userRepository.save(user)).thenReturn(user);

        UserBasicViewDto resp = authService.register(
                UUID.fromString("0e6f108c-ec1e-11ed-a05b-0242ac120003"),
                "john.watson",
                "John Watson",
                "john.watson@doctor.com",
                "");
        Assertions.assertEquals(resp, userBasic);
    }

    @Test
    void registerExistingUser() {
        var user = User
                .builder()
                .id(UUID.fromString("0e6f108c-ec1e-11ed-a05b-0242ac120003"))
                .username("john.watson")
                .name("John Watson")
                .email("john.watson@doctor.com")
                .description("")
                .build();

        var conflictException = new DataIntegrityViolationException("Some message");

        Mockito.when(userRepository.save(user)).thenThrow(conflictException);

        var registerException = Assertions.assertThrows(ApiException.class, () -> authService.register(
                UUID.fromString("0e6f108c-ec1e-11ed-a05b-0242ac120003"),
                "john.watson",
                "John Watson",
                "john.watson@doctor.com",
                ""));
        Assertions.assertEquals("User with this email or username already exists", registerException.getMessage());
        Assertions.assertEquals(registerException.getHttpStatus(), HttpStatus.CONFLICT);
    }
}
