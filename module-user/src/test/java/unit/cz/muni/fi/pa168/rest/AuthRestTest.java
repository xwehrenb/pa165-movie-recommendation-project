package unit.cz.muni.fi.pa168.rest;

import cz.muni.fi.pa165.App;
import cz.muni.fi.pa165.api.auth.registration.RegisterRequestDto;
import cz.muni.fi.pa165.api.auth.registration.RegisterResponseDto;
import cz.muni.fi.pa165.api.user.UserBasicViewDto;
import cz.muni.fi.pa165.exceptions.ApiException;
import cz.muni.fi.pa165.facade.AuthFacade;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.test.web.servlet.MockMvc;

import java.time.Instant;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.authentication;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest(classes = App.class)
@AutoConfigureMockMvc
public class AuthRestTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AuthFacade authFacade;

    @Test
    void successfulRegistrationTest() throws Exception {
        String expectedMessage = "User john@mail.com is registered";
        String expectedToken = "expected token";

        var resDto = new RegisterResponseDto(new UserBasicViewDto(
                UUID.fromString("0e6f108c-ec1e-11ed-a05b-0242ac120003"),
                "john.watson",
                "John Watson",
                "john.watson@doctor.com",
                ""
        ),expectedMessage);

        Mockito.when(authFacade.register(
                UUID.fromString("0e6f108c-ec1e-11ed-a05b-0242ac120003"),
                "john.watson",
                "John Watson",
                "john.watson@doctor.com",
                "")
        ).thenReturn(resDto);

        var request = post("/api/auth/register")
                .with(authentication(produceUser()))
                .contentType(MediaType.APPLICATION_JSON_VALUE);

        this.mockMvc.perform(request)
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.message").value(expectedMessage));
    }

    @Test
    void existingUserRegistrationTest() throws Exception {
        String expectedDescription = "User with this email already exists";
        var conflictException = new ApiException(HttpStatus.CONFLICT, expectedDescription);


        Mockito.when(authFacade.register(
                UUID.fromString("0e6f108c-ec1e-11ed-a05b-0242ac120003"),
                "john.watson",
                "John Watson",
                "john.watson@doctor.com",
                "")
        ).thenThrow(conflictException);

        var request = post("/api/auth/register")
                .with(authentication(produceUser()))
                .contentType(MediaType.APPLICATION_JSON_VALUE);

        this.mockMvc.perform(request)
                .andDo(print())
                .andExpect(status().is4xxClientError())
                .andExpect(jsonPath("$.description").value(expectedDescription));
    }

    private JwtAuthenticationToken produceUser() {
        String value = "placeholder";
        Instant issuedAt = Instant.now();
        Instant expiresAt = issuedAt.plusSeconds(3600);

        Map<String, Object> headers = new HashMap<>();
        headers.put("alg", "HS256");
        headers.put("typ", "JWT");

        Map<String, Object> claims = new HashMap<>();
        claims.put("sub", "0e6f108c-ec1e-11ed-a05b-0242ac120003");
        claims.put("custom_claim", "custom_value");
        claims.put("typ", "Bearer");
        claims.put("given_name", "John");
        claims.put("family_name", "Watson");
        claims.put("name", "john.watson");
        claims.put("email", "john.watson@doctor.com");
        claims.put("scope", "email profile");
        Jwt jwt = new Jwt(value, issuedAt, expiresAt, headers, claims);

        Collection<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_user"); // Add the required authorities
        return new JwtAuthenticationToken(jwt, authorities);
    }
}
