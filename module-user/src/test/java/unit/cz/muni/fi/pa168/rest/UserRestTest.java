package unit.cz.muni.fi.pa168.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.fi.pa165.App;
import cz.muni.fi.pa165.api.user.UserAuthResponseDto;
import cz.muni.fi.pa165.api.user.UserBasicViewDto;
import cz.muni.fi.pa165.api.user.UserListResponseDto;
import cz.muni.fi.pa165.data.model.Role;
import cz.muni.fi.pa165.data.model.User;
import cz.muni.fi.pa165.data.repository.UserRepository;
import cz.muni.fi.pa165.exceptions.JwtFailException;
import cz.muni.fi.pa165.facade.UserFacade;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.test.web.servlet.MockMvc;

import java.time.Instant;
import java.util.*;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.authentication;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest(classes = App.class)
@AutoConfigureMockMvc
public class UserRestTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserFacade userFacade;

    @MockBean
    private UserRepository userRepository;

    @Autowired
    private ObjectMapper objectMapper;

    private final String fakeAuthToken = "someFakeAuthToken";


    @Test
    void allUsersTest() throws Exception {
        var usersDto = List.of(
                new UserBasicViewDto(
                        UUID.fromString("30e1a9ac-ec25-11ed-a05b-0242ac120003"),
                        "inspector_lestrade", "Sherlock Holmes", "sherlock@mail.com", Role.ADMIN),
                new UserBasicViewDto(
                        UUID.fromString("30e1a9ac-ec25-11ed-a05b-0242ac120004"),
                        "jim_moriarty", "Jim Moriarty", "jim@mail.com", Role.ADMIN)
        );

        var resDto = new UserListResponseDto(usersDto, "Listed 2 users");
        var expectedResponseBody = objectMapper.writeValueAsString(resDto);

        Mockito.when(userFacade.getAllUsers()).thenReturn(resDto);

        var request = get("/api/users").with(authentication(produceAdminUser()));

        this.mockMvc.perform(request)
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(expectedResponseBody));
    }

    @Test
    void emptyUsersTest() throws Exception {
        List<UserBasicViewDto> usersDto = List.of();

        var resDto = new UserListResponseDto(usersDto, "Listed 0 users");
        var expectedResponseBody = objectMapper.writeValueAsString(resDto);

        Mockito.when(userFacade.getAllUsers()).thenReturn(resDto);

        var request = get("/api/users").with(authentication(produceAdminUser()));;

        this.mockMvc.perform(request)
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(expectedResponseBody));
    }

    @Test
    void unauthorizedUserListTest() throws Exception {
        String expectedContent = "Missing valid Bearer token";

        var request = get("/api/users");

        this.mockMvc.perform(request)
                .andDo(print())
                .andExpect(status().isUnauthorized());
    }

    private JwtAuthenticationToken produceAdminUser() throws Exception {
        String value = "placeholder";
        Instant issuedAt = Instant.now();
        Instant expiresAt = issuedAt.plusSeconds(3600);

        Map<String, Object> headers = new HashMap<>();
        headers.put("alg", "HS256");
        headers.put("typ", "JWT");

        Map<String, Object> claims = new HashMap<>();
        claims.put("sub", "0e6f108c-ec1e-11ed-a05b-0242ac120003");
        claims.put("custom_claim", "custom_value");
        claims.put("typ", "Bearer");
        claims.put("given_name", "John");
        claims.put("family_name", "Watson");
        claims.put("name", "john.watson");
        claims.put("email", "john.watson@doctor.com");
        claims.put("scope", "email profile");
        Jwt jwt = new Jwt(value, issuedAt, expiresAt, headers, claims);

        Collection<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_app-admin"); // Add the required authorities
        return new JwtAuthenticationToken(jwt, authorities);
    }

}
