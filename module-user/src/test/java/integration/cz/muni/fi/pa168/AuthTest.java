package integration.cz.muni.fi.pa168;

import cz.muni.fi.pa165.App;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;

import java.time.Instant;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest(classes = App.class)
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
class AuthTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void successfulRegistrationTest() throws Exception {

        String value = "placeholder";
        Instant issuedAt = Instant.now();
        Instant expiresAt = issuedAt.plusSeconds(3600);

        Map<String, Object> headers = new HashMap<>();
        headers.put("alg", "HS256");
        headers.put("typ", "JWT");

        Map<String, Object> claims = new HashMap<>();
        claims.put("sub", "0e6f108c-ec1e-11ed-a05b-0242ac120003");
        claims.put("custom_claim", "custom_value");
        claims.put("typ", "Bearer");
        claims.put("given_name", "John");
        claims.put("family_name", "Watson");
        claims.put("name", "john.watson");
        claims.put("email", "john.watson@doctor.com");
        claims.put("scope", "email profile");
        Jwt jwt = new Jwt(value, issuedAt, expiresAt, headers, claims);

        Collection<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_app-user"); // Add the required authorities
        JwtAuthenticationToken auth= new JwtAuthenticationToken(jwt, authorities);

        String expectedMessage = "User john.watson@doctor.com is registered";

        var request = post("/api/auth/register")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .with(authentication(auth))
                .with(csrf());

        this.mockMvc.perform(request)
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.message").value(expectedMessage));
    }

    @Test
    void userAlreadyExistsRegistrationTest() throws Exception {

        String value = "placeholder";
        Instant issuedAt = Instant.now();
        Instant expiresAt = issuedAt.plusSeconds(3600);

        Map<String, Object> headers = new HashMap<>();
        headers.put("alg", "HS256");
        headers.put("typ", "JWT");

        Map<String, Object> claims = new HashMap<>();
        claims.put("sub", "0e6f108c-ec1e-22fa-a05b-0242ac120003");
        claims.put("custom_claim", "custom_value");
        claims.put("typ", "Bearer");
        claims.put("given_name", "Sherlock");
        claims.put("family_name", "Holmes");
        claims.put("name", "Sherlock Holmes");
        claims.put("email", "jim.moriarty@evil.com");
        claims.put("scope", "email profile");
        Jwt jwt = new Jwt(value, issuedAt, expiresAt, headers, claims);

        Collection<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_user"); // Add the required authorities
        JwtAuthenticationToken auth= new JwtAuthenticationToken(jwt, authorities);

        String expectedMessage = "User with this email or username already exists";

        var request = post("/api/auth/register")
                .with(authentication(auth))
                .contentType(MediaType.APPLICATION_JSON_VALUE);

        this.mockMvc.perform(request)
                .andDo(print())
                .andExpect(status().is4xxClientError())
                .andExpect(jsonPath("$.description").value(expectedMessage));
    }
}
