package integration.cz.muni.fi.pa168;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.fi.pa165.App;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
// org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;

import java.time.Instant;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.authentication;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest(classes = App.class)
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class UserTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;

    @Test
    //@WithMockUser(roles = {"ADMIN"})
    void getUsersTest() throws Exception {

        String expectedContent = """
            {
                "item": [
                    {
                       "id": "30e1a9ac-ec25-11ed-a05b-0242ac120003",
                       "username": "inspector_lestrade",
                       "name": "Inspector Lestrade",
                       "email": "inpector@mail.com",
                       "description": "I am a detective inspector at Scotland Yard."
                     },
                     {
                       "id": "30e1a9ac-ec25-11ed-a05b-0242ac120004",
                       "username": "jim_moriarty",
                       "name": "Jim Moriarty",
                       "email": "jim.moriarty@evil.com",
                       "description": "I am a consulting criminal."
                     }
                ],
                "message": "Listed 2 users"
            }
        """;

        var request = get("/api/users").with(authentication(produceAdminUser()));

        this.mockMvc.perform(request)
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(expectedContent));
    }

    @Test
    void missingTokenTest() throws Exception {
        var request = get("/api/users");

        this.mockMvc.perform(request)
                .andDo(print())
                .andExpect(status().isUnauthorized());
    }


    private JwtAuthenticationToken produceAdminUser() throws Exception {
        String value = "placeholder";
        Instant issuedAt = Instant.now();
        Instant expiresAt = issuedAt.plusSeconds(3600);

        Map<String, Object> headers = new HashMap<>();
        headers.put("alg", "HS256");
        headers.put("typ", "JWT");

        Map<String, Object> claims = new HashMap<>();
        claims.put("sub", "0e6f108c-ec1e-11ed-a05b-0242ac120003");
        claims.put("custom_claim", "custom_value");
        claims.put("typ", "Bearer");
        claims.put("given_name", "John");
        claims.put("family_name", "Watson");
        claims.put("name", "john.watson");
        claims.put("email", "john.watson@doctor.com");
        claims.put("scope", "email profile");
        Jwt jwt = new Jwt(value, issuedAt, expiresAt, headers, claims);

        Collection<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_app-admin"); // Add the required authorities
        return new JwtAuthenticationToken(jwt, authorities);
    }
}
