package cz.muni.fi.pa165.rest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import cz.muni.fi.pa165.api.auth.AuthResponse;
import cz.muni.fi.pa165.api.auth.Role;
import cz.muni.fi.pa165.api.auth.UserBasicViewDto;
import cz.muni.fi.pa165.api.movieStakeholder.CreateMovieStakeHolderRequestDto;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.io.IOException;
import java.time.Instant;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.authentication;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class MovieStakeHolderTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;


    @Order(1)
    @Test
    void getMovieStakeholdersTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/movie-stakeholders"))
              .andExpect(MockMvcResultMatchers.status().isOk())
              .andExpect(content().json(
                      """
                      {
                      	"content": [
                      		{
                      			"id": 1,
                      			"name": "Francis",
                      			"surname": "Coppola"
                      		},
                      		{
                      			"id": 2,
                      			"name": "Al",
                      			"surname": "Pacino"
                      		},
                      		{
                      			"id": 3,
                      			"name": "Marlon",
                      			"surname": "Brando"
                      		},
                      		{
                      			"id": 4,
                      			"name": "Jason",
                      			"surname": "Momoa"
                      		},
                      		{
                      			"id": 5,
                      			"name": "Amber",
                      			"surname": "Heard"
                      		}
                      	],
                      	"message": "Success"
                      }
                      """
              ));
    }

    @Order(2)
    @Rollback(false)
    @Test
    void postMovieStakeholderTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/movie-stakeholders")
              .with(authentication(produceAdminUser()))
              .content(objectMapper.writeValueAsString(new CreateMovieStakeHolderRequestDto("Anthony", "Hopkins")))
              .contentType("application/json"))
              .andExpect(MockMvcResultMatchers.status().isCreated())
              .andExpect(content().json(
                        """
                        {
                            "content": {
                                "id": 6,
                                "name": "Anthony",
                                "surname": "Hopkins"
                            },
                            "message": "Movie stake holder successfully created."
                        }
                        
                        """
              ));
    }

    @Order(3)
    @Test
    void verifyMovieStakeholderCreatedTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/movie-stakeholders"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().json(
                        """
                        {
                            "content": [
                                {
                                    "id": 1,
                                    "name": "Francis",
                                    "surname": "Coppola"
                                },
                                {
                                    "id": 2,
                                    "name": "Al",
                                    "surname": "Pacino"
                                },
                                {
                                    "id": 3,
                                    "name": "Marlon",
                                    "surname": "Brando"
                                },
                                {
                                    "id": 4,
                                    "name": "Jason",
                                    "surname": "Momoa"
                                },
                                {
                                    "id": 5,
                                    "name": "Amber",
                                    "surname": "Heard"
                                },
                                {
                                    "id": 6,
                                    "name": "Anthony",
                                    "surname": "Hopkins"
                                }
                            ],
                            "message": "Success"
                        }
                        """
                ));
    }

    @Order(4)
    @Test
    void updateMovieStakeholderTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.put("/api/movie-stakeholders/5")
                        .with(authentication(produceAdminUser()))
                        .content(objectMapper.writeValueAsString(new CreateMovieStakeHolderRequestDto("Amber", "Stupid")))
                        .contentType("application/json")
                )
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().json(
                      """
                      {
                          "content": {
                              "id": 5,
                              "name": "Amber",
                              "surname": "Stupid"
                          },
                          "message": "Movie stakeholder successfully updated."
                      }
                      """
              ));
    }

    @Order(5)
    @Test
    void verifyMovieStakeholderUpdatedTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/movie-stakeholders"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().json(
                        """
                        {
                            "content": [
                                {
                                    "id": 1,
                                    "name": "Francis",
                                    "surname": "Coppola"
                                },
                                {
                                    "id": 2,
                                    "name": "Al",
                                    "surname": "Pacino"
                                },
                                {
                                    "id": 3,
                                    "name": "Marlon",
                                    "surname": "Brando"
                                },
                                {
                                    "id": 4,
                                    "name": "Jason",
                                    "surname": "Momoa"
                                },
                                {
                                    "id": 5,
                                    "name": "Amber",
                                    "surname": "Stupid"
                                },
                                {
                                    "id": 6,
                                    "name": "Anthony",
                                    "surname": "Hopkins"
                                }
                            ],
                            "message": "Success"
                        }
                        """
                ));
    }

    @Order(6)
    @Test
    void getMovieStakeholderPagedTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/movie-stakeholders/paged")
                        .queryParam("page", "0")
                        .queryParam("size", "3"))
             .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().json(
                            """
                            {
                            	"content": [
                            		{
                            			"id": 1,
                            			"name": "Francis",
                            			"surname": "Coppola"
                            		},
                            		{
                            			"id": 2,
                            			"name": "Al",
                            			"surname": "Pacino"
                            		},
                            		{
                            			"id": 3,
                            			"name": "Marlon",
                            			"surname": "Brando"
                            		}
                            	],
                            	"total": 6,
                            	"message": "Success"
                            }
                            """));
    }

    @Order(7)
    @Rollback(false)
    @Test
    void deleteMovieStakeholderTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/movie-stakeholders/5")
                .with(authentication(produceAdminUser())))
                .andExpect(MockMvcResultMatchers.status().isNoContent())
                .andExpect(content().json(
                        """
                        {
                            "message": "Movie stakeholder successfully deleted."
                        }
                        """
                ));
    }

    @Order(8)
    @Test
    void verifyMovieStakeholderDeletedTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/movie-stakeholders"))
               .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().json(
                        """
                        {
                            "content": [
                                {
                                    "id": 1,
                                    "name": "Francis",
                                    "surname": "Coppola"
                                },
                                {
                                    "id": 2,
                                    "name": "Al",
                                    "surname": "Pacino"
                                },
                                {
                                    "id": 3,
                                    "name": "Marlon",
                                    "surname": "Brando"
                                },
                                {
                                    "id": 4,
                                    "name": "Jason",
                                    "surname": "Momoa"
                                },
                                {
                                    "id": 6,
                                    "name": "Anthony",
                                    "surname": "Hopkins"
                                }
                            ],
                            "message": "Success"
                        }
                        """
                ));
    }

    @Order(9)
    @Test
    void authWorkingTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/movie-stakeholders")
                        .content(objectMapper.writeValueAsString(new CreateMovieStakeHolderRequestDto("Anthony", "Hopkins")))
                        .contentType("application/json"))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }


    private JwtAuthenticationToken produceAdminUser() throws Exception {
        String value = "placeholder";
        Instant issuedAt = Instant.now();
        Instant expiresAt = issuedAt.plusSeconds(3600);

        Map<String, Object> headers = new HashMap<>();
        headers.put("alg", "HS256");
        headers.put("typ", "JWT");

        Map<String, Object> claims = new HashMap<>();
        claims.put("sub", "0e6f108c-ec1e-11ed-a05b-0242ac120003");
        claims.put("custom_claim", "custom_value");
        claims.put("typ", "Bearer");
        claims.put("given_name", "John");
        claims.put("family_name", "Watson");
        claims.put("name", "john.watson");
        claims.put("email", "john.watson@doctor.com");
        claims.put("scope", "email profile");
        Jwt jwt = new Jwt(value, issuedAt, expiresAt, headers, claims);

        Collection<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_app-admin"); // Add the required authorities
        return new JwtAuthenticationToken(jwt, authorities);
    }
}
