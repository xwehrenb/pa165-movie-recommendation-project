package cz.muni.fi.pa165.service;

import cz.muni.fi.pa165.api.image.CreateImageRequestDto;
import cz.muni.fi.pa165.config.Routes;
import cz.muni.fi.pa165.data.model.Image;
import cz.muni.fi.pa165.data.repository.ImageRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doNothing;

@SpringBootTest
public class ImageServiceTest {

    private ImageRepository imageRepository;
    private ImageService imageService;
    @BeforeEach
    public void setUp() {
        imageRepository = Mockito.mock(ImageRepository.class);
        imageService = new ImageService(imageRepository);
    }

    @Test
    void getAllImagesTest() {
        Mockito.when(imageRepository.findAll()).thenReturn(List.of(getImage()));
        List<Image> images = imageService.listImages(Routes.IMAGES);

        assertEquals(1, images.size());
        Image foundImage = images.get(0);
        assertEquals(-1L, foundImage.getId());
        assertEquals("url", foundImage.getUrl());
        assertEquals("Nice", foundImage.getDescription());
    }

    @Test
    void getImageTest() {
        Mockito.when(imageRepository.findById(-1L)).thenReturn(Optional.of(getImage()));
        Image foundImage = imageService.getImageById(-1L, Routes.IMAGES + Routes.IMAGE_SINGLE);

        assertEquals(-1L, foundImage.getId());
        assertEquals("url", foundImage.getUrl());
        assertEquals("Nice", foundImage.getDescription());
    }

    @Test
    void createImageTest() {
        Mockito.when(imageRepository.save(any(Image.class))).thenReturn(getImage());

        CreateImageRequestDto requestDto = new CreateImageRequestDto("url", "Nice");
        Image savedImage = imageService.create(requestDto, Routes.IMAGES);

        Mockito.verify(imageRepository, Mockito.times(1)).save(any(Image.class));

        assertEquals(-1L, savedImage.getId());
        assertEquals("url", savedImage.getUrl());
        assertEquals("Nice", savedImage.getDescription());
    }

    @Test
    void deleteImageTest() {
        Mockito.when(imageRepository.existsById(-1L)).thenReturn(true);
        doNothing().when(imageRepository).deleteById(anyLong());

        imageService.delete(-1L, Routes.IMAGES + Routes.IMAGE_SINGLE);

        Mockito.verify(imageRepository, Mockito.times(1)).deleteById(-1L);
    }



    private Image getImage() {
        Image image = new Image();
        image.setId(-1L);
        image.setUrl("url");
        image.setDescription("Nice");
        return image;
    }

}
