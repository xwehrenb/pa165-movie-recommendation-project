package cz.muni.fi.pa165.rest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import cz.muni.fi.pa165.api.auth.AuthResponse;
import cz.muni.fi.pa165.api.auth.Role;
import cz.muni.fi.pa165.api.auth.UserBasicViewDto;
import cz.muni.fi.pa165.api.genre.CreateGenreRequestDto;
import cz.muni.fi.pa165.api.image.CreateImageRequestDto;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.web.servlet.MockMvc;

import java.io.IOException;
import java.time.Instant;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.authentication;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class ImageControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @Order(1)
    @Test
    void getImages() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/images"))
              .andExpect(status().isOk())
              .andExpect(content().json(
                      """
                                 {
                                 	"content": [
                                 		{
                                 			"id": 1,
                                 			"url": "www.random.com/image1",
                                 			"description": "Nice title image"
                                 		},
                                 		{
                                 			"id": 2,
                                 			"url": "www.random.com/image2",
                                 			"description": "Nice image"
                                 		},
                                 		{
                                 			"id": 3,
                                 			"url": "www.random.com/image3",
                                 			"description": "Ugly title image"
                                 		},
                                 		{
                                 			"id": 4,
                                 			"url": "www.random.com/image4",
                                 			"description": "Low res image"
                                 		}
                                 	],
                                 	"message": "Success"
                                 }
                                 """
              ));
    }

    @Order(2)
    @Rollback(false)
    @Test
    void createImage() throws Exception {
        String json = objectMapper.writeValueAsString(new CreateImageRequestDto("https://i1.sndcdn.com/artworks-dVH0EE65EwUOKdvd-NfhGug-t500x500.jpg", "test"));
        mockMvc.perform(MockMvcRequestBuilders.post("/api/images")
                    .contentType("application/json")
                    .with(authentication(produceAdminUser()))
                    .content(json)
                )
                .andExpect(status().isCreated())
                .andExpect(content().json("""
                        {
                            "content": {
                                 "id": 5,
                                 "url": "https://i1.sndcdn.com/artworks-dVH0EE65EwUOKdvd-NfhGug-t500x500.jpg",
                                 "description": "test"
                            },
                            "message":"Success"
                        }
                        """
                        )
                );
    }

    @Order(3)
    @Test
    void verifyImageCreated() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/images"))
                .andExpect(status().isOk())
                .andExpect(content().json(
                       """
                       {
                           "content": [
                               {
                                   "id": 1,
                                   "url": "www.random.com/image1",
                                   "description": "Nice title image"
                               },
                               {
                                   "id": 2,
                                   "url": "www.random.com/image2",
                                   "description": "Nice image"
                               },
                               {
                                   "id": 3,
                                   "url": "www.random.com/image3",
                                   "description": "Ugly title image"
                               },
                               {
                                   "id": 4,
                                   "url": "www.random.com/image4",
                                   "description": "Low res image"
                               },
                               {
                                 "id": 5,
                                 "url": "https://i1.sndcdn.com/artworks-dVH0EE65EwUOKdvd-NfhGug-t500x500.jpg",
                                 "description": "test"
                               }
                           ],
                           "message": "Success"
                       }
                       """
                ));
    }

    @Order(4)
    @Test
    void getImage() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/images/1"))
               .andExpect(status().isOk())
               .andExpect(content().json(
                        """
                        {
                            "content": {
                                 "id": 1,
                                 "url": "www.random.com/image1",
                                 "description": "Nice title image"
                            },
                            "message":"Success"
                        }
                        """
                ));
    }

    @Order(5)
    @Test
    void deleteImage() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/images/5")
                        .with(authentication(produceAdminUser()))
                )
                .andExpect(status().isNoContent())
                .andExpect(content().json(
                        """
                        {
                            "message":"Success"
                        }
                        """
                ));
    }

    @Order(6)
    @Test
    void verifyImageDeleted() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/images"))
                .andExpect(status().isOk())
                .andExpect(content().json(
                        """
                                   {
                                       "content": [
                                           {
                                               "id": 1,
                                               "url": "www.random.com/image1",
                                               "description": "Nice title image"
                                           },
                                           {
                                               "id": 2,
                                               "url": "www.random.com/image2",
                                               "description": "Nice image"
                                           },
                                           {
                                               "id": 3,
                                               "url": "www.random.com/image3",
                                               "description": "Ugly title image"
                                           },
                                           {
                                               "id": 4,
                                               "url": "www.random.com/image4",
                                               "description": "Low res image"
                                           }
                                       ],
                                       "message": "Success"
                                   }
                                   """
                ));
    }

    @Order(7)
    @Test
    void authWorkingTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/images/5"))
                .andExpect(status().isUnauthorized());
    }

    private JwtAuthenticationToken produceAdminUser() throws Exception {
        String value = "placeholder";
        Instant issuedAt = Instant.now();
        Instant expiresAt = issuedAt.plusSeconds(3600);

        Map<String, Object> headers = new HashMap<>();
        headers.put("alg", "HS256");
        headers.put("typ", "JWT");

        Map<String, Object> claims = new HashMap<>();
        claims.put("sub", "0e6f108c-ec1e-11ed-a05b-0242ac120003");
        claims.put("custom_claim", "custom_value");
        claims.put("typ", "Bearer");
        claims.put("given_name", "John");
        claims.put("family_name", "Watson");
        claims.put("name", "john.watson");
        claims.put("email", "john.watson@doctor.com");
        claims.put("scope", "email profile");
        Jwt jwt = new Jwt(value, issuedAt, expiresAt, headers, claims);

        Collection<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_app-admin"); // Add the required authorities
        return new JwtAuthenticationToken(jwt, authorities);
    }
}
