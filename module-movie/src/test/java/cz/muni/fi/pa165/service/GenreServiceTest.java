package cz.muni.fi.pa165.service;

import cz.muni.fi.pa165.api.genre.CreateGenreRequestDto;
import cz.muni.fi.pa165.config.Routes;
import cz.muni.fi.pa165.data.model.Genre;
import cz.muni.fi.pa165.data.repository.GenreRepository;
import cz.muni.fi.pa165.exceptions.ApiException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@SpringBootTest
public class GenreServiceTest {

    private GenreRepository genreRepository;

    private GenreService genreService;

    @BeforeEach
    public void setUp() {
        genreRepository = Mockito.mock(GenreRepository.class);
        genreService = new GenreService(genreRepository);
    }

    // write test to get all genres using mocked function
    @Test
    void getAllGenresTest() {
        when(genreRepository.findAll()).thenReturn(List.of(getGenre()));

        List<Genre> genres = genreService.findAllGenres(Routes.GENRES);

        assertEquals(1, genres.size());
        Genre foundGenre = genres.get(0);
        assertEquals(-1, foundGenre.getId());
        assertEquals("Genre0", foundGenre.getName());
    }

    @Test
    void getPageOfGenresTest() {
        when(genreRepository.findAll(any(Pageable.class))).thenReturn(new PageImpl<>(List.of(getGenre())));

        Page<Genre> genres = genreService.getPageOfGenres(Pageable.unpaged(), Routes.GENRES + Routes.GENRE_PAGED);

        assertEquals(1, genres.get().count());
        Genre foundGenre = genres.stream().toList().get(0);
        assertEquals(-1, foundGenre.getId());
        assertEquals("Genre0", foundGenre.getName());
    }

    @Test
    void createGenreTest() {
        when(genreRepository.save(any(Genre.class))).thenReturn(getGenre());

        CreateGenreRequestDto requestDto = new CreateGenreRequestDto("Genre0");

        Genre genre = genreService.create(requestDto, Routes.GENRES);

        assertEquals(-1, genre.getId());
        assertEquals("Genre0", genre.getName());
    }

    @Test
    void createGenreWithExistingNameTest() {
        when(genreRepository.save(argThat(p -> p.getName().equals("Genre0")))).thenThrow(
                new ApiException(
                        "Genre with this name already exists",
                        HttpStatus.BAD_REQUEST,
                        Routes.GENRES
                )
        );
        CreateGenreRequestDto requestDto = new CreateGenreRequestDto("Genre0");

        assertThrows(ApiException.class, () -> genreService.create(requestDto, Routes.GENRES));
    }

    @Test
    void updateGenreTest() {
        when(genreRepository.findById(anyLong())).thenReturn(Optional.of(getGenre()));
        CreateGenreRequestDto requestDto = new CreateGenreRequestDto("Updated");

        Genre updatedGenre = genreService.update(getGenre().getId(), requestDto, Routes.GENRES + Routes.GENRE_SINGLE);
        Mockito.verify(genreRepository, Mockito.times(1)).save(any(Genre.class));
        assertEquals(-1, updatedGenre.getId());
        assertEquals("Updated", updatedGenre.getName());
    }

    @Test
    void deleteGenreTest() {
        when(genreRepository.existsById(anyLong())).thenReturn(true);
        doNothing().when(genreRepository).deleteById(anyLong());

        genreService.delete(getGenre().getId(), Routes.GENRES + Routes.GENRE_SINGLE);

        Mockito.verify(genreRepository, Mockito.times(1)).deleteById(anyLong());

    }

    private Genre getGenre() {
        Genre genre = new Genre();
        genre.setId(-1L);
        genre.setName("Genre0");
        return genre;
    }
}
