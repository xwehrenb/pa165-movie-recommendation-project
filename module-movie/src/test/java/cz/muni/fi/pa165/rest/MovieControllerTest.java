package cz.muni.fi.pa165.rest;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import cz.muni.fi.pa165.api.auth.AuthResponse;
import cz.muni.fi.pa165.api.auth.Role;
import cz.muni.fi.pa165.api.auth.UserBasicViewDto;
import cz.muni.fi.pa165.api.genre.CreateGenreRequestDto;
import cz.muni.fi.pa165.api.movie.CreateMovieRequestDto;
import cz.muni.fi.pa165.api.movieStakeholder.CreateMovieStakeHolderRequestDto;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.result.StatusResultMatchers;

import java.io.IOException;
import java.time.Instant;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.authentication;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class MovieControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @Order(1)
    @Test
    void getMoviesTest() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.get("/api/movies"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().json(
                        """
                                {
                                	"content": [
                                		{
                                			"id": 1,
                                			"title": "The Godfather",
                                			"year": 2009,
                                			"director": {
                                				"id": 1,
                                				"name": "Francis",
                                				"surname": "Coppola"
                                			},
                                			"genres": [
                                				{
                                					"id": 1,
                                					"name": "Drama"
                                				}
                                			],
                                			"actors": [
                                				{
                                					"id": 2,
                                					"name": "Al",
                                					"surname": "Pacino"
                                				}
                                			],
                                			"images": []
                                		}
                                	],
                                	"message": "Success"
                                }
                                """
                ));
    }

    @Order(2)
    @Rollback(false)
    @Test
    void postMovieTest() throws Exception {
        String json = objectMapper.writeValueAsString(new CreateMovieRequestDto(
                "Aquaman",
                2019,
                "Cringe movie",
                4L,
                new Long[]{1L},
                new Long[]{4L, 5L},
                new Long[]{3L, 4L}
        ));
        this.mockMvc.perform(MockMvcRequestBuilders.post("/api/movies")
                        .contentType(MediaType.APPLICATION_JSON)
                        .with(authentication(produceAdminUser()))
                        .content(json)
                )
                .andExpect(status().isCreated())
                .andExpect(content().json(
                        """
                                {
                                	"item": {
                                		"id": 2,
                                		"title": "Aquaman",
                                		"year": 2019,
                                		"director": {
                                			"id": 4,
                                			"name": "Jason",
                                			"surname": "Momoa"
                                		},
                                		"genres": [
                                			{
                                				"id": 1,
                                				"name": "Drama"
                                			}
                                		],
                                		"actors": [
                                			{
                                				"id": 4,
                                				"name": "Jason",
                                				"surname": "Momoa"
                                			},
                                			{
                                				"id": 5,
                                				"name": "Amber",
                                				"surname": "Heard"
                                			}
                                		],
                                		"images": [
                                			{
                                				"id": 3,
                                				"imageUrl": "www.random.com/image3",
                                				"description": "Ugly title image"
                                			},
                                			{
                                				"id": 4,
                                				"imageUrl": "www.random.com/image4",
                                				"description": "Low res image"
                                			}
                                		]
                                	},
                                	"message": "Success"
                                }
                                """
                ));
    }

    @Order(3)
    @Test
    void verifyPostMovieTest() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.get("/api/movies"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().json(
                        """
                                {
                                	"content": [
                                		{
                                			"id": 1,
                                			"title": "The Godfather",
                                			"year": 2009,
                                			"director": {
                                				"id": 1,
                                				"name": "Francis",
                                				"surname": "Coppola"
                                			},
                                			"genres": [
                                				{
                                					"id": 1,
                                					"name": "Drama"
                                				}
                                			],
                                			"actors": [
                                				{
                                					"id": 2,
                                					"name": "Al",
                                					"surname": "Pacino"
                                				}
                                			],
                                			"images": []
                                		},
                                		{
                                			"id": 2,
                                			"title": "Aquaman",
                                			"year": 2019,
                                			"director": {
                                				"id": 4,
                                				"name": "Jason",
                                				"surname": "Momoa"
                                			},
                                			"genres": [
                                				{
                                					"id": 1,
                                					"name": "Drama"
                                				}
                                			],
                                			"actors": [
                                				{
                                					"id": 4,
                                					"name": "Jason",
                                					"surname": "Momoa"
                                				},
                                				{
                                					"id": 5,
                                					"name": "Amber",
                                					"surname": "Heard"
                                				}
                                			],
                                			"images": [
                                                {
                                                    "id": 3,
                                                    "imageUrl": "www.random.com/image3",
                                                    "description": "Ugly title image"
                                                },
                                                {
                                                    "id": 4,
                                                    "imageUrl": "www.random.com/image4",
                                                    "description": "Low res image"
                                                }
                                		    ]
                                		}
                                	],
                                	"message": "Success"
                                }
                                """
                ));
    }

    @Order(4)
    @Test
    void getMoviesPagedTest() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.get("/api/movies/paged")
                        .queryParam("page", "0")
                        .queryParam("size", "1"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().json(
                        """
                                {
                                	"content": [
                                		{
                                			"id": 1,
                                			"title": "The Godfather",
                                			"year": 2009,
                                			"director": {
                                				"id": 1,
                                				"name": "Francis",
                                				"surname": "Coppola"
                                			},
                                			"genres": [
                                				{
                                					"id": 1,
                                					"name": "Drama"
                                				}
                                			],
                                			"actors": [
                                				{
                                					"id": 2,
                                					"name": "Al",
                                					"surname": "Pacino"
                                				}
                                			],
                                			"images": []
                                		}
                                	],
                                	"total": 2,
                                	"message": "Success"
                                }
                                """
                ));
    }

    @Order(5)
    @Test
    void getSingleMovieTest() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.get("/api/movies/1"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().json(
                        """
                                {
                                	"item": {
                                		"id": 1,
                                		"title": "The Godfather",
                                		"year": 2009,
                                		"director": {
                                			"id": 1,
                                			"name": "Francis",
                                			"surname": "Coppola"
                                		},
                                		"genres": [
                                			{
                                				"id": 1,
                                				"name": "Drama"
                                			}
                                		],
                                		"actors": [
                                			{
                                				"id": 2,
                                				"name": "Al",
                                				"surname": "Pacino"
                                			}
                                		],
                                		"images": []
                                	},
                                	"message": "Success"
                                }
                                """
                ));
    }

    @Order(6)
    @Rollback(false)
    @Test
    void updateMovieTest() throws Exception {
        String json = objectMapper.writeValueAsString(new CreateMovieRequestDto(
                "Aquaman",
                2019,
                "Cringe movie",
                4L,
                new Long[]{1L},
                new Long[]{4L},
                new Long[]{3L}
        ));
        this.mockMvc.perform(MockMvcRequestBuilders.put("/api/movies/2")
                        .contentType(MediaType.APPLICATION_JSON)
                        .with(authentication(produceAdminUser()))
                        .content(json))
                .andExpect(status().isOk())
                .andExpect(content().json(
                        """
                                {
                                	"item": {
                                		"id": 2,
                                		"title": "Aquaman",
                                		"year": 2019,
                                		"director": {
                                			"id": 4,
                                			"name": "Jason",
                                			"surname": "Momoa"
                                		},
                                		"genres": [
                                			{
                                				"id": 1,
                                				"name": "Drama"
                                			}
                                		],
                                		"actors": [
                                			{
                                				"id": 4,
                                				"name": "Jason",
                                				"surname": "Momoa"
                                			}
                                		],
                                		"images": [
                                			{
                                				"id": 3,
                                				"imageUrl": "www.random.com/image3",
                                				"description": "Ugly title image"
                                			}
                                		]
                                	},
                                	"message": "Success"
                                }
                                """
                ));
    }

    @Order(7)
    @Test
    void verifyUpdateMovieTest() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.get("/api/movies"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().json(
                        """
                                {
                                	"content": [
                                		{
                                			"id": 1,
                                			"title": "The Godfather",
                                			"year": 2009,
                                			"director": {
                                				"id": 1,
                                				"name": "Francis",
                                				"surname": "Coppola"
                                			},
                                			"genres": [
                                				{
                                					"id": 1,
                                					"name": "Drama"
                                				}
                                			],
                                			"actors": [
                                				{
                                					"id": 2,
                                					"name": "Al",
                                					"surname": "Pacino"
                                				}
                                			],
                                			"images": []
                                		},
                                		{
                                  			"id": 2,
                                  			"title": "Aquaman",
                                  			"year": 2019,
                                  			"director": {
                                  				"id": 4,
                                  				"name": "Jason",
                                  				"surname": "Momoa"
                                  			},
                                  			"genres": [
                                  				{
                                  					"id": 1,
                                  					"name": "Drama"
                                  				}
                                  			],
                                  			"actors": [
                                  				{
                                  					"id": 4,
                                  					"name": "Jason",
                                  					"surname": "Momoa"
                                  				}
                                  			],
                                  			"images": [
                                  				{
                                  					"id": 3,
                                  					"imageUrl": "www.random.com/image3",
                                  					"description": "Ugly title image"
                                  				},
                                  				{
                                  					"id": 4,
                                  					"imageUrl": "www.random.com/image4",
                                  					"description": "Low res image"
                                  				}
                                  			]
                                  		}
                                	],
                                	"message": "Success"
                                }
                                """
                ));
    }

    @Order(8)
    @Rollback(false)
    @Test
    void deleteMovieTest() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.delete("/api/movies/2")
                        .with(authentication(produceAdminUser()))
                )
                .andExpect(MockMvcResultMatchers.status().isNoContent())
                .andExpect(content().json(
                        """
                                {
                                    "message": "Success"
                                }
                                """
                ));
    }

    @Order(9)
    @Rollback(false)
    @Test
    void verifyDeleteMovieTest() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.get("/api/movies"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().json(
                        """
                                {
                                	"content": [
                                		{
                                			"id": 1,
                                			"title": "The Godfather",
                                			"year": 2009,
                                			"director": {
                                				"id": 1,
                                				"name": "Francis",
                                				"surname": "Coppola"
                                			},
                                			"genres": [
                                				{
                                					"id": 1,
                                					"name": "Drama"
                                				}
                                			],
                                			"actors": [
                                				{
                                					"id": 2,
                                					"name": "Al",
                                					"surname": "Pacino"
                                				}
                                			],
                                			"images": []
                                		}
                                	],
                                	"message": "Success"
                                }
                                """
                ));
    }

    @Order(10)
    @Rollback(false)
    @Test
    void getRandomMovieTest() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.get("/api/movies/random"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().json(
                        """
                                {
                                	"content": [
                                		{
                                			"id": 1,
                                			"title": "The Godfather",
                                			"year": 2009,
                                			"director": {
                                				"id": 1,
                                				"name": "Francis",
                                				"surname": "Coppola"
                                			},
                                			"genres": [
                                				{
                                					"id": 1,
                                					"name": "Drama"
                                				}
                                			],
                                			"actors": [
                                				{
                                					"id": 2,
                                					"name": "Al",
                                					"surname": "Pacino"
                                				}
                                			],
                                			"images": []
                                		}
                                	],
                                	"message": "Success"
                                }
                                """
                ));
    }

    private JwtAuthenticationToken produceAdminUser() throws Exception {
        String value = "placeholder";
        Instant issuedAt = Instant.now();
        Instant expiresAt = issuedAt.plusSeconds(3600);

        Map<String, Object> headers = new HashMap<>();
        headers.put("alg", "HS256");
        headers.put("typ", "JWT");

        Map<String, Object> claims = new HashMap<>();
        claims.put("sub", "0e6f108c-ec1e-11ed-a05b-0242ac120003");
        claims.put("custom_claim", "custom_value");
        claims.put("typ", "Bearer");
        claims.put("given_name", "John");
        claims.put("family_name", "Watson");
        claims.put("name", "john.watson");
        claims.put("email", "john.watson@doctor.com");
        claims.put("scope", "email profile");
        Jwt jwt = new Jwt(value, issuedAt, expiresAt, headers, claims);

        Collection<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_app-admin"); // Add the required authorities
        return new JwtAuthenticationToken(jwt, authorities);
    }
}
