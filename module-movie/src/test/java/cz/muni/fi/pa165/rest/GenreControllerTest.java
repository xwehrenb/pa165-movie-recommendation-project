package cz.muni.fi.pa165.rest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import cz.muni.fi.pa165.api.auth.AuthResponse;
import cz.muni.fi.pa165.api.auth.Role;
import cz.muni.fi.pa165.api.auth.UserBasicViewDto;
import cz.muni.fi.pa165.api.genre.CreateGenreRequestDto;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import wiremock.org.apache.http.impl.client.CloseableHttpClient;

import java.io.IOException;
import java.time.Instant;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.authentication;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class GenreControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @Order(1)
    @Test
    void getGenresTest() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.get("/api/genres")
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(
                        status().isOk()
                ).andExpect(
                        content().json(
                                """
                                {
                                	"content": [
                                		{
                                			"id": 1,
                                			"name": "Drama"
                                		},
                                		{
                                			"id": 2,
                                			"name": "Thriller"
                                		},
                                		{
                                			"id": 3,
                                			"name": "Comedy"
                                		},
                                		{
                                			"id": 4,
                                			"name": "Romance"
                                		}
                                	],
                                	"message": "Success"
                                }
                                """
                        )
                );
    }

    @Test
    @Order(2)
    @Rollback(false)
    void postGenreTest() throws Exception {
        String json = objectMapper.writeValueAsString(new CreateGenreRequestDto("test"));

        this.mockMvc
                .perform(post("/api/genres")
                        .contentType(MediaType.APPLICATION_JSON)
                        .with(authentication(produceAdminUser()))
                        .content(json))
                .andExpect(status().isCreated())
                .andExpect(content().json(
                        """
                        {
                            "content": {
                                "id":5,"name":"test"
                            },
                            "message":"Successfully created genre."
                        }
                        """
                ));
    }

    @Order(3)
    @Test
    void getGenresCheckAfterAddedTest() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.get("/api/genres")
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(
                        status().isOk()
                ).andExpect(
                        content().json(
                                """
                                {
                                	"content": [
                                		{
                                			"id": 1,
                                			"name": "Drama"
                                		},
                                		{
                                			"id": 2,
                                			"name": "Thriller"
                                		},
                                		{
                                			"id": 3,
                                			"name": "Comedy"
                                		},
                                		{
                                			"id": 4,
                                			"name": "Romance"
                                		},
                                		{
                                		    "id":5,
                                		    "name":"test"
                                		}
                                	],
                                	"message": "Success"
                                }
                                """
                        )
                );
    }

    @Order(4)
    @Test
    @Rollback(false)
    void updateGenreTest() throws Exception {
        String json = objectMapper.writeValueAsString(new CreateGenreRequestDto("Fantasy"));

        this.mockMvc.perform(MockMvcRequestBuilders.put("/api/genres/5")
                .contentType(MediaType.APPLICATION_JSON)
                .with(authentication(produceAdminUser()))
                .content(json))
                .andExpect(status().isOk())
                .andExpect(content().json(
                        """
                        {
                            "content": {
                                "id":5,"name":"Fantasy"
                            },
                            "message":"Successfully updated genre."
                        }
                        """
                ));
    }

    @Order(5)
    @Test
    @Rollback(false)
    void getGenresPagedTest() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.get("/api/genres/paged?page=0&size=3&sort=id,asc")
                        .contentType(MediaType.APPLICATION_JSON)
                        .queryParam("page", "0")
                        .queryParam("size", "2")
                ).andExpect(status().isOk())
                .andExpect(content().json(
                    """
                    {
                    	"content": [
                    		{
                    			"id": 1,
                    			"name": "Drama"
                    		},
                    		{
                    			"id": 2,
                    			"name": "Thriller"
                    		},
                    		{
                    			"id": 3,
                    			"name": "Comedy"
                    		}
                    	],
                    	"total": 5,
                    	"message": "Success"
                    }
                    """
                ));
    }

    @Order(6)
    @Test
    @Rollback(false)
    void deleteGenreTest() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.delete("/api/genres/5")
                        .contentType(MediaType.APPLICATION_JSON)
                        .with(authentication(produceAdminUser()))
                ).andExpect(status().isNoContent())
               .andExpect(content().json(
                        """
                        {
                            "message":"Successfully deleted genre."
                        }
                        """
                ));
    }

    @Order(7)
    @Test
    @Rollback(false)
    void checkGenreDeletedTest() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.get("/api/genres")
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(
                        status().isOk()
                ).andExpect(
                        content().json(
                                """
                                {
                                	"content": [
                                		{
                                			"id": 1,
                                			"name": "Drama"
                                		},
                                		{
                                			"id": 2,
                                			"name": "Thriller"
                                		},
                                		{
                                			"id": 3,
                                			"name": "Comedy"
                                		},
                                		{
                                			"id": 4,
                                			"name": "Romance"
                                		}
                                	],
                                	"message": "Success"
                                }
                                """
                        )
                );
    }

    @Order(8)
    @Test
    @Rollback(false)
    void testAuthIsWorkingTest() throws Exception {
        String json = objectMapper.writeValueAsString(new CreateGenreRequestDto("test"));
        this.mockMvc
                .perform(post("/api/genres")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json))
                .andExpect(status().isUnauthorized());
    }

    private JwtAuthenticationToken produceAdminUser() throws Exception {
        String value = "placeholder";
        Instant issuedAt = Instant.now();
        Instant expiresAt = issuedAt.plusSeconds(3600);

        Map<String, Object> headers = new HashMap<>();
        headers.put("alg", "HS256");
        headers.put("typ", "JWT");

        Map<String, Object> claims = new HashMap<>();
        claims.put("sub", "0e6f108c-ec1e-11ed-a05b-0242ac120003");
        claims.put("custom_claim", "custom_value");
        claims.put("typ", "Bearer");
        claims.put("given_name", "John");
        claims.put("family_name", "Watson");
        claims.put("name", "john.watson");
        claims.put("email", "john.watson@doctor.com");
        claims.put("scope", "email profile");
        Jwt jwt = new Jwt(value, issuedAt, expiresAt, headers, claims);

        Collection<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_app-admin"); // Add the required authorities
        return new JwtAuthenticationToken(jwt, authorities);
    }
}
