package cz.muni.fi.pa165.service;

import cz.muni.fi.pa165.api.movie.CreateMovieRequestDto;
import cz.muni.fi.pa165.config.Routes;
import cz.muni.fi.pa165.data.model.Genre;
import cz.muni.fi.pa165.data.model.Image;
import cz.muni.fi.pa165.data.model.Movie;
import cz.muni.fi.pa165.data.model.MovieStakeHolder;
import cz.muni.fi.pa165.data.repository.GenreRepository;
import cz.muni.fi.pa165.data.repository.ImageRepository;
import cz.muni.fi.pa165.data.repository.MovieRepository;
import cz.muni.fi.pa165.data.repository.StakeHolderRepository;
import cz.muni.fi.pa165.exceptions.ApiException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.mockito.Mockito;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

@SpringBootTest
public class MovieServiceTest {

    private MovieRepository movieRepository;
    private GenreRepository genreRepository;
    private StakeHolderRepository movieStakeHolderRepository;
    private ImageRepository imageRepository;

    private MovieService movieService;

    @BeforeEach
    void setUp() {
        movieRepository = Mockito.mock(MovieRepository.class);
        genreRepository = Mockito.mock(GenreRepository.class);
        movieStakeHolderRepository = Mockito.mock(StakeHolderRepository.class);
        imageRepository = Mockito.mock(ImageRepository.class);

        movieService = new MovieService(movieRepository, genreRepository, movieStakeHolderRepository, imageRepository);
    }

    @Test
    void getMovieByIdTest() {
        when(movieRepository.findById(anyLong())).thenReturn(Optional.of(getMovie()));
        Movie movie = movieService.findById(1L, Routes.MOVIES + Routes.MOVIE_SINGLE);

        assertEquals(-1L, movie.getId());
        assertEquals("title", movie.getTitle());
        assertEquals("description", movie.getDescription());
        assertEquals(1999, movie.getYear());
        assertEquals(getMovieStakeHolder(), movie.getDirector());
        assertEquals(getGenre(), movie.getCategories().stream().toList().get(0));
        assertEquals(getImage(), movie.getImages().stream().toList().get(0));
        assertEquals(1, movie.getActors().size());
        assertEquals(getMovieStakeHolder(), movie.getActors().stream().toList().get(0));
    }

    @Test
    void getMovieByIdNotFoundTest() {
        when(movieRepository.findById(anyLong())).thenReturn(Optional.empty());

        assertThrows(ApiException.class, () -> movieService.findById(-1L, Routes.MOVIES + Routes.MOVIE_SINGLE));

    }

    @Test
    void getMoviesByCriteriaTest() {
        when(movieRepository.findByCriteria(any(), any(), any())).thenReturn(List.of(getMovie()));
        List<Movie> movies = movieService.getMoviesByCriteria(Optional.of(-1L), Optional.of(-1L), Optional.of(-1L), Routes.MOVIES);

        assertEquals(1, movies.size());
        assertEquals(getMovie(), movies.get(0));
    }

    @Test
    void getPageOfMoviesByCriteriaTest() {
        when(movieRepository.findByCriteriaPaged(any(), any(), any(), any(Pageable.class))).thenReturn(new PageImpl<>(List.of(getMovie())));
        Page<Movie> movies = movieService.getPageOfMoviesByCriteria(Optional.of(-1L), Optional.of(-1L), Optional.of(-1L),Pageable.unpaged(), Routes.MOVIES + Routes.MOVIES_PAGED);

        assertEquals(1, movies.get().count());
        assertEquals(getMovie(), movies.stream().toList().get(0));
    }

    @Test
    void getRandomMoviesTest() {
        when(movieRepository.getRandomMovies(any())).thenReturn(List.of(getMovie()));

        List<Movie> movies = movieService.getRandomMovies(Optional.of(1L), Routes.MOVIES + Routes.MOVIES_RANDOM);
        Mockito.verify(movieRepository, Mockito.times(1)).getRandomMovies(any());
        assertEquals(1, movies.size());
        assertEquals(getMovie(), movies.get(0));
    }

    @Test
    void createMovieTest() {
        when(movieRepository.save(any())).thenReturn(getMovie());
        when(genreRepository.findAllByIdIn(anySet())).thenReturn(Set.of(getGenre()));
        when(movieStakeHolderRepository.findAllByIdIn(anySet())).thenReturn(Set.of(getMovieStakeHolder()));
        when(imageRepository.findAllByIdIn(anySet())).thenReturn(Set.of(getImage()));
        when(movieStakeHolderRepository.findById(anyLong())).thenReturn(Optional.of(getMovieStakeHolder()));
        CreateMovieRequestDto requestDto = new CreateMovieRequestDto(
                "title",
                1999,
                "description",
                getMovieStakeHolder().getId(),
                new Long[]{ getGenre().getId() },
                new Long[]{ getMovieStakeHolder().getId() },
                new Long[]{ getImage().getId() }
        );

        Movie movie = movieService.create(requestDto, Routes.MOVIES);

        Mockito.verify(movieRepository, Mockito.times(1)).save(any());
        Mockito.verify(genreRepository, Mockito.times(1)).findAllByIdIn(anySet());
        Mockito.verify(movieStakeHolderRepository, Mockito.times(1)).findAllByIdIn(anySet());
        Mockito.verify(imageRepository, Mockito.times(1)).findAllByIdIn(anySet());

        assertEquals(-1L, movie.getId());
        assertEquals("title", movie.getTitle());
        assertEquals("description", movie.getDescription());
        assertEquals(1999, movie.getYear());
        assertEquals(getMovieStakeHolder(), movie.getDirector());
        assertEquals(getGenre(), movie.getCategories().stream().toList().get(0));
        assertEquals(getImage(), movie.getImages().stream().toList().get(0));
        assertEquals(1, movie.getActors().size());
        assertEquals(getMovieStakeHolder(), movie.getActors().stream().toList().get(0));
    }

    @Test
    void updateMovieTest() {
        when(movieRepository.findById(anyLong())).thenReturn(Optional.of(getMovie()));
        when(genreRepository.findAllByIdIn(anySet())).thenReturn(Set.of(getGenre()));
        when(movieStakeHolderRepository.findAllByIdIn(anySet())).thenReturn(Set.of(getMovieStakeHolder()));
        when(imageRepository.findAllByIdIn(anySet())).thenReturn(Set.of(getImage()));
        when(movieStakeHolderRepository.findById(anyLong())).thenReturn(Optional.of(getMovieStakeHolder()));
        CreateMovieRequestDto requestDto = new CreateMovieRequestDto(
                "Updated",
                1999,
                "description",
                getMovieStakeHolder().getId(),
                new Long[]{ getGenre().getId() },
                new Long[]{ getMovieStakeHolder().getId() },
                new Long[]{ getImage().getId() }
        );

        Movie movie = movieService.update(-1L, requestDto, Routes.MOVIES + Routes.MOVIE_SINGLE);

        Mockito.verify(movieRepository, Mockito.times(1)).findById(anyLong());
        Mockito.verify(genreRepository, Mockito.times(1)).findAllByIdIn(anySet());
        Mockito.verify(movieStakeHolderRepository, Mockito.times(1)).findAllByIdIn(anySet());
        Mockito.verify(imageRepository, Mockito.times(1)).findAllByIdIn(anySet());
        Mockito.verify(movieRepository, Mockito.times(1)).save(any());

        assertEquals(-1L, movie.getId());
        assertEquals("Updated", movie.getTitle());
        assertEquals("description", movie.getDescription());
        assertEquals(1999, movie.getYear());
        assertEquals(getMovieStakeHolder(), movie.getDirector());
        assertEquals(getGenre(), movie.getCategories().stream().toList().get(0));
        assertEquals(getImage(), movie.getImages().stream().toList().get(0));
        assertEquals(1, movie.getActors().size());
        assertEquals(getMovieStakeHolder(), movie.getActors().stream().toList().get(0));
    }

    @Test
    void deleteMovieTest() {
        when(movieRepository.existsById(anyLong())).thenReturn(true);
        movieService.delete(-1L, Routes.MOVIES + Routes.MOVIE_SINGLE);

        Mockito.verify(movieRepository, Mockito.times(1)).existsById(anyLong());
        Mockito.verify(movieRepository, Mockito.times(1)).deleteById(any());
    }

    private Movie getMovie() {
        Movie movie = new Movie();
        movie.setId(-1L);
        movie.setTitle("title");
        movie.setDescription("description");
        movie.setYear(1999);

        MovieStakeHolder movieStakeHolder = getMovieStakeHolder();
        movie.addActor(movieStakeHolder);
        movie.setDirector(movieStakeHolder);

        Genre genre = getGenre();
        movie.addGenre(genre);

        Image image = getImage();
        movie.addImage(image);

        return movie;
    }

    private Genre getGenre() {
        Genre genre = new Genre();
        genre.setId(-1L);
        genre.setName("Genre0");
        return genre;
    }

    private Image getImage() {
        Image image = new Image();
        image.setId(-1L);
        image.setUrl("url");
        image.setDescription("Nice");
        return image;
    }

    private MovieStakeHolder getMovieStakeHolder() {
        MovieStakeHolder movieStakeHolder = new MovieStakeHolder();
        movieStakeHolder.setId(-1L);
        movieStakeHolder.setName("First");
        movieStakeHolder.setSurname("Last");
        return movieStakeHolder;
    }

}
