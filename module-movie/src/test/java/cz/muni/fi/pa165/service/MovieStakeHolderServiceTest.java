package cz.muni.fi.pa165.service;

import cz.muni.fi.pa165.api.movieStakeholder.CreateMovieStakeHolderRequestDto;
import cz.muni.fi.pa165.config.Routes;
import cz.muni.fi.pa165.data.model.MovieStakeHolder;
import cz.muni.fi.pa165.data.repository.StakeHolderRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@SpringBootTest
public class MovieStakeHolderServiceTest {

    private StakeHolderRepository movieStakeHolderRepository;

    private MovieStakeHolderService movieStakeHolderService;

    @BeforeEach
    public void setUp() {
        movieStakeHolderRepository = Mockito.mock(StakeHolderRepository.class);
        movieStakeHolderService = new MovieStakeHolderService(movieStakeHolderRepository);
    }

    @Test
    void getAllMovieStakeHoldersTest() {
        when(movieStakeHolderRepository.findAll()).thenReturn(List.of(getMovieStakeHolder()));

        List<MovieStakeHolder> movieStakeHolders = movieStakeHolderService.getAllMovieStakeHolders(Routes.STAKEHOLDERS);

        assertEquals(1, movieStakeHolders.size());
        MovieStakeHolder foundMovieStakeHolder = movieStakeHolders.get(0);
        assertEquals(-1L, foundMovieStakeHolder.getId());
        assertEquals("First", foundMovieStakeHolder.getName());
        assertEquals("Last", foundMovieStakeHolder.getSurname());
    }

    @Test
    void getPageOfMovieStakeHoldersTest() {
        when(movieStakeHolderRepository.findAll(any(Pageable.class))).thenReturn(new PageImpl<>(List.of(getMovieStakeHolder())));

        Page<MovieStakeHolder> movieStakeHoldersPage = movieStakeHolderService.getPageOfMovieStakeHolders(Pageable.unpaged(), Routes.STAKEHOLDERS + Routes.STAKEHOLDERS_PAGED);

        assertEquals(1, movieStakeHoldersPage.get().count());
        MovieStakeHolder foundMovieStakeHolder = movieStakeHoldersPage.stream().toList().get(0);
        assertEquals(-1L, foundMovieStakeHolder.getId());
        assertEquals("First", foundMovieStakeHolder.getName());
        assertEquals("Last", foundMovieStakeHolder.getSurname());
    }

    @Test
    void createMovieStakeHolderTest() {
        when(movieStakeHolderRepository.save(any(MovieStakeHolder.class))).thenReturn(getMovieStakeHolder());

        CreateMovieStakeHolderRequestDto requestDto = new CreateMovieStakeHolderRequestDto("First", "Last");
        MovieStakeHolder movieStakeHolder = movieStakeHolderService.create(requestDto, Routes.STAKEHOLDERS);

        Mockito.verify(movieStakeHolderRepository, Mockito.times(1)).save(any(MovieStakeHolder.class));
        assertEquals(-1L, movieStakeHolder.getId());
        assertEquals("First", movieStakeHolder.getName());
        assertEquals("Last", movieStakeHolder.getSurname());
    }

    @Test
    void updateMovieStakeHolderTest() {
        when(movieStakeHolderRepository.findById(anyLong())).thenReturn(Optional.of(getMovieStakeHolder()));

        CreateMovieStakeHolderRequestDto requestDto = new CreateMovieStakeHolderRequestDto("First", "Updated");

        MovieStakeHolder movieStakeHolder = movieStakeHolderService.update(getMovieStakeHolder().getId(), requestDto, Routes.STAKEHOLDERS + Routes.STAKEHOLDER_SINGLE);
        Mockito.verify(movieStakeHolderRepository, Mockito.times(1)).save(any(MovieStakeHolder.class));
        assertEquals(-1L, movieStakeHolder.getId());
        assertEquals("First", movieStakeHolder.getName());
        assertEquals("Updated", movieStakeHolder.getSurname());
    }

    @Test
    void deleteMovieStakeHolderTest() {
        when(movieStakeHolderRepository.existsById(anyLong())).thenReturn(true);
        doNothing().when(movieStakeHolderRepository).deleteById(anyLong());

        movieStakeHolderService.delete(getMovieStakeHolder().getId(), Routes.STAKEHOLDERS + Routes.STAKEHOLDER_SINGLE);
        Mockito.verify(movieStakeHolderRepository, Mockito.times(1)).deleteById(anyLong());
    }

    private MovieStakeHolder getMovieStakeHolder() {
        MovieStakeHolder movieStakeHolder = new MovieStakeHolder();
        movieStakeHolder.setId(-1L);
        movieStakeHolder.setName("First");
        movieStakeHolder.setSurname("Last");
        return movieStakeHolder;
    }
}
