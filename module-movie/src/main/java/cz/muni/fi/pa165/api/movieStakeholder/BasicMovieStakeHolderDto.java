package cz.muni.fi.pa165.api.movieStakeholder;

import cz.muni.fi.pa165.data.model.MovieStakeHolder;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;

@Schema(
        description = "Movie stake holder which can be director or actor",
        example = "Jonathan Nolan"
)
public record BasicMovieStakeHolderDto(@NotBlank
                                       @Schema(description = "Id", example = "1")
                                       Long id,
                                       @NotBlank
                                       @Schema(description = "Name", example = "Jonathan")
                                       String name,
                                       @NotBlank
                                       @Schema(description = "Surname", example = "Nolan")
                                       String surname
                                      ) {
    public static BasicMovieStakeHolderDto from(MovieStakeHolder movieStakeHolder) {
        return new BasicMovieStakeHolderDto(
                movieStakeHolder.getId(),
                movieStakeHolder.getName(),
                movieStakeHolder.getSurname()
        );
    }
}
