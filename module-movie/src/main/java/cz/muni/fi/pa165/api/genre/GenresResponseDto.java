package cz.muni.fi.pa165.api.genre;


import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;

import java.util.List;

@Schema(
        title = "Response for a list of genres",
        description = "DTO which contains a list of genres "
)
public record GenresResponseDto(@NotBlank
                                List<GenreDto> content,
                                @NotBlank
                                @Schema(description = "message", example = "Success")
                                String message

) {

}
