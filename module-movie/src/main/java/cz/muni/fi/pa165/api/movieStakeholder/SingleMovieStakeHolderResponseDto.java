package cz.muni.fi.pa165.api.movieStakeholder;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;

@Schema(
        title = "SingleMovieStakeHolderResponseDto",
        description = "Contains detailed movie stakeholder resonse"
)
public record SingleMovieStakeHolderResponseDto(@NotBlank
                                                BasicMovieStakeHolderDto content,
                                                @NotBlank
                                                @Schema(description = "Additional message", example = "Success")
                                                String message
                                                ) {
}
