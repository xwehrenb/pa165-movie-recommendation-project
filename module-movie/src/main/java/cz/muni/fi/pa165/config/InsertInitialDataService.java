package cz.muni.fi.pa165.config;

import cz.muni.fi.pa165.data.model.Genre;
import cz.muni.fi.pa165.data.model.Image;
import cz.muni.fi.pa165.data.model.Movie;
import cz.muni.fi.pa165.data.model.MovieStakeHolder;
import cz.muni.fi.pa165.data.repository.GenreRepository;
import cz.muni.fi.pa165.data.repository.ImageRepository;
import cz.muni.fi.pa165.data.repository.MovieRepository;
import cz.muni.fi.pa165.data.repository.StakeHolderRepository;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Transactional
@Service
public class InsertInitialDataService {

    @Value("${seed.mode}") // Set the seed mode so the tests can be more atomic
    private String seedMode;
    private final MovieRepository movieRepository;
    private final StakeHolderRepository stakeHolderRepository;
    private final GenreRepository genreRepository;
    private final ImageRepository imageRepository;

    @Autowired
    public InsertInitialDataService(MovieRepository movieRepository, StakeHolderRepository stakeHolderRepository, GenreRepository categoryRepository, ImageRepository imageRepository) {
        this.movieRepository = movieRepository;
        this.stakeHolderRepository = stakeHolderRepository;
        this.genreRepository = categoryRepository;
        this.imageRepository = imageRepository;
    }

    @PostConstruct
    public void insertInitialData() {
        System.out.println("Seed mode " + seedMode);
        if (seedMode.equals("test")) {
            insertAMovie();
            insertExtraMovieStakeHolders();
            insertExtraImages();
            insertExtraGenres();
        } else if (seedMode.equals("prod")) {
            generateRandomMovies();
        }

    }

    void generateRandomMovies() {
        String[] genreNames = {"Action", "Adventure", "Comedy", "Drama", "Horror",
                "Sci-Fi", "Thriller", "Romance", "Crime", "Fantasy"};

        List<Genre> genres = new ArrayList<>();

        for (String genreName : genreNames) {
            Genre genre = new Genre();
            genre.setName(genreName);
            genreRepository.saveAndFlush(genre);
            genres.add(genre);
        }

        List<MovieStakeHolder> directors = new ArrayList<>(25);

        String[][] directorNames = {
                {"Benjamin", "Fletcher"},
                {"Olivia", "Robertson"},
                {"Isaac", "Bradley"},
                {"Madeline", "Lawson"},
                {"Samuel", "Thompson"},
                {"Lily", "Austin"},
                {"Harrison", "Sanders"},
                {"Grace", "McKenzie"},
                {"Zachary", "Daniels"},
                {"Victoria", "Gardner"},
                {"Oliver", "Sherman"},
                {"Amelia", "Hudson"},
                {"Xavier", "Andrews"},
                {"Charlotte", "Stevens"},
                {"William", "Riley"},
                {"Evelyn", "Porter"},
                {"Thomas", "Wheeler"},
                {"Elizabeth", "Schmidt"},
                {"Alexander", "Burton"},
                {"Sophia", "Holmes"},
                {"Sebastian", "Wade"},
                {"Abigail", "Day"},
                {"Julian", "Greene"},
                {"Emma", "Warren"},
                {"Theodore", "Francis"}
        };

        for (String[] directorName : directorNames) {
            MovieStakeHolder director = new MovieStakeHolder();
            director.setName(directorName[0]);
            director.setSurname(directorName[1]);
            stakeHolderRepository.save(director);
            directors.add(director);
        }

        String[][] actorNames = {
                {"Elijah", "Reed"},
                {"Ava", "Hunt"},
                {"Mason", "Barnes"},
                {"Sophia", "Mills"},
                {"Michael", "Price"},
                {"Isabella", "Holland"},
                {"Lucas", "Woods"},
                {"Mia", "Stone"},
                {"Nathan", "Spencer"},
                {"Harper", "Manning"},
                {"Aiden", "Hale"},
                {"Grace", "Cunningham"},
                {"James", "Brady"},
                {"Layla", "Richards"},
                {"Benjamin", "Pearson"},
                {"Natalie", "Ellis"},
                {"Sebastian", "Higgins"},
                {"Zoe", "Cobb"},
                {"Cameron", "Fitzgerald"},
                {"Aubrey", "Nash"},
                {"Jack", "Walters"},
                {"Penelope", "Barker"},
                {"Alexander", "Reeves"},
                {"Samantha", "Sharp"},
                {"Grayson", "Copeland"},
                {"Piper", "Booth"},
                {"Liam", "Horton"},
                {"Isla", "Rose"},
                {"Carter", "Maddox"},
                {"Sadie", "Wade"},
                {"Henry", "Page"},
                {"Stella", "Winters"},
                {"Wyatt", "Gilbert"},
                {"Violet", "Frost"},
                {"Owen", "Buckley"},
                {"Hazel", "Bates"},
                {"Gabriel", "Bass"},
                {"Kennedy", "Gaines"},
                {"William", "Pruitt"},
                {"Kinsley", "McIntyre"},
                {"Connor", "Beard"},
                {"Delilah", "Dorsey"},
                {"Oliver", "Duke"},
                {"Paisley", "Hess"},
                {"Landon", "Hutchinson"},
                {"Ivy", "Blevins"},
                {"Ethan", "Mayer"},
                {"Norah", "Nichols"},
                {"Joseph", "McNeil"},
                {"Riley", "Hoffman"},
                {"Matthew", "Schultz"},
                {"Madeline", "Brock"},
                {"Xavier", "Ross"},
                {"Cora", "McDowell"},
                {"Andrew", "Kane"},
                {"Scarlett", "Ashley"},
                {"Joshua", "Curry"},
                {"Aria", "Braun"},
                {"Samuel", "Sims"},
                {"Savannah", "Moses"},
                {"John", "McLean"},
                {"Amelia", "Petersen"},
                {"Anthony", "Bentley"},
                {"Emilia", "Little"},
                {"Christopher", "Crane"},
                {"Quinn", "Reeves"},
                {"Hudson", "Merritt"},
                {"Ariana", "Pugh"},
                {"Isaiah", "Riggs"},
                {"Kaylee", "Hodge"},
                {"Adrian", "House"},
                {"Sophie", "Fleming"},
                {"Robert", "Harrison"},
                {"Alyssa", "Bryan"},
                {"Zachary", "Vaughn"},
                {"Lily", "McCall"},
                {"Caleb", "Hurst"},
                {"Lucy", "Wilkinson"},
                {"Thomas", "Barton"},
                {"Tristan", "Finch"},
                {"Lila", "Randall"},
                {"Colin", "Carr"},
                {"Vivian", "Guthrie"},
                {"Dominic", "Dickson"},
                {"Willow", "Clements"},
                {"Leon", "Frye"},
                {"Eloise", "Crosby"},
                {"Oscar", "Barr"},
                {"Luna", "Parks"},
                {"Nicolas", "McPherson"},
                {"Phoebe", "Potts"},
                {"Julian", "Cortez"},
                {"Adeline", "Holder"},
                {"Vincent", "Wilder"},
                {"Hadley", "Dunlap"},
                {"Jeremy", "Cooke"},
                {"Iris", "Meadows"},
                {"Wesley", "Miles"},
                {"Reagan", "Clay"},
                {"Beau", "Morrow"},
                {"Sienna", "Drake"}
        };

        List<MovieStakeHolder> actors = new ArrayList<>(100);

        for (String[] actorName : actorNames) {
            MovieStakeHolder actor = new MovieStakeHolder();
            actor.setName(actorName[0]);
            actor.setSurname(actorName[1]);
            stakeHolderRepository.saveAndFlush(actor);
            actors.add(actor);
        }

        String[] movieNames = {
                "A Whisper of Stars",
                "Time's Embrace",
                "Fury Unleashed",
                "Storms of Destiny",
                "Mystery of the Ancients",
                "Shadow's Descent",
                "A Dream of Twilight",
                "Serpents of Silence",
                "Wind's Redemption",
                "A Dance with Shadows",
                "The Lost Echo",
                "Whispers of Eternity",
                "The Dreamer's Key",
                "Sea of Whispers",
                "The Secret of the Forgotten",
                "Moonlit Oath",
                "The Edge of Time",
                "Frostfire",
                "Eternal Embers",
                "Children of the Void",
                "The Sundered Throne",
                "Embers of the Wild",
                "The Shadow's Veil",
                "A Forest of Glass",
                "The Shattered Realm",
                "A Symphony of Darkness",
                "The Ephemeral Dream",
                "The Last Enchantment",
                "A Labyrinth of Secrets",
                "The Shadow's Lament",
                "The Forbidden Artifact",
                "The Silent Oath",
                "The Cursed Chalice",
                "The Phoenix Prophecy",
                "The Stormweaver's Legacy",
                "The Crimson Spell",
                "The Tides of Memory",
                "The Void's Embrace",
                "The Forgotten Labyrinth",
                "The Eternal Mirage",
                "The Enchanted Forest",
                "The Midnight Sun",
                "The Tower of Whispers",
                "The Sorrowful Sea",
                "The Ocean's Fury",
                "The Frozen Heart",
                "The Soul's Requiem",
                "The Song of Shadows",
                "The Serpent's Curse",
                "The Dreaming Tide",
                "The Celestial Flame",
                "The Dreamer's Voyage",
                "The Whispers of the Wind",
                "The Forsaken Crown",
                "The Sun's Descent",
                "The Eternal Storm",
                "The Silent Garden",
                "The Shadow's Dance",
                "The Lost Chalice",
                "The Edge of Infinity",
                "The Enchanted Citadel",
                "The Void's Requiem",
                "The Timeless Mirror",
                "The Forgotten Storm",
                "The Ocean's Embrace",
                "The Crystal Key",
                "The Shattered Moon",
                "The Eternal Enigma",
                "The Shrouded World",
                "The Starlit Oath",
                "The Scarlet Tempest",
                "The Echoes of Twilight",
                "The Whispering Sands",
                "The Radiant Darkness",
                "The Spirit's Lament",
                "The Solstice Prophecy",
                "The Sapphire Dream",
                "The Winds of Illusion",
                "The Enchanted Grotto",
                "The Shadow's Legacy",
                "The Endless Abyss",
                "The Celestial Serenade",
                "The Sea of Stars",
                "The Storm's Lullaby",
                "The Ephemeral Ocean",
                "The Dreaming Sky",
                "The Timeless Voyage",
                "The Hidden Symphony",
                "The Veil of Memories",
                "The Silent Eclipse",
                "The Forsaken Labyrinth",
                "The Sea's Requiem",
                "The Garden of Shadows",
                "The Dreamer's Enchantment",
                "The Path of Stars",
                "The Eternal Sun",
                "The Phoenix's Descent",
                "The Whispers of the Void",
                "The Dance of Dreams",
                "The Edge of the Abyss",
                "Unseen Mirage"
        };

        for (String movieName : movieNames) {
            Random random = new Random();
            Movie movie = new Movie();
            movie.setTitle(movieName);
            movie.setYear(2000 + random.nextInt(20));

            MovieStakeHolder director = directors.get(new Random().nextInt(directors.size()));
            movie.setDirector(director);

            Set<MovieStakeHolder> movieActors = new HashSet<>();
            int numActors = 4 + new Random().nextInt(3);

            while (movieActors.size() < numActors) {
                MovieStakeHolder actor = actors.get(random.nextInt(actors.size()));
                movieActors.add(actor);
            }
            movie.setActors(movieActors);

            Set<Genre> movieGenres = new HashSet<>();
            int numGenres = 1 + random.nextInt(3);
            while (movieGenres.size() < numGenres) {
                Genre genre = genres.get(random.nextInt(genres.size()));
                movieGenres.add(genre);
            }
            movie.setCategories(movieGenres);

            Set<Image> movieImages = new HashSet<>();
            for(int i = 0; i < 3; i++) {
                Image image2 = new Image();
                image2.setUrl("www.random.com/image_" + movieName.replace(' ', '_') + i);
                image2.setDescription("Nice image");
                imageRepository.saveAndFlush(image2);
                movieImages.add(image2);
            }
            movie.setImages(movieImages);
            movieRepository.save(movie);
        }
    }

    void insertAMovie() {
        MovieStakeHolder director = new MovieStakeHolder();
        director.setName("Francis");
        director.setSurname("Coppola");
        stakeHolderRepository.saveAndFlush(director);

        MovieStakeHolder actor = new MovieStakeHolder();
        actor.setName("Al");
        actor.setSurname("Pacino");
        stakeHolderRepository.saveAndFlush(actor);

        Genre genre = new Genre();
        genre.setName("Drama");
        genreRepository.saveAndFlush(genre);

        Image image1 = new Image();
        image1.setUrl("www.random.com/image1");
        image1.setDescription("Nice title image");
        imageRepository.saveAndFlush(image1);

        Image image2 = new Image();
        image2.setUrl("www.random.com/image2");
        image2.setDescription("Nice image");
        imageRepository.saveAndFlush(image2);

        Set<Genre> categories = new HashSet(Arrays.asList(genre));
        Set<Image> images = new HashSet(Arrays.asList(image1, image2));

        Movie movie = new Movie();
        movie.setTitle("The Godfather");
        movie.setYear(2009);
        movie.setDirector(director);
        movie.addActor(actor);
        movie.setImages(images);
        movie.addGenre(genre);
        movieRepository.save(movie);

    }

    public void insertExtraMovieStakeHolders() {
        MovieStakeHolder actor = new MovieStakeHolder();
        actor.setName("Marlon");
        actor.setSurname("Brando");
        stakeHolderRepository.save(actor);

        MovieStakeHolder actor1 = new MovieStakeHolder();
        actor1.setName("Jason");
        actor1.setSurname("Momoa");
        stakeHolderRepository.save(actor1);

        MovieStakeHolder actor2 = new MovieStakeHolder();
        actor2.setName("Amber");
        actor2.setSurname("Heard");
        stakeHolderRepository.save(actor2);


    }

    public void insertExtraImages() {
        Image image1 = new Image();
        image1.setUrl("www.random.com/image3");
        image1.setDescription("Ugly title image");

        Image image2 = new Image();
        image2.setUrl("www.random.com/image4");
        image2.setDescription("Low res image");

        imageRepository.save(image1);
        imageRepository.save(image2);
    }

    private void insertExtraGenres() {
        Genre genre1 = new Genre();
        genre1.setName("Thriller");
        genreRepository.save(genre1);

        Genre genre2 = new Genre();
        genre2.setName("Comedy");
        genreRepository.save(genre2);

        Genre genre3 = new Genre();
        genre3.setName("Romance");
        genreRepository.save(genre3);
    }

}
