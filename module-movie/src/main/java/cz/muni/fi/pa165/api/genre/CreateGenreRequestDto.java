package cz.muni.fi.pa165.api.genre;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;

public record CreateGenreRequestDto(
        @NotBlank
        @Schema(
                description = "Category Name",
                example = "Thriller"
        )
        String name
) {
}
