package cz.muni.fi.pa165.rest;

import cz.muni.fi.pa165.api.ErrorMessage;
import cz.muni.fi.pa165.api.MessageResponseDto;
import cz.muni.fi.pa165.api.movieStakeholder.*;
import cz.muni.fi.pa165.config.Routes;
import cz.muni.fi.pa165.exceptions.ApiException;
import cz.muni.fi.pa165.facade.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = Routes.STAKEHOLDERS)
public class MovieStakeHolderRestController {

    private final MovieStakeHolderFacade movieStakeHolderFacade;

    @Autowired
    public MovieStakeHolderRestController(MovieStakeHolderFacade movieStakeHolderFacade) {
        this.movieStakeHolderFacade = movieStakeHolderFacade;
    }

    @Operation(
            summary = "Create a new movie stakeholder.",
            description = """
                    Create a new movie stakeholder.
                    """,
            responses = {
                    @ApiResponse(responseCode = "201", content = @Content(schema = @Schema(implementation = SingleMovieStakeHolderResponseDto.class))),
                    @ApiResponse(responseCode = "400", description = "Movie Stakeholder could not be created",
                            content = @Content(schema = @Schema(implementation = ErrorMessage.class))),
                    @ApiResponse(responseCode = "500", description = "An error occurred",
                            content = @Content(schema = @Schema(implementation = ErrorMessage.class)))
            })
    @SecurityRequirement(name = "OAuth2")
    @PostMapping("")
    @ResponseStatus(HttpStatus.CREATED)
    public SingleMovieStakeHolderResponseDto createMovieStakeHolder(@RequestBody CreateMovieStakeHolderRequestDto createMovieStakeHolderRequestDto) {
        if(createMovieStakeHolderRequestDto.name().length() < 2) {
            throw new ApiException(
                    "Name is too short",
                    HttpStatus.BAD_REQUEST,
                    Routes.STAKEHOLDER_SINGLE
            );
        }
        if(createMovieStakeHolderRequestDto.surname().length() < 2) {
            throw new ApiException(
                    "Surname is too short",
                    HttpStatus.BAD_REQUEST,
                    Routes.STAKEHOLDER_SINGLE
            );
        }
        return new SingleMovieStakeHolderResponseDto(movieStakeHolderFacade.create(createMovieStakeHolderRequestDto, Routes.STAKEHOLDERS), "Movie stake holder successfully created.");
    }

    @Operation(
            summary = "Get all movie stakeholders.",
            description = """
                    Get all movie stakeholders.
                    """,
            responses = {
                    @ApiResponse(responseCode = "200",
                            content = @Content(mediaType = "application/json",schema = @Schema(implementation = MovieStakeHoldersResponseDto.class))),
                    @ApiResponse(responseCode = "500", description = "An error occurred",
                            content = @Content(schema = @Schema(implementation = ErrorMessage.class)))
            })
    @GetMapping("")
    public MovieStakeHoldersResponseDto getAllMovieStakeHolders() {
        return new MovieStakeHoldersResponseDto(movieStakeHolderFacade.getAllMovieStakeHolders(Routes.STAKEHOLDERS), "Success");
    }

    @Operation(
            summary = "Get all movie stakeholders by page.",
            description = """
                    Get all movie stakeholders by page.
                    """,
            responses = {
                    @ApiResponse(responseCode = "200", content = @Content(mediaType = "application/json",schema = @Schema(implementation = PagedMovieStakeHolderResponseDto.class))),
                    @ApiResponse(responseCode = "500", description = "An error occurred",
                            content = @Content(schema = @Schema(implementation = ErrorMessage.class)))
            })
    @GetMapping(Routes.STAKEHOLDERS_PAGED)
    public PagedMovieStakeHolderResponseDto getPageOfMovieStakeHolders(@ParameterObject Pageable pageable) {
        Page<BasicMovieStakeHolderDto> result = movieStakeHolderFacade.getPageOfMovieStakeHolders(pageable, Routes.STAKEHOLDERS + Routes.STAKEHOLDERS_PAGED);
        return new PagedMovieStakeHolderResponseDto(result.getContent(), result.getTotalElements(), "Success");
    }

    @Operation(
            summary = "Update movie stakeholder",
            description = """
                    Update movie stakeholder.
                    """,
            responses = {
                    @ApiResponse(responseCode = "200", content = @Content(mediaType = "application/json",schema = @Schema(implementation = SingleMovieStakeHolderResponseDto.class))),
                    @ApiResponse(responseCode = "404", description = "Movie Stakeholder could not be updated",
                            content = @Content(schema = @Schema(implementation = ErrorMessage.class))),
                    @ApiResponse(responseCode = "500", description = "An error occurred",
                            content = @Content(schema = @Schema(implementation = ErrorMessage.class)))
            }
    )
    @SecurityRequirement(name = "OAuth2")
    @PutMapping(Routes.STAKEHOLDER_SINGLE)
    @ResponseStatus(HttpStatus.OK)
    public SingleMovieStakeHolderResponseDto updateMovieStakeHolder(@PathVariable Long id, @RequestBody CreateMovieStakeHolderRequestDto createMovieStakeHolderRequestDto) {
        return new SingleMovieStakeHolderResponseDto(movieStakeHolderFacade.update(id, createMovieStakeHolderRequestDto, Routes.STAKEHOLDERS + Routes.STAKEHOLDER_SINGLE), "Movie stakeholder successfully updated.");
    }

    @Operation(
            summary = "Delete movie stakeholder",
            description = """
                    Delete movie stakeholder.
                    """,
            responses = {
                    @ApiResponse(responseCode = "200", content = @Content(mediaType = "application/json",schema = @Schema(implementation = MessageResponseDto.class))),
                    @ApiResponse(responseCode = "404", description = "Movie Stakeholder could not be deleted",
                            content = @Content(schema = @Schema(implementation = ErrorMessage.class))),
                    @ApiResponse(responseCode = "500", description = "An error occurred",
                            content = @Content(schema = @Schema(implementation = ErrorMessage.class)))
            }
            )
    @SecurityRequirement(name = "OAuth2")
    @DeleteMapping(Routes.STAKEHOLDER_SINGLE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public MessageResponseDto deleteMovieStakeHolder(@PathVariable Long id) {
        movieStakeHolderFacade.delete(id, Routes.STAKEHOLDERS + Routes.STAKEHOLDER_SINGLE);
        return new MessageResponseDto("Movie stakeholder successfully deleted.");
    }

}
