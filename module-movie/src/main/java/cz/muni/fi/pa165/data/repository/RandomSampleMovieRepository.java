package cz.muni.fi.pa165.data.repository;

import cz.muni.fi.pa165.data.model.Movie;

import java.util.List;
import java.util.Optional;


public interface RandomSampleMovieRepository {
    List<Movie> getRandomMovies(Optional<Long> optionalCount);
}
