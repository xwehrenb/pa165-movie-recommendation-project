package cz.muni.fi.pa165.service;


import cz.muni.fi.pa165.data.model.MovieStakeHolder;
import cz.muni.fi.pa165.data.repository.StakeHolderRepository;
import cz.muni.fi.pa165.exceptions.ApiException;
import cz.muni.fi.pa165.api.movieStakeholder.CreateMovieStakeHolderRequestDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class MovieStakeHolderService {

    private final StakeHolderRepository movieStakeHolderRepository;

    public MovieStakeHolderService(StakeHolderRepository movieStakeHolderRepository) {
        this.movieStakeHolderRepository = movieStakeHolderRepository;
    }

    @Transactional(readOnly = true)
    public List<MovieStakeHolder> getAllMovieStakeHolders(String route) {
        try {
            return movieStakeHolderRepository.findAll();
        } catch (Exception e) {
            throw new ApiException("An unexpected error occurred.", HttpStatus.INTERNAL_SERVER_ERROR, route);
        }

    }

    @Transactional(readOnly = true)
    public Page<MovieStakeHolder> getPageOfMovieStakeHolders(Pageable pageable, String route) {
        try {
            return movieStakeHolderRepository.findAll(pageable);
        } catch (Exception e) {
            throw new ApiException("An unexpected error occurred.", HttpStatus.INTERNAL_SERVER_ERROR, route);
        }

    }

    @Transactional
    public MovieStakeHolder create(CreateMovieStakeHolderRequestDto movieStakeHolder, String route) {
        MovieStakeHolder msh = new MovieStakeHolder();
        msh.setName(movieStakeHolder.name());
        msh.setSurname(movieStakeHolder.surname());
        try {
            msh = movieStakeHolderRepository.save(msh);
            movieStakeHolderRepository.flush();
        } catch (Exception e) {
            throw new ApiException("An unexpected error occurred.", HttpStatus.INTERNAL_SERVER_ERROR, route);
        }

        return msh;
    }

    @Transactional
    public MovieStakeHolder update(Long id, CreateMovieStakeHolderRequestDto movieStakeHolder, String route) {
        try {
            MovieStakeHolder msh = movieStakeHolderRepository.findById(id)
                    .orElseThrow(() -> new ApiException("Movie stake holder not found", HttpStatus.NOT_FOUND, route));
            msh.setName(movieStakeHolder.name());
            msh.setSurname(movieStakeHolder.surname());
            movieStakeHolderRepository.save(msh);
            movieStakeHolderRepository.flush();
            return msh;
        } catch (ApiException e) {
            throw e;
        } catch (Exception e) {
            throw new ApiException("An unexpected error occurred.", HttpStatus.INTERNAL_SERVER_ERROR, route);
        }


    }

    @Transactional
    public void delete(Long id, String route) {
        try {
            if(!movieStakeHolderRepository.existsById(id)) {
                throw new ApiException("Movie stake holder not found", HttpStatus.NOT_FOUND, route);
            }
            movieStakeHolderRepository.deleteById(id);
        } catch (ApiException e) {
            throw e;
        } catch (Exception e) {
            throw new ApiException("An unexpected error occurred.", HttpStatus.INTERNAL_SERVER_ERROR, route);
        }

    }
}
