package cz.muni.fi.pa165.api.auth;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import org.springframework.core.ParameterizedTypeReference;

@Schema(title = "Single response", description = "Response of a single item")
public record SingleResponse<T>(
        T item,
        @Schema(description = "Message", example = "Success")
        String message
) {
}
