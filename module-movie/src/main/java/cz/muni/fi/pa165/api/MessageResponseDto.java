package cz.muni.fi.pa165.api;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;

@Schema(title = "Simple message")
public record MessageResponseDto(@NotBlank
                              @Schema(description = "message", example = "Success")
                              String message
                              )
{ }
