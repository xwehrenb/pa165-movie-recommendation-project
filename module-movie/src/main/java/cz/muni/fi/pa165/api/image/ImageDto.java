package cz.muni.fi.pa165.api.image;

import cz.muni.fi.pa165.data.model.Image;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;

@Schema(
        title = "ImageDto",
        description = "Dto for image"
)
public record ImageDto(
        @NotBlank
        @Schema(description = "Genre Id", example = "1")
        Long id,
        @NotBlank
        @Schema(description = "Url")
        String url,
        @NotBlank
        @Schema(description = "Description")
        String description
) {
        public static ImageDto from(Image image) {
            return new ImageDto(image.getId(), image.getUrl(), image.getDescription());
        }
}
