package cz.muni.fi.pa165.facade;

import cz.muni.fi.pa165.api.genre.CreateGenreRequestDto;
import cz.muni.fi.pa165.api.genre.GenreDto;
import cz.muni.fi.pa165.service.GenreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class GenreFacade {
    private final GenreService genreService;

    @Autowired
    public GenreFacade(GenreService genreService) {
        this.genreService = genreService;
    }

    @Transactional(readOnly = true)
    public List<GenreDto> findAllGenres(String route) {
        return genreService.findAllGenres(route).stream().map((genre) -> GenreDto.from(genre)).toList();
    }

    @Transactional
    public GenreDto createGenre(CreateGenreRequestDto genre, String route) {
        return GenreDto.from(genreService.create(genre, route));
    }

    @Transactional(readOnly = true)
    public Page<GenreDto> getPageOfGenres(Pageable pageable, String route) {
        return genreService.getPageOfGenres(pageable, route).map(GenreDto::from);
    }

    @Transactional
    public GenreDto update(Long id, CreateGenreRequestDto createGenreRequestDto, String route) {
        return GenreDto.from(genreService.update(id, createGenreRequestDto, route));
    }

    @Transactional
    public void delete(Long id, String route) {
        genreService.delete(id, route);
    }

}
