package cz.muni.fi.pa165.exceptions;

import org.springframework.http.HttpStatus;

public class ApiException extends RuntimeException {

    private final HttpStatus httpStatus;
    private final String route;

    public ApiException(String message, HttpStatus httpStatus, String route) {
        super(message);
        this.httpStatus = httpStatus;
        this.route = route;
    }

    public ApiException(String message, String route, HttpStatus httpStatus, Throwable cause) {
        super(message, cause);
        this.httpStatus = httpStatus;
        this.route = route;
    }

    public String getRoute() {
        return route;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

}
