package cz.muni.fi.pa165.api.movie;


import cz.muni.fi.pa165.api.movieStakeholder.BasicMovieStakeHolderDto;
import cz.muni.fi.pa165.data.model.Movie;
import cz.muni.fi.pa165.api.genre.GenreDto;
import cz.muni.fi.pa165.api.ImageDto;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;

@Schema(description = "Detailed movie dto object"

)
public record DetailedMovieDto(@NotBlank
                               @Schema(description = "Id", example = "1")
                               long id,
                               @NotBlank
                               @Schema(description = "Title", example = "The Godfather")
                               String title,
                               @NotBlank
                               @Schema(description = "Year", example = "1994")
                               int year,
                               @NotBlank
                               BasicMovieStakeHolderDto director,
                               @NotBlank
                               GenreDto[] genres,
                               @NotBlank
                               BasicMovieStakeHolderDto[] actors,
                               @NotBlank
                               ImageDto[] images
                               ) {
    public static DetailedMovieDto from(Movie movie) {
        return new DetailedMovieDto(
                movie.getId(),
                movie.getTitle(),
                movie.getYear(),
                BasicMovieStakeHolderDto.from(movie.getDirector()),
                movie.getCategories().stream().map((GenreDto::from)).toArray(GenreDto[]::new),
                movie.getActors().stream().map(BasicMovieStakeHolderDto::from).toArray(BasicMovieStakeHolderDto[]::new),
                movie.getImages().stream().map(ImageDto::from).toArray(ImageDto[]::new)
        );
    }

}
