package cz.muni.fi.pa165.api.image;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;

@Schema(
        title = "CreateImageRequestDto",
        description = "Dto for creation of new image"
)
public record CreateImageRequestDto(
        @NotBlank
        @Schema(description = "Url")
        String url,
        @NotBlank
        @Schema(description = "Description")
        String description
) {
}
