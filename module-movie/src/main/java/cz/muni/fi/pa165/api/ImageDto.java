package cz.muni.fi.pa165.api;

import cz.muni.fi.pa165.data.model.Image;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;

@Schema(title = "chat message",
        description = "represents a message in a chat"
)
public record ImageDto(@NotBlank
                       @Schema(description = "Id", example = "1")
                       Long id,
                       @NotBlank
                       @Schema(description = "Image url", example = "http://example.com/image.png")
                       String imageUrl,
                       @NotBlank
                       @Schema(description = "Description", example = "Example description")
                       String description
                      ) {
    public static ImageDto from(Image image) {
        return new ImageDto(
                image.getId(),
                image.getUrl(),
                image.getDescription()
        );
    }

}
