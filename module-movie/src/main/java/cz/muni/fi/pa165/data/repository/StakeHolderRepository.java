package cz.muni.fi.pa165.data.repository;

import cz.muni.fi.pa165.data.model.MovieStakeHolder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;
import java.util.Set;

public interface StakeHolderRepository extends JpaRepository<MovieStakeHolder, Long> {

    @Query("SELECT m FROM MovieStakeHolder m WHERE m.id =:id")
    Optional<MovieStakeHolder> findById(@Param("id") Long id);

    @Query("SELECT s FROM MovieStakeHolder s WHERE s.id IN :ids")
    Set<MovieStakeHolder> findAllByIdIn(@Param("ids") Set<Long> ids);

}
