package cz.muni.fi.pa165.data.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;

import java.io.Serializable;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "movie_stake_holder")
public class MovieStakeHolder implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "surname")
    private String surname;

    @JsonBackReference
    @OneToMany(fetch = FetchType.LAZY,
            mappedBy = "director",
            cascade = { CascadeType.ALL
    })
    private Set<Movie> directedMovies;

    @JsonBackReference
    @ManyToMany(mappedBy = "actors", fetch = FetchType.LAZY)
    private Set<Movie> actedIn;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Set<Movie> getDirectedMovies() {
        return directedMovies;
    }

    public void setDirectedMovies(Set<Movie> directedMovies) {
        this.directedMovies = directedMovies;
    }

    public Set<Movie> getActedIn() {
        return actedIn;
    }

    public void setActedIn(Set<Movie> actedIn) {
        this.actedIn = actedIn;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MovieStakeHolder that)) return false;

        if (!name.equals(that.name)) return false;
        if (!surname.equals(that.surname)) return false;
        if (!Objects.equals(directedMovies, that.directedMovies))
            return false;
        return Objects.equals(actedIn, that.actedIn);
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + surname.hashCode();
        return result;
    }
}
