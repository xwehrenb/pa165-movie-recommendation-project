package cz.muni.fi.pa165.api.movieStakeholder;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;

public record CreateMovieStakeHolderRequestDto(@NotBlank
                                               @Schema(description = "Name", example = "Jonathan")
                                               String name,
                                               @NotBlank
                                               @Schema(description = "Surname", example = "Nolan")
                                               String surname
                                               ) {
}
