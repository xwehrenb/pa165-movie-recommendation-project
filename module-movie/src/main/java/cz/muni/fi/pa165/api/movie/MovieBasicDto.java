package cz.muni.fi.pa165.api.movie;

import cz.muni.fi.pa165.data.model.Genre;
import cz.muni.fi.pa165.data.model.Movie;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;

@Schema(title = "Basic movie",
        description = "Basic movie")
public record MovieBasicDto (@NotBlank
                             @Schema(description = "id", example = "1")
                             long id,
                             @NotBlank
                             @Schema(description = "title", example = "The Godfather")
                             String title,
                             @NotBlank
                             @Schema(description = "year", example = "1994")
                             int year,
                             @NotBlank
                             @Schema(description = "director_name", example = "1")
                             String director_name,
                             @NotBlank
                             @Schema(description = "genres", example = "[Thriller, Drama]")
                             String[] genres
                            ) {
    public static MovieBasicDto from(Movie movie) {
        System.out.println(movie.getCategories().size());
        System.out.println(movie.getActors().size());
        return new MovieBasicDto(
                movie.getId(),
                movie.getTitle(),
                movie.getYear(),
                movie.getDirector().getName() + " " + movie.getDirector().getSurname(),
                movie.getCategories().stream().map((Genre::getName)).toArray(String[]::new)
        );
    }

}
