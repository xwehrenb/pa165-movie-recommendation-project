package cz.muni.fi.pa165.facade;

import cz.muni.fi.pa165.api.movieStakeholder.BasicMovieStakeHolderDto;
import cz.muni.fi.pa165.api.movieStakeholder.CreateMovieStakeHolderRequestDto;
import cz.muni.fi.pa165.service.MovieStakeHolderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class MovieStakeHolderFacade {
    private final MovieStakeHolderService movieStakeHolderService;

    @Autowired
    public MovieStakeHolderFacade(MovieStakeHolderService movieStakeHolderService) {
        this.movieStakeHolderService = movieStakeHolderService;
    }

    @Transactional
    public BasicMovieStakeHolderDto create(CreateMovieStakeHolderRequestDto movieStakeHolderRequestDto, String route) {
        return BasicMovieStakeHolderDto.from(movieStakeHolderService.create(movieStakeHolderRequestDto, route));
    }

    @Transactional(readOnly = true)
    public List<BasicMovieStakeHolderDto> getAllMovieStakeHolders(String route) {
        return movieStakeHolderService.getAllMovieStakeHolders(route).stream().map(BasicMovieStakeHolderDto::from).toList();
    }

    @Transactional(readOnly = true)
    public Page<BasicMovieStakeHolderDto> getPageOfMovieStakeHolders(Pageable pageable, String route) {
        return movieStakeHolderService.getPageOfMovieStakeHolders(pageable, route).map(BasicMovieStakeHolderDto::from);
    }

    @Transactional
    public BasicMovieStakeHolderDto update(Long id, CreateMovieStakeHolderRequestDto createMovieStakeHolderRequestDto, String route) {
        return BasicMovieStakeHolderDto.from(movieStakeHolderService.update(id, createMovieStakeHolderRequestDto, route));
    }

    @Transactional
    public void delete(Long id, String route) {
        movieStakeHolderService.delete(id, route);
    }
}
