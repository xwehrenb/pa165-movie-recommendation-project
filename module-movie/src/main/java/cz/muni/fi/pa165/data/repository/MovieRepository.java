package cz.muni.fi.pa165.data.repository;

import cz.muni.fi.pa165.data.model.Movie;
import cz.muni.fi.pa165.data.model.MovieStakeHolder;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;


@Repository
public interface MovieRepository extends JpaRepository<Movie, Long>, RandomSampleMovieRepository {

    @Query("SELECT m FROM Movie m LEFT JOIN FETCH m.actors a LEFT JOIN FETCH m.director d LEFT JOIN FETCH m.images i WHERE m.id = :id")
    Optional<Movie> findById(@Param("id") Long id);

    @Query(value =
            "SELECT DISTINCT m FROM Movie m LEFT JOIN FETCH m.actors a LEFT JOIN FETCH m.director d LEFT JOIN FETCH m.images i LEFT JOIN FETCH m.genres g WHERE (:genreId is null OR :genreId = g.id) AND (:directorId IS NULL OR :directorId = d.id) AND (:actorId is null OR :actorId = a.id)",
            countQuery = "SELECT COUNT(DISTINCT m) FROM Movie m LEFT JOIN m.actors a LEFT JOIN m.director d LEFT JOIN m.images i LEFT JOIN m.genres g WHERE (:genreId is null OR :genreId = g.id) AND (:directorId IS NULL OR :directorId = d.id) AND (:actorId is null OR :actorId = a.id)"
    )
    Page<Movie> findByCriteriaPaged(@Param("genreId") Optional<Long> genreId, @Param("directorId") Optional<Long> directorId, @Param("actorId") Optional<Long> actorId, Pageable pageable);

    @Query("SELECT DISTINCT m FROM Movie m LEFT JOIN FETCH m.actors a LEFT JOIN FETCH m.director d LEFT JOIN FETCH m.images i LEFT JOIN FETCH m.genres g WHERE (:genreId is null OR :genreId = g.id) AND (:directorId IS NULL OR :directorId = d.id) AND (:actorId is null OR :actorId = a.id)")
    List<Movie> findByCriteria(@Param("genreId") Optional<Long> genreId, @Param("directorId") Optional<Long> directorId, @Param("actorId") Optional<Long> actorId);

}
