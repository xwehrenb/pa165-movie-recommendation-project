package cz.muni.fi.pa165.api.genre;

import cz.muni.fi.pa165.data.model.Genre;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;

@Schema(
        description= "Category"
)
public record GenreDto(@NotBlank
                       @Schema(description = "Genre Id", example = "1")
                       Long id,
                       @NotBlank
                          @Schema(
                                  description = "Genre Name",
                                  example = "Thriller"
                          )
                          String name

                         ) {
    public static GenreDto from(Genre genre) {
        return new GenreDto(genre.getId(), genre.getName());
    }
}
