package cz.muni.fi.pa165.data.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;

import java.io.Serializable;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "movie")
public class Movie implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id")
    private Long id;

    @Column(name = "title", length = 60, unique = true)
    private String title;

    @Column(name = "year")
    private int year;

    @Column(name = "description")
    private String description;

    @JsonManagedReference
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "director_id")
    private MovieStakeHolder director;

    @JsonManagedReference
    @ManyToMany(fetch = FetchType.LAZY,
                cascade = {CascadeType.MERGE})
    @JoinTable(name = "movie_stake_holder_role",
            joinColumns = @JoinColumn(name = "role_id"),
            inverseJoinColumns = @JoinColumn(name = "movie_stake_holder_id")
    )
    private Set<MovieStakeHolder> actors;

    @JsonManagedReference
    @ManyToMany(fetch = FetchType.LAZY,
            cascade = { CascadeType.MERGE})
    @JoinTable(
            name = "movie_genre",
            joinColumns = @JoinColumn(name = "movie_id")
    )
    private Set<Genre> genres;

    @JsonManagedReference
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "movie",
            cascade = { CascadeType.MERGE, CascadeType.REMOVE } )
    private Set<Image> images;

    public Long getId() {
        return id;
    }

    public Set<Genre> getCategories() {
        return genres;
    }

    public void setCategories(Set<Genre> categories) {
        this.genres = categories;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public MovieStakeHolder getDirector() {
        return director;
    }

    public void setDirector(MovieStakeHolder director) {
        this.director = director;
    }

    public Set<MovieStakeHolder> getActors() {
        return actors;
    }

    public void setActors(Set<MovieStakeHolder> actors) {
        this.actors = actors;
    }

    public Set<Image> getImages() {
        if (images == null) {
            return Set.of();
        }
        return images;
    }

    public void setImages(Set<Image> images) {
        this.images = images;
    }

    public void addGenre(Genre genre) {
        if (this.genres == null) {
            this.genres = new java.util.HashSet<>();
        }
        this.genres.add(genre);
    }

    public void addImage(Image image) {
        if (this.images == null) {
            this.images = new java.util.HashSet<>();
        }
        this.images.add(image);
    }

    public void addActor(MovieStakeHolder actor) {
        if (this.actors == null) {
            this.actors = new java.util.HashSet<>();
        }
        this.actors.add(actor);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Movie movie)) return false;

        if (year != movie.year) return false;
        if (!title.equals(movie.title)) return false;
        if (!description.equals(movie.description)) return false;
        if (!director.equals(movie.director)) return false;
        if (!Objects.equals(actors, movie.actors)) return false;
        if (!Objects.equals(genres, movie.genres)) return false;
        return Objects.equals(images, movie.images);
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + title.hashCode();
        result = 31 * result + year;
        result = 31 * result + (description != null ? description.hashCode() : 0);
        return result;
    }
}
