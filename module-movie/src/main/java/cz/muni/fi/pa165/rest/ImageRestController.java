package cz.muni.fi.pa165.rest;

import cz.muni.fi.pa165.api.ErrorMessage;
import cz.muni.fi.pa165.api.MessageResponseDto;
import cz.muni.fi.pa165.api.image.CreateImageRequestDto;
import cz.muni.fi.pa165.api.image.ImagesResponseDto;
import cz.muni.fi.pa165.api.image.SingleImageResponseDto;
import cz.muni.fi.pa165.config.Routes;
import cz.muni.fi.pa165.exceptions.ApiException;
import cz.muni.fi.pa165.facade.ImageFacade;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = Routes.IMAGES, produces = MediaType.APPLICATION_JSON_VALUE)
public class ImageRestController {

    private final ImageFacade imageFacade;

    @Autowired
    public ImageRestController(ImageFacade imageFacade) {
        this.imageFacade = imageFacade;
    }

    @Operation(
            summary = "Get all images",
            description = """
                    Get all images.
                    """,
            responses = {
                    @ApiResponse(responseCode = "200",  content = @Content(mediaType = "application/json",schema = @Schema(implementation = ImagesResponseDto.class))),
                    @ApiResponse(responseCode = "500", description = "An error occurred",
                            content = @Content(schema = @Schema(implementation = ErrorMessage.class)))
            }
    )
    @GetMapping("")
    public ImagesResponseDto getImages() {
        return new ImagesResponseDto(imageFacade.getAll(Routes.IMAGES), "Success");
    }

    @Operation(
            summary = "Get image by id",
            description = """
                    Get image by id.
                    """,
            responses = {
                    @ApiResponse(responseCode = "200",  content = @Content(mediaType = "application/json",schema = @Schema(implementation = SingleImageResponseDto.class))),
                    @ApiResponse(responseCode = "404", description = "Image not found", content = @Content(schema = @Schema(implementation = ErrorMessage.class))),
                    @ApiResponse(responseCode = "500", description = "An error occurred",
                            content = @Content(schema = @Schema(implementation = ErrorMessage.class)))
            }
    )
    @GetMapping(Routes.IMAGE_SINGLE)
    public SingleImageResponseDto getImageById(@PathVariable Long id) {
        return new SingleImageResponseDto(imageFacade.getById(id, Routes.IMAGES + Routes.IMAGE_SINGLE), "Success");
    }

    @Operation(
            summary = "Delete image by id",
            description = """
                    Delete image by id.
                    """,
            responses = {
                    @ApiResponse(responseCode = "204",  content = @Content(mediaType = "application/json",schema = @Schema(implementation = MessageResponseDto.class))),
                    @ApiResponse(responseCode = "404", description = "Image not found", content = @Content(schema = @Schema(implementation = ErrorMessage.class))),
                    @ApiResponse(responseCode = "500", description = "An error occurred",
                            content = @Content(schema = @Schema(implementation = ErrorMessage.class)))
            }
            )
    @SecurityRequirement(name = "Bearer Authentication")
    @DeleteMapping(Routes.IMAGE_SINGLE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public MessageResponseDto deleteImageById(@PathVariable Long id) {
        imageFacade.delete(id, Routes.IMAGES + Routes.IMAGE_SINGLE);
        return new MessageResponseDto("Success");
    }

    @Operation(
            summary = "Create image",
            description = """
                    Create image.
                    """,
            responses = {
                    @ApiResponse(responseCode = "201",  content = @Content(mediaType = "application/json",schema = @Schema(implementation = SingleImageResponseDto.class))),
                    @ApiResponse(responseCode = "400", description = "Bad request", content = @Content(schema = @Schema(implementation = ErrorMessage.class))),
                    @ApiResponse(responseCode = "500", description = "An error occurred",
                            content = @Content(schema = @Schema(implementation = ErrorMessage.class)))
            }
    )
    @SecurityRequirement(name = "OAuth2")
    @PostMapping("")
    @ResponseStatus(HttpStatus.CREATED)
    public SingleImageResponseDto createImage(@RequestBody CreateImageRequestDto image) {
        if(image.url().length() < 5) {
            throw new ApiException(
                    "Url must be at least 5 characters long.",
                    HttpStatus.BAD_REQUEST,
                    Routes.IMAGES
            );
        }
        return new SingleImageResponseDto(imageFacade.create(image, Routes.IMAGES), "Success");
    }
}
