package cz.muni.fi.pa165.api.movieStakeholder;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;

import java.util.List;

@Schema(
        title = "PagedMovieStakeHolderResponseDto",
        description = "Response which contains list of movie-stake-holders"
)
public record MovieStakeHoldersResponseDto(
        List<BasicMovieStakeHolderDto> content,
        @NotBlank
        @Schema(description = "Additional message", example = "Success")
        String message
) {
}
