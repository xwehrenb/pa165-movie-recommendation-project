package cz.muni.fi.pa165.data.repository;

import cz.muni.fi.pa165.data.model.Movie;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.TypedQuery;
import org.springframework.stereotype.Repository;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.LongStream;

@Repository
public class RandomSampleMovieRepositoryImpl implements RandomSampleMovieRepository {
    @PersistenceContext
    private EntityManager entityManager;

    private Long getMovieCount() {
        return entityManager.createQuery(
                "SELECT COUNT(m) FROM Movie m", Long.class
        ).getSingleResult();

    }

    private Set<Long> generateRandomOffsets(long max, long count) {
        if(max <= count) {
            return LongStream.range(0, max).boxed().collect(Collectors.toSet());
        }
        Random random = new Random();
        Set<Long> offsets = new HashSet<>();
        while (offsets.size() < count) {
            offsets.add(random.nextLong(max));
        }
        return offsets;
    }

    @Override
    public List<Movie> getRandomMovies(Optional<Long> optionalCount) {
        Long count = optionalCount.orElse(20l);
        Long max = getMovieCount();

        TypedQuery<Movie> query = entityManager.createQuery(
                "SELECT m FROM Movie m", Movie.class
        );

        if(count >= max) {
            return query.getResultList();
        }

        List<Movie> movies = new ArrayList<>(Math.toIntExact(count));
        Set<Long> offsets = generateRandomOffsets(max, count);

        for(Long offset : offsets) {
            query.setFirstResult(Math.toIntExact(offset));
            query.setMaxResults(1);
            movies.add(query.getSingleResult());
        }

        return movies;
    }
}
