package cz.muni.fi.pa165.service;


import cz.muni.fi.pa165.data.model.Genre;
import cz.muni.fi.pa165.data.repository.GenreRepository;
import cz.muni.fi.pa165.exceptions.ApiException;
import cz.muni.fi.pa165.api.genre.CreateGenreRequestDto;
import org.h2.jdbc.JdbcSQLIntegrityConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class GenreService {
    private final GenreRepository genreRepository;

    @Autowired
    public GenreService(GenreRepository genreRepository) {
        this.genreRepository = genreRepository;
    }

    @Transactional(readOnly = true)
    public List<Genre> findAllGenres(String route) {
        try {
            return genreRepository.findAll();
        } catch (Exception e) {
            throw new ApiException(
                    "An unexpected error occurred while retrieving all genres",
                    HttpStatus.INTERNAL_SERVER_ERROR,
                    route
            );
        }

    }

    @Transactional(readOnly = true)
    public Page<Genre> getPageOfGenres(Pageable pageable, String route) {
        try {
            return genreRepository.findAll(pageable);
        } catch (Exception e) {
            throw new ApiException(
                    "An unexpected error occurred while retrieving all genres",
                    HttpStatus.INTERNAL_SERVER_ERROR,
                    route
            );
        }

    }

    @Transactional
    public Genre create(CreateGenreRequestDto createGenreRequestDto, String route) {
        Genre genre = new Genre();
        genre.setName(createGenreRequestDto.name());
        try {
            genre = genreRepository.save(genre);
            genreRepository.flush();
        } catch (DataIntegrityViolationException e) {
            throw new ApiException("Genre with that name already exists", HttpStatus.BAD_REQUEST, route);
        } catch (Exception e) {
            throw new ApiException("Unexpected error occurred", HttpStatus.INTERNAL_SERVER_ERROR, route);
        }
        return genre;
    }

    @Transactional
    public Genre update(Long id, CreateGenreRequestDto createGenreRequestDto, String route) {
        try {
            Optional<Genre> genreOptional = genreRepository.findById(id);
            if (genreOptional.isEmpty()) {
                throw new ApiException("Genre id is not valid", HttpStatus.NOT_FOUND, route);
            }
            Genre genre = genreOptional.get();
            genre.setName(createGenreRequestDto.name());
            genreRepository.save(genre);
            genreRepository.flush();
            return genre;
        } catch (DataIntegrityViolationException e) {
            throw new ApiException("Genre with that name already exists", HttpStatus.BAD_REQUEST, route);
        } catch (Exception e) {
            throw new ApiException("Unexpected error occurred", HttpStatus.INTERNAL_SERVER_ERROR, route);
        }
    }

    @Transactional
    public void delete(Long id, String route) {
        try {
            if (!genreRepository.existsById(id)) {
                throw new ApiException("Genre id is not valid", HttpStatus.NOT_FOUND, route);
            }
            genreRepository.deleteById(id);
            genreRepository.flush();
        } catch (ApiException e) {
            throw e;
        } catch (Exception e) {
            throw new ApiException("Unexpected error occurred", HttpStatus.INTERNAL_SERVER_ERROR, route);
        }
    }
}
