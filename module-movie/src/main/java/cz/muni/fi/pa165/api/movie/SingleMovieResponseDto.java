package cz.muni.fi.pa165.api.movie;


import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;

@Schema(
        title = "Response for a single movie",
        description = "DTO which contains a message and "
)
public record SingleMovieResponseDto(
        DetailedMovieDto item,
        @NotBlank
        @Schema(description = "Additional message", example = "Success")
        String message
) {

}
