package cz.muni.fi.pa165.api.auth;

public record AuthResponse(
        UserBasicViewDto item,
        String message
) {
}
