package cz.muni.fi.pa165.config;

import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.security.OAuthFlow;
import io.swagger.v3.oas.models.security.OAuthFlows;
import io.swagger.v3.oas.models.security.Scopes;
import io.swagger.v3.oas.models.security.SecurityScheme;
import org.springdoc.core.customizers.OpenApiCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OpenApi {

    private final SecurityScheme swaggerSecurityScheme = new SecurityScheme()
            .type(SecurityScheme.Type.OAUTH2)
            .description("OAuth2 KeyCloak")
            .flows(new OAuthFlows()
                    .authorizationCode(new OAuthFlow()
                            .authorizationUrl("http://keycloak:8080/auth/realms/JavaProject/protocol/openid-connect/auth")
                            .tokenUrl("http://keycloak:8080/auth/realms/JavaProject/protocol/openid-connect/token")
                            .scopes(new Scopes())
                    )
            );

    @Bean
    public OpenApiCustomizer openAPICustomizer() {
        return openApi -> openApi
                .info(new Info()
                    .title("Movie Module")
                    .description("This module is responsible for movie management.")
                    .contact(new Contact().name("Marek Mišík"))
                )
                .getComponents()
                .addSecuritySchemes("OAuth2", swaggerSecurityScheme);
    }

}
