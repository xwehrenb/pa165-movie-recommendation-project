package cz.muni.fi.pa165.config;

import cz.muni.fi.pa165.exceptions.ApiException;
import cz.muni.fi.pa165.api.ErrorMessage;
import org.hibernate.exception.JDBCConnectionException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.ZonedDateTime;

@ControllerAdvice
public class ApiExceptionHandler {

    @ExceptionHandler(value = { ApiException.class })
    public ResponseEntity<Object> handleApiException(ApiException e) {
        ErrorMessage errorMessage = new ErrorMessage(
                ZonedDateTime.now().toString(),
                e.getHttpStatus().value(),
                e.getCause() != null ? e.getMessage(): "",
                e.getMessage(),
                e.getRoute()

        );
        return ResponseEntity.status(e.getHttpStatus()).body(errorMessage);
    }
    @ExceptionHandler(value = { JDBCConnectionException.class })
    public ResponseEntity<Object> handleRuntimeException(JDBCConnectionException e) {
        ErrorMessage errorMessage = new ErrorMessage(
                ZonedDateTime.now().toString(),
                500,
                e.getCause().getMessage(),
                e.getMessage(),
                ""

        );
        return ResponseEntity.status(500).body(errorMessage);
    }
}
