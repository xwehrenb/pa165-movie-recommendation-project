package cz.muni.fi.pa165.rest;

import cz.muni.fi.pa165.config.Routes;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UtilRestController {

    @GetMapping(Routes.HEALTHCHECK)
    public String all() {
        return "OK";
    }
}

