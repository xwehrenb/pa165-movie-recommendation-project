package cz.muni.fi.pa165.data.repository;

import cz.muni.fi.pa165.data.model.Genre;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;
import java.util.Set;

public interface GenreRepository extends JpaRepository<Genre, Long> {
    @Query("SELECT g FROM Genre g WHERE g.id =:id")
    Optional<Genre> findById(@Param("id") Long id);

    @Query("SELECT g FROM Genre g WHERE g.id IN :ids")
    Set<Genre> findAllByIdIn(@Param("ids") Set<Long> ids);

}
