package cz.muni.fi.pa165.config;

public class Routes {
    public static final String MOVIES = "/api/movies";
    public static final String MOVIE_SINGLE= "/{id}";
    public static final String MOVIES_RANDOM = "/random";
    public static final String MOVIES_PAGED = "/paged";
    public static final String GENRES = "/api/genres";
    public static final String GENRE_PAGED = "/paged";
    public static final String GENRE_SINGLE = "/{id}";
    public static final String STAKEHOLDERS = "/api/movie-stakeholders";
    public static final String STAKEHOLDER_SINGLE = "/{id}";
    public static final String STAKEHOLDERS_PAGED = "/paged";
    public static final String IMAGES = "/api/images";
    public static final String IMAGE_SINGLE = "/{id}";
    public static final String HEALTHCHECK = "/api/healthcheck";
}
