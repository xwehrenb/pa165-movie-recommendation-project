package cz.muni.fi.pa165.facade;

import cz.muni.fi.pa165.api.image.CreateImageRequestDto;
import cz.muni.fi.pa165.api.image.ImageDto;
import cz.muni.fi.pa165.service.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ImageFacade {
    private final ImageService imageService;

    @Autowired
    public ImageFacade(ImageService imageService) {
        this.imageService = imageService;
    }

    @Transactional
    public ImageDto create(CreateImageRequestDto createImageRequestDto, String route) {
        return ImageDto.from(imageService.create(createImageRequestDto, route));
    }

    @Transactional
    public void delete(Long id, String route) {
        imageService.delete(id, route);
    }

    @Transactional
    public ImageDto getById(Long id, String route) {
        return ImageDto.from(imageService.getImageById(id, route));
    }

    @Transactional
    public List<ImageDto> getAll(String route) {
        return imageService.listImages(route).stream().map(ImageDto::from).toList();
    }
}
