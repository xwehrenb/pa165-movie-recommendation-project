package cz.muni.fi.pa165.api.image;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;

import java.util.List;

@Schema(
        title = "SingleImageResponse",
        description = "SingleImageResponse"
)
public record SingleImageResponseDto(@NotBlank
                                     ImageDto content,
                                     @NotBlank
                                     @Schema(description = "message", example = "Success")
                                     String message) {

}