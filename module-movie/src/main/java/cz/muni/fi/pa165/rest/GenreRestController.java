package cz.muni.fi.pa165.rest;

import cz.muni.fi.pa165.api.ErrorMessage;
import cz.muni.fi.pa165.api.MessageResponseDto;
import cz.muni.fi.pa165.api.genre.*;
import cz.muni.fi.pa165.config.Routes;
import cz.muni.fi.pa165.exceptions.ApiException;
import cz.muni.fi.pa165.facade.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = Routes.GENRES, produces = MediaType.APPLICATION_JSON_VALUE)
public class GenreRestController {

    private final GenreFacade genreFacade;

    @Autowired
    public GenreRestController(GenreFacade genreFacade) {
        this.genreFacade = genreFacade;
    }

    @Operation(
            summary = "Create a new genre.",
            description = """
                    Create a new genre.
                    """,
            responses = {
                    @ApiResponse(responseCode = "201",  content = @Content(mediaType = "application/json",schema = @Schema(implementation = SingleGenreResponseDto.class))),
                    @ApiResponse(responseCode = "400", description = "Genre could not be created",
                            content = @Content(schema = @Schema(implementation = ErrorMessage.class))),
                    @ApiResponse(responseCode = "500", description = "An error occurred",
                            content = @Content(schema = @Schema(implementation = ErrorMessage.class)))
            })
    @SecurityRequirement(name = "OAuth2")
    @PostMapping("")
    @ResponseStatus(HttpStatus.CREATED)
    public SingleGenreResponseDto createGenre(@RequestBody CreateGenreRequestDto createGenreRequestDto) {
        if(createGenreRequestDto.name().length() < 3) {
            throw new ApiException(
                    "Genre name must be at least 3 characters long.",
                    HttpStatus.BAD_REQUEST,
                    Routes.GENRES
            );
        }
        return new SingleGenreResponseDto(genreFacade.createGenre(createGenreRequestDto, Routes.GENRES), "Successfully created genre.");
    }

    @Operation(
            summary = "Get genres",
            description = """
                    Get list of genres.
                    """,
            responses = {
                    @ApiResponse(responseCode = "200",  content = @Content(mediaType = "application/json", schema = @Schema(implementation = GenresResponseDto.class))),
                    @ApiResponse(responseCode = "500", description = "An error occurred",
                            content = @Content(schema = @Schema(implementation = ErrorMessage.class)))
            })
    @GetMapping("")
    public GenresResponseDto getAllGenres() {
        return new GenresResponseDto(genreFacade.findAllGenres(Routes.GENRES), "Success");
    }

    @Operation(
            summary = "Get page of genres",
            description = """
                    Get page of genres.
                    """,
            responses = {
                    @ApiResponse(responseCode = "200",  content = @Content(mediaType = "application/json",schema = @Schema(implementation = PagedGenresResponseDto.class))),
                    @ApiResponse(responseCode = "500", description = "An error occurred",
                            content = @Content(schema = @Schema(implementation = ErrorMessage.class)))
            })
    @GetMapping(Routes.GENRE_PAGED)
    public PagedGenresResponseDto getPageOfGenres(@ParameterObject Pageable pageable) {
        Page<GenreDto> result = genreFacade.getPageOfGenres(pageable, Routes.GENRES + Routes.GENRE_PAGED);
        return new PagedGenresResponseDto(result.getContent(), result.getTotalElements(), "Success");
    }

    @Operation(
            summary = "Update genre",
            description = """
                    Update genre.
                    """,
            responses = {
                    @ApiResponse(responseCode = "200",  content = @Content(mediaType = "application/json",schema = @Schema(implementation = SingleGenreResponseDto.class))),
                    @ApiResponse(responseCode = "404", description = "Genre not found.",
                            content = @Content(schema = @Schema(implementation = ErrorMessage.class))),
                    @ApiResponse(responseCode = "500", description = "An error occurred",
                            content = @Content(schema = @Schema(implementation = ErrorMessage.class)))
            }
    )
    @SecurityRequirement(name = "OAuth2")
    @PutMapping(Routes.GENRE_SINGLE)
    @ResponseStatus(HttpStatus.OK)
    public SingleGenreResponseDto updateGenre(@PathVariable Long id, @RequestBody CreateGenreRequestDto createGenreRequestDto) {
        return new SingleGenreResponseDto(genreFacade.update(id, createGenreRequestDto, Routes.GENRES + Routes.GENRE_PAGED), "Successfully updated genre.");
    }

    @Operation(
            summary = "Delete genre",
            description = """
                    Delete genre.
                    """,
            responses = {
                    @ApiResponse(responseCode = "204",  content = @Content(mediaType = "application/json",schema = @Schema(implementation = MessageResponseDto.class))),
                    @ApiResponse(responseCode = "404", description = "Genre not found.",
                            content = @Content(schema = @Schema(implementation = ErrorMessage.class))),
                    @ApiResponse(responseCode = "500", description = "An error occurred",
                            content = @Content(schema = @Schema(implementation = ErrorMessage.class)))
            }
            )
    @SecurityRequirement(name = "OAuth2")
    @DeleteMapping(Routes.GENRE_SINGLE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public MessageResponseDto deleteGenre(@PathVariable Long id) {
        genreFacade.delete(id, Routes.GENRES + Routes.GENRE_SINGLE);
        return new MessageResponseDto("Successfully deleted genre.");
    }
}
