package cz.muni.fi.pa165.api.image;


import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;

import java.util.List;

@Schema(title = "Images response",
        description = "Images response containing a list of all images"
)
public record ImagesResponseDto(@NotBlank
                                List<ImageDto> content,
                                @NotBlank
                                @Schema(description = "message", example = "Success")
                                String message) {
}
