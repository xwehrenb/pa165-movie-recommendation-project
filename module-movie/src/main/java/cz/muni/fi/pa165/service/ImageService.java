package cz.muni.fi.pa165.service;

import cz.muni.fi.pa165.api.image.CreateImageRequestDto;
import cz.muni.fi.pa165.data.model.Image;
import cz.muni.fi.pa165.data.repository.ImageRepository;
import cz.muni.fi.pa165.exceptions.ApiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ImageService {
    private final ImageRepository imageRepository;

    @Autowired
    public ImageService(ImageRepository imageRepository) {
        this.imageRepository = imageRepository;
    }

    @Transactional
    public Image create(CreateImageRequestDto createImageRequestDto, String route) {
        Image image = new Image();
        image.setUrl(createImageRequestDto.url());
        image.setDescription(createImageRequestDto.description());
        try {
            image = imageRepository.save(image);
            imageRepository.flush();
        } catch (Exception e) {
            throw new ApiException("An unexpected error occurred.", HttpStatus.BAD_REQUEST, route);
        }

        return image;
    }

    @Transactional(readOnly = true)
    public List<Image> listImages(String route) {
        try {
            return imageRepository.findAll();
        } catch (Exception e) {
            throw new ApiException("An unexpected error occurred.", HttpStatus.BAD_REQUEST, route);
        }

    }

    @Transactional(readOnly = true)
    public Image getImageById(Long id, String route) {
        try {
            return imageRepository.findById(id).orElseThrow(
                    () -> new ApiException("Image not found.", HttpStatus.NOT_FOUND, route ));
        } catch (ApiException e) {
            throw e;
        } catch (Exception e) {
            throw new ApiException("An unexpected error occurred.", HttpStatus.BAD_REQUEST, route);
        }

    }


    @Transactional
    public void delete(Long id, String route) {
        try {
            if (!imageRepository.existsById(id)) {
                throw new ApiException("Image not found", HttpStatus.BAD_REQUEST, route);
            }
            imageRepository.deleteById(id);
        } catch (ApiException e) {
            throw e;
        } catch (Exception e) {
            throw new ApiException("An unexpected error occurred.", HttpStatus.INTERNAL_SERVER_ERROR, route);
        }


    }

}
