package cz.muni.fi.pa165.api.genre;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;

@Schema(
        title = "Response containing a single genre",
        description = "Response containing a single genre"
)
public record SingleGenreResponseDto(@NotBlank
                                     GenreDto content,
                                     @NotBlank
                                     @Schema(description = "Message", example = "Success")
                                     String message
) {
}
