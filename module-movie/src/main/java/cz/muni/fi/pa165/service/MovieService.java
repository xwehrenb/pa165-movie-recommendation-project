package cz.muni.fi.pa165.service;

import cz.muni.fi.pa165.data.model.Genre;
import cz.muni.fi.pa165.data.model.Image;
import cz.muni.fi.pa165.data.model.Movie;
import cz.muni.fi.pa165.data.model.MovieStakeHolder;
import cz.muni.fi.pa165.data.repository.GenreRepository;
import cz.muni.fi.pa165.data.repository.ImageRepository;
import cz.muni.fi.pa165.data.repository.MovieRepository;
import cz.muni.fi.pa165.data.repository.StakeHolderRepository;
import cz.muni.fi.pa165.api.movie.CreateMovieRequestDto;
import cz.muni.fi.pa165.exceptions.ApiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class MovieService {
    private final MovieRepository movieRepository;
    private final GenreRepository genreRepository;
    private final StakeHolderRepository movieStakeHolderRepository;
    private final ImageRepository imageRepository;

    @Autowired
    public MovieService(MovieRepository movieRepository, GenreRepository genreRepository, StakeHolderRepository movieStakeHolderRepository, ImageRepository imageRepository) {
        this.movieRepository = movieRepository;
        this.genreRepository = genreRepository;
        this.movieStakeHolderRepository = movieStakeHolderRepository;
        this.imageRepository = imageRepository;
    }

    @Transactional(readOnly = true)
    public Movie findById(Long id, String route) {
        try {
            return movieRepository.findById(id).orElseThrow(() ->
                    new ApiException("Movie not found.", HttpStatus.NOT_FOUND, route)
            );
        } catch (ApiException e) {
            throw e;
        } catch (Exception e) {
            throw new ApiException("An unknown error occurred.", HttpStatus.INTERNAL_SERVER_ERROR, route);
        }

    }


    @SuppressWarnings("DuplicatedCode")
    @Transactional
    public Movie create(CreateMovieRequestDto movie, String route) {
        Movie m = new Movie();

        try {
            Set<Genre> categories = genreRepository.findAllByIdIn(Set.of(movie.genre_ids()));
            if (categories.size() != movie.genre_ids().length) {
                throw new ApiException("Genre ids are not valid", HttpStatus.BAD_REQUEST, route);
            }
            Set<MovieStakeHolder> actors = movieStakeHolderRepository.findAllByIdIn(Set.of(movie.actor_ids()));
            if (actors.size()!= movie.actor_ids().length) {
                throw new ApiException("Actor ids are not valid", HttpStatus.BAD_REQUEST, route);
            }
            Set<Image> images = imageRepository.findAllByIdIn(Set.of(movie.image_ids()));
            if (images.size()!= movie.image_ids().length) {
                throw new ApiException("Image ids are not valid", HttpStatus.BAD_REQUEST, route);
            }
            Optional<MovieStakeHolder> director = movieStakeHolderRepository.findById(movie.director_id());
            if (director.isEmpty()) {
                throw new ApiException("Director id is not valid", HttpStatus.BAD_REQUEST, route);
            }
            m.setActors(actors);
            m.setDirector(director.get());
            m.setCategories(categories);
            m.setTitle(movie.title());
            m.setYear(movie.year());
            m.setImages(images);
            m.setDescription(movie.description());
            m = movieRepository.save(m);
            Movie finalM = m;
            images.forEach(image -> image.setMovie(finalM));
            imageRepository.saveAll(images);
            movieRepository.flush();
            imageRepository.flush();
            return finalM;
        } catch (DataIntegrityViolationException e) {
            throw new ApiException("Movie could not be created.", HttpStatus.BAD_REQUEST, route);
        } catch (ApiException e) {
            throw e;
        } catch (Exception e) {
            throw new ApiException("An unknown error occurred.", HttpStatus.INTERNAL_SERVER_ERROR, route);
        }
    }

    @SuppressWarnings("DuplicatedCode")
    @Transactional
    public Movie update(Long id, CreateMovieRequestDto createMovieRequestDto, String route) {
        Movie m = findById(id, route);
        try {
            Set<Genre> categories = genreRepository.findAllByIdIn(Set.of(createMovieRequestDto.genre_ids()));
            if (categories.size()!= createMovieRequestDto.genre_ids().length) {
                throw new ApiException("Genre ids are not valid", HttpStatus.BAD_REQUEST, route);
            }
            Set<MovieStakeHolder> actors = movieStakeHolderRepository.findAllByIdIn(Set.of(createMovieRequestDto.actor_ids()));
            if (actors.size()!= createMovieRequestDto.actor_ids().length) {
                throw new ApiException("Actor ids are not valid", HttpStatus.BAD_REQUEST, route);
            }
            Set<Image> images = imageRepository.findAllByIdIn(Set.of(createMovieRequestDto.image_ids()));
            if (images.size()!= createMovieRequestDto.image_ids().length) {
                throw new ApiException("Image ids are not valid", HttpStatus.BAD_REQUEST, route);
            }
            Optional<MovieStakeHolder> director = movieStakeHolderRepository.findById(createMovieRequestDto.director_id());
            if (director.isEmpty()) {
                throw new ApiException("Director id is not valid", HttpStatus.BAD_REQUEST, route);
            }

            m.setActors(actors);
            m.setDirector(director.get());
            m.setCategories(categories);
            m.setTitle(createMovieRequestDto.title());
            m.setYear(createMovieRequestDto.year());
            m.setImages(images);
            m.setDescription(createMovieRequestDto.description());
            movieRepository.save(m);
            images.forEach(image -> image.setMovie(m));
            imageRepository.saveAll(images);
            movieRepository.flush();
            imageRepository.flush();
        } catch (DataIntegrityViolationException e) {
            throw new ApiException("Movie could not be created.", HttpStatus.BAD_REQUEST, route);
        } catch (ApiException e) {
            throw e;
        } catch (Exception e) {
            throw new ApiException("An unknown error occurred.", HttpStatus.INTERNAL_SERVER_ERROR, route);
        }

        return m;
    }

    @Transactional
    public void delete(Long id, String route) {
        try {
            if(!movieRepository.existsById(id)) {
                throw new ApiException("Movie not found", HttpStatus.NOT_FOUND, route);
            }
            movieRepository.deleteById(id);
        } catch (ApiException e) {
            throw e;
        } catch (Exception e) {
            throw new ApiException ("Movie not found", HttpStatus.INTERNAL_SERVER_ERROR, route);
        }

    }

    @Transactional(readOnly = true)
    public List<Movie> getMoviesByCriteria(Optional<Long> genreId, Optional<Long> directorId, Optional<Long> actorId, String route) {
        try {
            return movieRepository.findByCriteria(genreId, directorId, actorId);
        } catch (Exception e) {
            throw new ApiException("An unknown error occurred.", HttpStatus.INTERNAL_SERVER_ERROR, route);
        }

    }

    @Transactional(readOnly = true)
    public Page<Movie> getPageOfMoviesByCriteria(Optional<Long> genreId, Optional<Long> directorId, Optional<Long> actorId, Pageable pageable, String route) {
        try {
            return movieRepository.findByCriteriaPaged(genreId, directorId, actorId, pageable);
        } catch (Exception e) {
            throw new ApiException("An unknown error occurred.", HttpStatus.INTERNAL_SERVER_ERROR, route);
        }

    }

    @Transactional(readOnly = true)
    public List<Movie> getRandomMovies(Optional<Long> count, String route) {
        try {
            return movieRepository.getRandomMovies(count);
        } catch (Exception e) {
            throw new ApiException("An unknown error occurred." + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR, route);
        }
    }
}
