package cz.muni.fi.pa165.api.movie;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;

import java.util.List;

@Schema(
        name = "MoviesResponseDto",
        description = "The response of list of movies"
)
public record PagedMoviesResponseDto(
        List<DetailedMovieDto> content,
        @NotBlank
        @Schema(description = "Total items", example = "420")
        Long total,
        @NotBlank
        @Schema(description = "Additional message", example = "Success")
        String message
) {
}
