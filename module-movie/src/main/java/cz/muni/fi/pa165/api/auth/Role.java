package cz.muni.fi.pa165.api.auth;

public class Role {
    public static final String ADMIN = "app-admin";
    public static final String USER = "app-user";
}