package cz.muni.fi.pa165.api.movie;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;

@Schema(
        title = "UpdateMovieRequestDto",
        description = "Request to update movie"
)
public record UpdateMovieRequestDto(@NotBlank
                                    @Schema(description = "id", example = "1")
                                    Long id,
                                    @NotBlank
                                    @Schema(description = "title", example = "The Godfather")
                                    String title,
                                    @NotBlank
                                    @Schema(description = "year", example = "1994")
                                    int year,
                                    @NotBlank
                                    @Schema(description = "description", example = "The Godfather")
                                    String description,
                                    @NotBlank
                                    @Schema(description = "director_id", example = "1")
                                    Long director_id,
                                    @NotBlank
                                    @Schema(description = "genre_ids", example = "[1]")
                                    Long[] genre_ids,
                                    @NotBlank
                                    @Schema(description = "actor_ids", example = "[1]")
                                    Long[] actor_ids
) {
}
