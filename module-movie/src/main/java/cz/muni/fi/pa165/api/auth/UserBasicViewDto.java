package cz.muni.fi.pa165.api.auth;


import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;

@Schema(title = "Basic user", description = "Basic user")
public record UserBasicViewDto (
        @NotBlank
        @Schema(description = "id", example = "1")
        long id,

        @NotBlank
        @Schema(description = "User name", example = "John Doe")
        String name,

        @NotBlank
        @Schema(description = "User email", example = "john.doe@example.com")
        String email,

        @NotBlank
        @Schema(description = "User role", example = "USER")
        Role role
) { }

