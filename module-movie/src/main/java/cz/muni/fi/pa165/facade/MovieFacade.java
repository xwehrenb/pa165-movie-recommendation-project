package cz.muni.fi.pa165.facade;


import cz.muni.fi.pa165.api.movie.CreateMovieRequestDto;
import cz.muni.fi.pa165.api.movie.DetailedMovieDto;
import cz.muni.fi.pa165.service.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class MovieFacade {
    private final MovieService movieService;

    @Autowired
    public MovieFacade(MovieService movieService) {
        this.movieService = movieService;
    }

    @Transactional(readOnly = true)
    public DetailedMovieDto getMovieById(Long id, String route) {
        return DetailedMovieDto.from(movieService.findById(id, route));
    }

    @Transactional
    public DetailedMovieDto createMovie(CreateMovieRequestDto createMovieRequestDto, String route) {
        return DetailedMovieDto.from(movieService.create(createMovieRequestDto, route));
    }

    @Transactional(readOnly = true)
    public List<DetailedMovieDto> getMoviesByCriteria(Optional<Long> genreId, Optional<Long> directorId, Optional<Long> actorId, String route) {
        return movieService.getMoviesByCriteria(genreId, directorId, actorId, route).stream().map(DetailedMovieDto::from).toList();
    }

    @Transactional(readOnly = true)
    public Page<DetailedMovieDto> getMoviesByCriteriaPaged(Optional<Long> genreId, Optional<Long> directorId, Optional<Long> actorId, Pageable pageable, String route) {
        return movieService.getPageOfMoviesByCriteria(genreId, directorId, actorId, pageable, route).map((DetailedMovieDto::from));
    }

    @Transactional
    public void delete(Long id, String route) {
        movieService.delete(id, route);
    }

    @Transactional
    public DetailedMovieDto update(Long id, CreateMovieRequestDto createMovieRequestDto, String route) {
        return DetailedMovieDto.from(movieService.update(id, createMovieRequestDto, route));
    }

    @Transactional(readOnly = true)
    public List<DetailedMovieDto> getRandomMovies(Optional<Long> count, String route) {
        return movieService.getRandomMovies(count, route).stream().map(DetailedMovieDto::from).toList();
    }


}
