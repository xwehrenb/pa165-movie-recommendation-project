package cz.muni.fi.pa165.rest;

import cz.muni.fi.pa165.api.ErrorMessage;
import cz.muni.fi.pa165.api.MessageResponseDto;
import cz.muni.fi.pa165.api.movie.*;
import cz.muni.fi.pa165.config.Routes;
import cz.muni.fi.pa165.exceptions.ApiException;
import cz.muni.fi.pa165.facade.*;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.servers.Server;
import io.swagger.v3.oas.annotations.servers.ServerVariable;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.time.ZonedDateTime;
import java.util.Optional;

@RestController
@OpenAPIDefinition(
        info = @Info(title = "Movie repository",
                version = "1.0",
                description = """
                        Simple service for movie management
                        """,
                contact = @Contact(name = "Marek Misik", email = "xmisik@mail.muni.cz"),
                license = @License(name = "Apache 2.0", url = "https://www.apache.org/licenses/LICENSE-2.0.html")
        ),
        servers = @Server(description = "my server", url = "{scheme}://{server}:{port}", variables = {
                @ServerVariable(name = "scheme", allowableValues = {"http", "https"}, defaultValue = "http"),
                @ServerVariable(name = "server", defaultValue = "localhost"),
                @ServerVariable(name = "port", defaultValue = "8089"),
        })
)
@RequestMapping(path = Routes.MOVIES, produces = MediaType.APPLICATION_JSON_VALUE)
public class MovieRestController {
    private final MovieFacade movieFacade;

    @Autowired
    public MovieRestController(MovieFacade movieFacade) {
        this.movieFacade = movieFacade;
    }


    @Operation(
            summary = "Get all movies that satisfy criteria",
            description = """
                    Returns list of all movies with basic information satisfying criteria:
                    
                        genreId -> id of the wanted genre
                        directorId -> id of the wanted director
                        actorId -> id of the wanted actor
                        
                        If no criteria is given, returns all movies.
                    """,
            responses = {
                    @ApiResponse(responseCode = "200",  content = @Content(mediaType = "application/json",schema = @Schema(implementation = MoviesResponseDto.class))),
                    @ApiResponse(responseCode = "500", description = "Internal server error",
                            content = @Content(schema = @Schema(implementation = ErrorMessage.class)))
            },
            parameters = {
                    @Parameter(name = "genreId", allowEmptyValue = true, content = @Content(schema = @Schema(implementation = Long.class))),
                    @Parameter(name = "directorId", allowEmptyValue = true, content = @Content(schema = @Schema(implementation = Long.class))),
                    @Parameter(name = "actorId", allowEmptyValue = true, content = @Content(schema = @Schema(implementation = Long.class)))
            }
    )
    @GetMapping(path = "")
    public MoviesResponseDto getAllMovies(@RequestParam Optional<Long> genreId, @RequestParam Optional<Long> directorId, @RequestParam Optional<Long> actorId) {
        return new MoviesResponseDto(movieFacade.getMoviesByCriteria(genreId, directorId, actorId, Routes.MOVIES), "Success");
    }

    @Operation(
            summary = "Get all movies that satisfy criteria",
            description = """
                    Returns list of all movies with basic information satisfying criteria:
                    
                        genreId -> id of the wanted genre
                        directorId -> id of the wanted director
                        actorId -> id of the wanted actor
                        
                        If no criteria is given, returns all movies.
                    """,
            responses = {
                    @ApiResponse(responseCode = "200",  content = @Content(mediaType = "application/json", schema = @Schema(implementation = PagedMoviesResponseDto.class))),
                    @ApiResponse(responseCode = "500", description = "Internal server error",
                            content = @Content(schema = @Schema(implementation = ErrorMessage.class)))
            },
            parameters = {
                    @Parameter(name = "genreId", allowEmptyValue = true, content = @Content(schema = @Schema(implementation = Long.class))),
                    @Parameter(name = "directorId", allowEmptyValue = true, content = @Content(schema = @Schema(implementation = Long.class))),
                    @Parameter(name = "actorId", allowEmptyValue = true, content = @Content(schema = @Schema(implementation = Long.class)))
            }
    )
    @GetMapping(path = Routes.MOVIES_PAGED)
    public PagedMoviesResponseDto getAllMovies(@RequestParam Optional<Long> genreId, @RequestParam Optional<Long> directorId, @RequestParam Optional<Long> actorId, @ParameterObject Pageable pageable) {
        Page<DetailedMovieDto> result = movieFacade.getMoviesByCriteriaPaged(genreId, directorId, actorId, pageable, Routes.MOVIES + Routes.MOVIES_PAGED);
        return new PagedMoviesResponseDto(result.stream().toList(), result.getTotalElements(),"Success");
    }

    @Operation(
            summary = "Get movie detail",
            description = """
                    Returns detail of a selected movie
                    """,
            responses = {
                    @ApiResponse(responseCode = "200", content = @Content(
                            schema = @Schema(implementation = SingleMovieResponseDto.class))),
                    @ApiResponse(responseCode = "404", description = "Movie not found",
                            content = @Content(schema = @Schema(implementation = ErrorMessage.class)))
            })
    @GetMapping(path = Routes.MOVIE_SINGLE)
    public SingleMovieResponseDto getMovieDetail(@PathVariable("id") Long id) {
        return new SingleMovieResponseDto(movieFacade.getMovieById(id, Routes.MOVIES + Routes.MOVIE_SINGLE), "Success");
    }

    @Operation(
            summary = "Create movie",
            description = """
                    Creates a new movie
                    """,
            responses = {
                    @ApiResponse(responseCode = "201", content = @Content(
                            schema = @Schema(implementation = SingleMovieResponseDto.class))),
                    @ApiResponse(responseCode = "400", description = "Bad request",
                            content = @Content(schema = @Schema(implementation = ErrorMessage.class))),
                    @ApiResponse(responseCode = "500", description = "Internal server error",
                            content = @Content(schema = @Schema(implementation = ErrorMessage.class)))
            }
    )
    @SecurityRequirement(name = "OAuth2")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public SingleMovieResponseDto createMovie(@RequestBody CreateMovieRequestDto movieDto) {
        if(movieDto.title().length() < 1) {
            throw new ApiException(
                    "Validation Error, title must be at least 1 character long",
                    HttpStatus.BAD_REQUEST,
                    Routes.MOVIES
            );

        }
        if(movieDto.year() < 1900 || movieDto.year() > ZonedDateTime.now().getYear()) {
            throw new ApiException(
                    "Validation Error, invalid year",
                    HttpStatus.BAD_REQUEST,
                    Routes.MOVIES
            );
        }

        return new SingleMovieResponseDto(movieFacade.createMovie(movieDto, Routes.MOVIES), "Success");
    }

    @Operation(
            summary = "Update movie",
            description = """
                    Updates a movie
                    """,
            responses = {
                    @ApiResponse(responseCode = "200", content = @Content(
                            schema = @Schema(implementation = SingleMovieResponseDto.class))),
                    @ApiResponse(responseCode = "400", description = "Bad Request",
                            content = @Content(schema = @Schema(implementation = ErrorMessage.class))),
                    @ApiResponse(responseCode = "500", description = "Internal server error",
                            content = @Content(schema = @Schema(implementation = ErrorMessage.class)))
            }
    )
    @SecurityRequirement(name = "OAuth2")
    @PutMapping(path = Routes.MOVIE_SINGLE)
    public SingleMovieResponseDto updateMovie(@PathVariable("id") Long id, @RequestBody CreateMovieRequestDto movieDto) {
        return new SingleMovieResponseDto(movieFacade.update(id, movieDto, Routes.MOVIES + Routes.MOVIE_SINGLE), "Success");
    }

    @Operation(
            summary = "Delete movie",
            description = """
                    Deletes a movie
                    """,
            responses = {
                    @ApiResponse(responseCode = "200", content = @Content(
                            schema = @Schema(implementation = MessageResponseDto.class))),
                    @ApiResponse(responseCode = "404", description = "Movie not found",
                            content = @Content(schema = @Schema(implementation = ErrorMessage.class))),
                    @ApiResponse(responseCode = "500", description = "Internal server error",
                            content = @Content(schema = @Schema(implementation = ErrorMessage.class)))
            }
    )
    @SecurityRequirement(name = "OAuth2")
    @DeleteMapping(path = Routes.MOVIE_SINGLE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public MessageResponseDto deleteMovie(@PathVariable("id") Long id) {
        movieFacade.delete(id, Routes.MOVIES + Routes.MOVIE_SINGLE);
        return new MessageResponseDto("Success");
    }

    @Operation(
            summary = "Get random sample of movies.",
            description = """
                    Returns a random sample of movies.
                    """,
            responses = {
                    @ApiResponse(responseCode = "200", content = @Content(
                            schema = @Schema(implementation = MoviesResponseDto.class))),
                    @ApiResponse(responseCode = "500", description = "Internal server error",
                            content = @Content(schema = @Schema(implementation = ErrorMessage.class)))
            },
            parameters = {
                    @Parameter(name = "count", allowEmptyValue = true, content = @Content(schema = @Schema(implementation = Long.class)))
            }
    )
    @GetMapping(path = Routes.MOVIES_RANDOM)
    public MoviesResponseDto getRandomMovies(@RequestParam Optional<Long> count) {
        return new MoviesResponseDto(movieFacade.getRandomMovies(count, Routes.MOVIES + Routes.MOVIES_RANDOM), "Success");
    }
}
