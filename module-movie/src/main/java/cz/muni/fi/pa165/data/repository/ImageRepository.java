package cz.muni.fi.pa165.data.repository;

import cz.muni.fi.pa165.data.model.Image;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;
import java.util.Set;

public interface ImageRepository extends JpaRepository<Image, Long> {

    @Query("SELECT i FROM Image i WHERE i.id = :id")
    Optional<Image> findById(@Param("id") Long id);

    @Query("SELECT i FROM Image i WHERE i.id IN :ids")
    Set<Image> findAllByIdIn(@Param("ids") Set<Long> ids);
}
