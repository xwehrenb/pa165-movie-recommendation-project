package unit.cz.muni.fi.pa165;

import cz.muni.fi.pa165.model.data.*;
import cz.muni.fi.pa165.model.repository.MoviePointService;
import cz.muni.fi.pa165.model.repository.RatingPointService;
import cz.muni.fi.pa165.service.RecommendationService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.*;

import static org.mockito.Mockito.when;

public class RecommendationServiceTest {
    private RecommendationService recommendationService;
    private MoviePointService moviePointService;
    private RatingPointService ratingPointService;

    private ArrayList<Movie> movies;

    private ArrayList<Rating> ratings;

    private UUID uuid = UUID.randomUUID();
    private UUID uuid2 = UUID.randomUUID();
    private UUID uuid3 = UUID.randomUUID();

    @BeforeEach
    public void setup() {
        moviePointService = Mockito.mock(MoviePointService.class);
        ratingPointService = Mockito.mock(RatingPointService.class);
        recommendationService = new RecommendationService(moviePointService, ratingPointService);
        movies = prepareMovies();
        setupMovieMocks();
        ratings = prepareRatings();
        setupRatingMocks();
    }

    @Test
    public void basicSimilarityTest() {
        var movie = movies.get(0);
        var similarMovies = recommendationService.getSimilarMovies(movie.getId());

        assert similarMovies.size() == 2;
        assert similarMovies.get(0).getId() == 2;
        assert similarMovies.get(1).getId() == 4;
        assert similarMovies.get(0).getSimilarityRank() > similarMovies.get(1).getSimilarityRank();
        assert similarMovies.get(0).getSimilarityRank() == 4;
        assert similarMovies.get(1).getSimilarityRank() == 3;
    }

    @Test
    public void extensiveSimilarityTest() {
        var movie = movies.get(1);
        var similarMovies = recommendationService.getSimilarMovies(movie.getId());

        assert similarMovies.size() == 3;
        assert similarMovies.get(0).getId() == 1;
        assert similarMovies.get(1).getId() == 3;
        assert similarMovies.get(2).getId() == 4;
        assert similarMovies.get(0).getSimilarityRank() > similarMovies.get(1).getSimilarityRank();
        assert similarMovies.get(1).getSimilarityRank() > similarMovies.get(2).getSimilarityRank();
        assert similarMovies.get(0).getSimilarityRank() == 4;
        assert similarMovies.get(1).getSimilarityRank() == 2;
        assert similarMovies.get(2).getSimilarityRank() == 1;
    }

    @Test
    public void missingMovieSimilarityTest() {
        var temporaryMovie = new Movie();
        temporaryMovie.setId(200L);
        var similarMovies = recommendationService.getSimilarMovies(temporaryMovie.getId());

        assert similarMovies.size() == 0;
    }

    @Test
    public void noSimilarMoviesTest() {
        var movie = movies.get(4);
        var similarMovies = recommendationService.getSimilarMovies(movie.getId());

        assert similarMovies.size() == 0;
    }

    @Test
    public void basicRatingTest() {
        var movie = movies.get(0);
        var likedMovies = recommendationService.likedMovies(movie.getId(), Optional.empty(), Optional.empty());

        assert likedMovies.size() == 3;
        assert likedMovies.get(0).getId() == 3;
        assert likedMovies.get(1).getId() == 4;
        assert likedMovies.get(2).getId() == 2;
        assert likedMovies.get(0).getUserRatingScore() > likedMovies.get(1).getUserRatingScore();
        assert likedMovies.get(1).getUserRatingScore() > likedMovies.get(2).getUserRatingScore();
        assert likedMovies.get(0).getUserRatingScore() == 30.0;
        assert likedMovies.get(1).getUserRatingScore() == 16.0;
        assert likedMovies.get(2).getUserRatingScore() == 6.75;
    }

    @Test
    public void missingMovieRatingTest() {
        var temporaryMovie = new Movie();
        temporaryMovie.setId(200L);
        var likedMovies = recommendationService.likedMovies(temporaryMovie.getId(), Optional.empty(), Optional.empty());

        assert likedMovies.size() == 0;
    }

    @Test
    public void noRatingsTest() {
        var movie = movies.get(4);
        var likedMovies = recommendationService.likedMovies(movie.getId(), Optional.empty(), Optional.empty());

        assert likedMovies.size() == 0;
    }

    @Test
    public void pagingTest() {
        var movie = movies.get(0);

        var likedMovies = recommendationService.likedMovies(movie.getId(), Optional.of(0), Optional.of(1));
        assert likedMovies.size() == 1;
        assert likedMovies.get(0).getId() == 3;

        var likedMovies2 = recommendationService.likedMovies(movie.getId(), Optional.of(1), Optional.of(1));
        assert likedMovies2.size() == 1;
        assert likedMovies2.get(0).getId() == 4;
    }

    private void setupRatingMocks() {
        when(ratingPointService.getRatingsByMovieId(1)).thenReturn(List.of(ratings.get(0), ratings.get(1), ratings.get(2)));
        when(ratingPointService.getRatingsByMovieId(2)).thenReturn(List.of(ratings.get(3), ratings.get(4), ratings.get(5)));
        when(ratingPointService.getRatingsByMovieId(3)).thenReturn(List.of(ratings.get(6), ratings.get(7), ratings.get(8)));
        when(ratingPointService.getRatingsByMovieId(4)).thenReturn(List.of(ratings.get(9), ratings.get(10), ratings.get(11)));
        when(ratingPointService.getRatingsByMovieId(5)).thenReturn(List.of());

        when(ratingPointService.getRatingsByUserId(uuid)).thenReturn(List.of(ratings.get(0), ratings.get(3), ratings.get(6), ratings.get(9), ratings.get(10)));
        when(ratingPointService.getRatingsByUserId(uuid2)).thenReturn(List.of(ratings.get(1), ratings.get(4), ratings.get(7), ratings.get(11)));
        when(ratingPointService.getRatingsByUserId(uuid3)).thenReturn(List.of(ratings.get(2), ratings.get(5), ratings.get(8)));

        when(ratingPointService.getRatingsByUserIds(new HashSet<>(Arrays.asList(uuid, uuid2, uuid3)))).thenReturn(List.of(
                ratings.get(0), ratings.get(1), ratings.get(2), ratings.get(3), ratings.get(4), ratings.get(5), ratings.get(6), ratings.get(7), ratings.get(8), ratings.get(9), ratings.get(10), ratings.get(11)
        ));
    }
    private ArrayList<Rating> prepareRatings() {
        return new ArrayList<>(Arrays.asList(
                new Rating() {{ //0
                    setMovie_id(1);
                    setUser_id(uuid);
                    setCriterion_id(1);
                    setValue(5);
                }},
                new Rating() {{ //1
                    setMovie_id(1);
                    setUser_id(uuid2);
                    setCriterion_id(1);
                    setValue(4);
                }},
                new Rating() {{ //2
                    setMovie_id(1);
                    setUser_id(uuid3);
                    setCriterion_id(1);
                    setValue(10);
                }},
                new Rating() {{ //3
                    setMovie_id(2);
                    setUser_id(uuid);
                    setCriterion_id(1);
                    setValue(1);
                }},
                new Rating() {{ //4
                    setMovie_id(2);
                    setUser_id(uuid2);
                    setCriterion_id(1);
                    setValue(2);
                }},
                new Rating() {{ //5
                    setMovie_id(2);
                    setUser_id(uuid3);
                    setCriterion_id(1);
                    setValue(3);
                }},
                new Rating() {{ //6
                    setMovie_id(3);
                    setUser_id(uuid);
                    setCriterion_id(1);
                    setValue(10);
                }},
                new Rating() {{ //7
                    setMovie_id(3);
                    setUser_id(uuid2);
                    setCriterion_id(1);
                    setValue(10);
                }},
                new Rating() {{ //8
                    setMovie_id(3);
                    setUser_id(uuid3);
                    setCriterion_id(1);
                    setValue(10);
                }},
                new Rating() {{ //9
                    setMovie_id(4);
                    setUser_id(uuid);
                    setCriterion_id(1);
                    setValue(8);
                }},
                new Rating() {{ //10
                    setMovie_id(4);
                    setUser_id(uuid);
                    setCriterion_id(2);
                    setValue(8);
                }},
                new Rating() {{ //11
                    setMovie_id(4);
                    setUser_id(uuid2);
                    setCriterion_id(1);
                    setValue(8);
                }}
        ));
    }

    private void setupMovieMocks() {
        when(moviePointService.getMovie(1L)).thenReturn(movies.get(0));
        when(moviePointService.getMovie(2L)).thenReturn(movies.get(1));
        when(moviePointService.getMovie(3L)).thenReturn(movies.get(2));
        when(moviePointService.getMovie(4L)).thenReturn(movies.get(3));
        when(moviePointService.getUnrankedMovies(movies.get(0))).thenReturn(new HashMap<Long, Movie>() {{
            put(1L, movies.get(0));
            put(2L, movies.get(1));
            put(4L, movies.get(3));
        }});
        when(moviePointService.getUnrankedMovies(movies.get(1))).thenReturn(new HashMap<Long, Movie>() {{
            put(1L, movies.get(0));
            put(2L, movies.get(1));
            put(3L, movies.get(2));
            put(4L, movies.get(3));
        }});
        when(moviePointService.getUnrankedMovies(movies.get(2))).thenReturn(new HashMap<Long, Movie>() {{
            put(2L, movies.get(1));
            put(3L, movies.get(2));
        }});
        when(moviePointService.getUnrankedMovies(movies.get(3))).thenReturn(new HashMap<Long, Movie>() {{
            put(1L, movies.get(0));
            put(2L, movies.get(1));
            put(3L, movies.get(2));
        }});
        when(moviePointService.getUnrankedMovies(movies.get(4))).thenReturn(new HashMap<Long, Movie>() {{
            put(5L, movies.get(4));
        }});
    }

    private ArrayList<Movie> prepareMovies() {
        return new ArrayList<Movie>() {
            {
                add(new Movie() {{
                    setId(1L);
                    setTitle("Movie1");
                    setDirector(new Director() {{
                        setId(1L);
                    }});
                    setGenres(new Genre[]{
                            new Genre() {{
                                setId(1L);
                            }},
                            new Genre() {{
                                setId(2L);
                            }}
                    });
                    setActors(new Actor[]{
                            new Actor() {{
                                setId(1L);
                            }},
                            new Actor() {{
                                setId(2L);
                            }},
                            new Actor() {{
                                setId(3L);
                            }}
                    });
                    setYear(2000);
                }});
                add(new Movie() {{
                    setId(2L);
                    setTitle("Movie2");
                    setDirector(new Director() {{
                        setId(2L);
                    }});
                    setGenres(new Genre[]{
                            new Genre() {{
                                setId(2L);
                            }},
                            new Genre() {{
                                setId(3L);
                            }}
                    });
                    setActors(new Actor[]{
                            new Actor() {{
                                setId(2L);
                            }},
                            new Actor() {{
                                setId(3L);
                            }},
                            new Actor() {{
                                setId(4L);
                            }}
                    });
                    setYear(2001);
                }});
                add(new Movie() {{
                    setId(3L);
                    setTitle("Movie3");
                    setDirector(new Director() {{
                        setId(3L);
                    }});
                    setGenres(new Genre[]{
                            new Genre() {{
                                setId(3L);
                            }},
                            new Genre() {{
                                setId(4L);
                            }}
                    });
                    setActors(new Actor[]{
                            new Actor() {{
                                setId(10L);
                            }},
                            new Actor() {{
                                setId(11L);
                            }},
                            new Actor() {{
                                setId(12L);
                            }}
                    });
                    setYear(2002);
                }});
                add(new Movie() {{
                        setId(4L);
                        setTitle("Movie4");
                        setDirector(new Director() {{
                            setId(4L);
                        }});
                        setGenres(new Genre[]{
                                new Genre() {{
                                    setId(4L);
                                }},
                                new Genre() {{
                                    setId(1L);
                                }}
                        });
                        setActors(new Actor[]{
                                new Actor() {{
                                    setId(4L);
                                }},
                                new Actor() {{
                                    setId(5L);
                                }},
                                new Actor() {{
                                    setId(1L);
                                }}
                        });
                        setYear(2000);
                    }}
                );
                add(new Movie() {{
                    setId(5L);
                    setTitle("Movie5");
                    setDirector(new Director() {{
                        setId(100L);
                    }});
                    setGenres(new Genre[]{
                            new Genre() {{
                                setId(500L);
                            }},
                            new Genre() {{
                                setId(100L);
                            }}
                    });
                    setActors(new Actor[]{
                            new Actor() {{
                                setId(500L);
                            }},
                            new Actor() {{
                                setId(600L);
                            }},
                            new Actor() {{
                                setId(100L);
                            }}
                    });
                    setYear(2000);
                }});
            }};
    }
}
