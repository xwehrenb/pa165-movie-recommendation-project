package unit.cz.muni.fi.pa165;

import cz.muni.fi.pa165.App;
import cz.muni.fi.pa165.model.data.*;
import cz.muni.fi.pa165.service.RecommendationService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = App.class)
public class RecommendationServiceUtilTest {
    @Autowired
    private RecommendationService recommendationService;
    private HashMap<Long, Movie> movieList = new HashMap<>();
    private ArrayList<Rating> ratingList = new ArrayList<>();
    private UUID uuid1 = UUID.randomUUID();
    private UUID uuid2 = UUID.randomUUID();

    @Before
    public void initData() {
        var director1 = new Director();
        director1.setId(1L);
        var director2 = new Director();
        director2.setId(2L);
        var director3 = new Director();
        director3.setId(3L);

        var genre1 = new Genre();
        genre1.setId(1L);
        var genre2 = new Genre();
        genre2.setId(2L);
        var genre3 = new Genre();
        genre3.setId(3L);
        var genre4 = new Genre();
        genre4.setId(4L);

        var actor1 = new Actor();
        actor1.setId(1L);
        var actor2 = new Actor();
        actor2.setId(2L);
        var actor3 = new Actor();
        actor3.setId(3L);
        var actor4 = new Actor();
        actor4.setId(4L);
        var actor5 = new Actor();
        actor5.setId(5L);

        var movie1 = new Movie();
        movie1.setId(1L);
        movie1.setTitle("Movie1");
        movie1.setDirector(director1);
        movie1.setGenres(new Genre[]{genre1, genre2});
        movie1.setActors(new Actor[]{actor1, actor2, actor3});

        var movie2 = new Movie();
        movie2.setId(2L);
        movie2.setTitle("Movie2");
        movie2.setDirector(director2);
        movie2.setGenres(new Genre[]{genre1, genre2, genre3});
        movie2.setActors(new Actor[]{actor1, actor2, actor3, actor4});

        var movie3 = new Movie();
        movie3.setId(3L);
        movie3.setTitle("movie3");
        movie3.setDirector(director1);
        movie3.setGenres(new Genre[]{genre1, genre2});
        movie3.setActors(new Actor[]{actor1, actor2, actor3});

        var movie4 = new Movie();
        movie4.setId(4L);
        movie4.setTitle("Movie4");
        movie4.setDirector(director3);
        movie4.setGenres(new Genre[]{genre4});
        movie4.setActors(new Actor[]{actor5});

        movieList.put(movie1.getId(), movie1);
        movieList.put(movie2.getId(), movie2);
        movieList.put(movie3.getId(), movie3);
        movieList.put(movie4.getId(), movie4);

        var rating1 = new Rating();
        rating1.setMovie_id(1);
        rating1.setCriterion_id(1);
        rating1.setUser_id(uuid1);
        rating1.setValue(5);
        var rating2 = new Rating();
        rating2.setMovie_id(1);
        rating2.setCriterion_id(2);
        rating2.setUser_id(uuid1);
        rating2.setValue(6);

        var rating3 = new Rating();
        rating3.setMovie_id(2);
        rating3.setCriterion_id(1);
        rating3.setUser_id(uuid1);
        rating3.setValue(10);

        var rating5 = new Rating();
        rating5.setMovie_id(2);
        rating5.setCriterion_id(2);
        rating5.setUser_id(uuid2);
        rating5.setValue(4);

        var rating4 = new Rating();
        rating4.setMovie_id(3);
        rating4.setCriterion_id(1);
        rating4.setUser_id(uuid1);
        rating4.setValue(3);
        ratingList.add(rating1);
        ratingList.add(rating2);
        ratingList.add(rating3);
        ratingList.add(rating4);
        ratingList.add(rating5);
    }

    @Test
    public void similarityTest() {
        var movieListCopy = new HashMap<Long, Movie>(movieList);
        recommendationService.rankSimilarity(movieListCopy, movieListCopy.get(1L));
        var outputedOrder = getOrderedSimilarity(movieListCopy);
        var expectedOrder = new int[]{0, 7, 10, 10};
        Assert.assertArrayEquals(expectedOrder, outputedOrder);
    }

    @Test
    public void ratingTest() {
        var out = recommendationService.filterMoviesByRating(ratingList, movieList.get(4L).getId());
        var expectedOrder = new Long[]{2L, 1L, 3L};
        var expectedScores = new double[]{14.0, 5.5, 3.0};

        Assert.assertArrayEquals(expectedOrder, getMovieIdsFromRatings(out));
        Assert.assertArrayEquals(expectedScores, getScoresFromRatings(out), 0.0001);
    }

    private double[] getScoresFromRatings(ArrayList<RatingEntry> out) {
        var scores = new double[out.size()];
        for (int i = 0; i < out.size(); i++) {
            scores[i] = out.get(i).getFinalScore();
        }
        return scores;
    }

    private int[] getOrderedSimilarity(HashMap<Long, Movie> movies) {
        var ids = new int[movies.size()];

        var cnt = 0;
        for (var movie : movies.values()) {
            ids[cnt] = movie.getSimilarityRank();
            cnt++;
        }
        Arrays.sort(ids);
        return ids;
    }

    private Long[] getMovieIdsFromRatings(ArrayList<RatingEntry> movies) {
        var ids = new Long[movies.size()];
        for (int i = 0; i < movies.size(); i++) {
            ids[i] = Long.valueOf(movies.get(i).getId());
        }
        return ids;
    }
}
