package integration.cz.muni.fi.pa165;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.WireMockServer;
import cz.muni.fi.pa165.App;
import cz.muni.fi.pa165.dto.*;
import cz.muni.fi.pa165.exceptions.MovieServiceException;
import cz.muni.fi.pa165.exceptions.RatingServiceException;
import cz.muni.fi.pa165.model.data.Actor;
import cz.muni.fi.pa165.model.data.Director;
import cz.muni.fi.pa165.model.data.Genre;
import cz.muni.fi.pa165.model.data.Movie;
import cz.muni.fi.pa165.rest.RecommendationRestController;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatusCode;

import java.util.Optional;
import java.util.UUID;

import static com.github.tomakehurst.wiremock.client.WireMock.*;

@SpringBootTest(classes = App.class)
public class RecommendationRestTest {

    @Autowired
    private RecommendationRestController controller;
    @Autowired
    ObjectMapper objectMapper;

    WireMockServer movieService = new WireMockServer(8089);
    WireMockServer ratingService = new WireMockServer(8087);
    UUID uuid = UUID.randomUUID();
    UUID uuid2 = UUID.randomUUID();

    @BeforeEach
    void setupMocks() throws JsonProcessingException {
        movieService.start();
        ratingService.start();
        var movie = new Movie();
        var director = new Director();
        var actor = new Actor();
        var genre = new Genre();

        director.setId(1L);
        actor.setId(1L);
        genre.setId(1L);

        movie.setDirector(director);
        movie.setActors(new Actor[] {actor});
        movie.setGenres(new Genre[] {genre});
        movie.setId(1L);

        mapMovie(movie);
        mapDirectors(1, movie);
        mapActors(1);
        mapGenres(1);
        mapRatings();
    }

    @AfterEach
    void stopMocks() {
        movieService.stop();
        ratingService.stop();
    }

    @Test
    void simpleRecommendationTest() {
        var response = controller.getLikedMovies(1L, Optional.of(1), Optional.of(1));
        Assertions.assertEquals(HttpStatusCode.valueOf(200), response.getStatusCode());
    }

    @Test
    void recommendationFailTest() {
        Assertions.assertThrows(RatingServiceException.class, ()-> {
            controller.getLikedMovies(60L, Optional.of(1), Optional.of(1));
        });
    }

    @Test
    void simpleRecommendationSimilarTest() {
        var response = controller.getSimilarMovies(1L, Optional.of(1), Optional.of(1));
        Assertions.assertEquals(HttpStatusCode.valueOf(200), response.getStatusCode());
    }

    @Test
    void recommendationSimilarFailTest() {
        Assertions.assertThrows(MovieServiceException.class, ()-> {
            controller.getSimilarMovies(60L, Optional.of(1), Optional.of(1));
        });
    }

    @Test
    void responseSimilarTest() {
        var response = controller.getSimilarMovies(1L, Optional.of(1), Optional.of(1));
        Assertions.assertEquals(HttpStatusCode.valueOf(200), response.getStatusCode());
        Assertions.assertNotNull(response.getBody());
        Assertions.assertEquals(1, response.getBody().size());
        var movie = response.getBody().get(0);
        Assertions.assertEquals(2, movie.getId());
    }

    @Test
    void responseLikedTest() {
        var response = controller.getLikedMovies(1L, Optional.of(1), Optional.of(1));
        Assertions.assertEquals(HttpStatusCode.valueOf(200), response.getStatusCode());
        Assertions.assertNotNull(response.getBody());
        Assertions.assertEquals(1, response.getBody().size());
        var movie = response.getBody().get(0);
        Assertions.assertEquals(3, movie.getId());
    }

    @Test
    void responseSimilarPagingTest() {
        var response = controller.getSimilarMovies(1L, Optional.of(1), Optional.of(2));
        Assertions.assertEquals(HttpStatusCode.valueOf(200), response.getStatusCode());
        Assertions.assertNotNull(response.getBody());
        Assertions.assertEquals(1, response.getBody().size());
        var movie = response.getBody().get(0);
        Assertions.assertEquals(3, movie.getId());
    }

    @Test
    void responsePagingLikedTest() {
        var response = controller.getLikedMovies(1L, Optional.of(2), Optional.of(2));
        Assertions.assertEquals(HttpStatusCode.valueOf(200), response.getStatusCode());
        Assertions.assertNotNull(response.getBody());
        Assertions.assertEquals(2, response.getBody().size());
        var movie = response.getBody().get(0);
        var movie2 = response.getBody().get(1);
        Assertions.assertEquals(2, movie.getId());
        Assertions.assertEquals(3, movie2.getId());
    }

    private void mapRatings() throws JsonProcessingException {
        var dto = new RatingDto();
        dto.setUser_id(uuid);
        dto.setValue(1);
        dto.setCriterion_id(1);
        dto.setMovie_id(1);
        var dto2 = new RatingDto();
        dto2.setUser_id(uuid);
        dto2.setValue(9);
        dto2.setCriterion_id(2);
        dto2.setMovie_id(1);
        RatingDto[] c = new RatingDto[] {dto, dto2};
        String resp = objectMapper.writeValueAsString(c);
        ratingService.stubFor(get(urlEqualTo("/api/rating?userId="+uuid))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withBody(resp)));

        var dto3 = new RatingDto();
        dto3.setUser_id(uuid2);
        dto3.setValue(1);
        dto3.setCriterion_id(1);
        dto3.setMovie_id(1);
        RatingDto[] input2 = new RatingDto[] {dto, dto2, dto3};
        String resp2 = objectMapper.writeValueAsString(input2);
        ratingService.stubFor(get(urlEqualTo("/api/rating?userId=&movieId=1"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withBody(resp2)));

        var dto4 = new RatingDto();
        dto4.setUser_id(uuid2);
        dto4.setValue(10);
        dto4.setCriterion_id(1);
        dto4.setMovie_id(2);
        var dto5 = new RatingDto();
        dto5.setUser_id(uuid2);
        dto5.setValue(8);
        dto5.setCriterion_id(2);
        dto5.setMovie_id(3);
        RatingDto[] input3 = new RatingDto[] {dto3, dto4, dto5};
        String resp3 = objectMapper.writeValueAsString(input3);
        ratingService.stubFor(get(urlEqualTo("/api/rating?userId="+uuid2))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withBody(resp3)));

    }

    private void mapGenres(int i) throws JsonProcessingException {
        var dto = new MovieConstrainedResponseDto();
        dto.setMessage("Success");

        var director = new DirectorDto();
        director.setId(60L);
        var actor = new ActorDto();
        actor.setId(80L);
        var genre = new GenreDto();
        genre.setId((long) i);

        var movieDto = new MovieDto();
        movieDto.setId(2L);
        movieDto.setDirector(director);
        movieDto.setActors(new ActorDto[] {actor});
        movieDto.setGenres(new GenreDto[] {genre});
        MovieDto[] dtoArray;
        dtoArray = new MovieDto[1];
        dtoArray[0] = movieDto;

        dto.setContent(dtoArray);
        String resp = objectMapper.writeValueAsString(dto);
        movieService.stubFor(get(urlEqualTo("/api/movies?genreId=1&directorId=&actorId="))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withBody(resp)));
    }

    private void mapActors(int i) throws JsonProcessingException {
        var dto = new MovieConstrainedResponseDto();
        dto.setMessage("Success");

        var director = new DirectorDto();
        director.setId(60L);
        var actor = new ActorDto();
        actor.setId((long) i);
        var genre = new GenreDto();
        genre.setId(40L);

        var movieDto = new MovieDto();
        movieDto.setId(3L);
        movieDto.setDirector(director);
        movieDto.setActors(new ActorDto[] {actor});
        movieDto.setGenres(new GenreDto[] {genre});

        dto.setContent(new MovieDto[] {movieDto});
        String resp = objectMapper.writeValueAsString(dto);
        movieService.stubFor(get(urlEqualTo("/api/movies?genreId=&directorId=&actorId=1"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withBody(resp)));
    }

    private void mapMovie(Movie movie) throws JsonProcessingException {
        var mapper = new ModelMapper();
        var dto = mapper.map(movie, MovieDto.class);
        var response = new MovieResponseDto();
        response.setMessage("Success");
        response.setItem(dto);

        String resp = objectMapper.writeValueAsString(response);
        movieService.stubFor(get(urlEqualTo("/api/movies/1"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withBody(resp)));

        var rankedFilm = new MovieDto();
        rankedFilm.setId(2L);
        var response2 = new MovieResponseDto();
        response2.setMessage("Success");
        response2.setItem(rankedFilm);

        String resp2 = objectMapper.writeValueAsString(response2);
        movieService.stubFor(get(urlEqualTo("/api/movies/2"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withBody(resp2)));

        var rankedFilm3 = new MovieDto();
        rankedFilm3.setId(3L);
        var response3 = new MovieResponseDto();
        response3.setMessage("Success");
        response3.setItem(rankedFilm3);

        String resp3 = objectMapper.writeValueAsString(response3);
        movieService.stubFor(get(urlEqualTo("/api/movies/3"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withBody(resp3)));
    }

    private void mapDirectors(int directorId, Movie movie) throws JsonProcessingException {
        var dto = new MovieConstrainedResponseDto();
        dto.setMessage("Success");

        var director = new DirectorDto();
        director.setId((long) directorId);
        var actor = new ActorDto();
        actor.setId(30L);
        var genre = new GenreDto();
        genre.setId(40L);

        var movieDto = new MovieDto();
        movieDto.setId(4L);
        movieDto.setDirector(director);
        movieDto.setActors(new ActorDto[] {actor});
        movieDto.setGenres(new GenreDto[] {genre});

        var mapper = new ModelMapper();
        var movieDto2 = mapper.map(movie, MovieDto.class);

        dto.setContent(new MovieDto[] {movieDto, movieDto2});
        String resp = objectMapper.writeValueAsString(dto);
        movieService.stubFor(get(urlEqualTo("/api/movies?genreId=&directorId=1&actorId="))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withBody(resp)));
    }

}
