package integration.cz.muni.fi.pa165;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.WireMockServer;
import cz.muni.fi.pa165.App;
import cz.muni.fi.pa165.dto.*;
import cz.muni.fi.pa165.exceptions.MovieServiceException;
import cz.muni.fi.pa165.model.data.Actor;
import cz.muni.fi.pa165.model.data.Director;
import cz.muni.fi.pa165.model.data.Genre;
import cz.muni.fi.pa165.model.data.Movie;
import cz.muni.fi.pa165.service.MoviePointAccessPoint;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static com.github.tomakehurst.wiremock.client.WireMock.*;

@SpringBootTest(classes = App.class)
public class MovieAccessPointTest {
    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    MoviePointAccessPoint point;

    WireMockServer wireMockServer = new WireMockServer( 8089);

    @BeforeEach
    void setupMockAuthServer() {
        wireMockServer.start();
    }

    @AfterEach
    void stop() {
        wireMockServer.stop();
    }

    @Test
    void getMovieByIdUnsuccessfulTest() throws JsonProcessingException {
        var dto = new MovieResponseDto();
        dto.setMessage("Definitely not successful response message");
        String resp = objectMapper.writeValueAsString(dto);
        wireMockServer.stubFor(get(urlEqualTo("/api/movies/1"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withBody(resp)));

        Assertions.assertThrows(MovieServiceException.class,() -> {
            point.getMovie(1L);
        });
    }

    @Test
    void getMovieByIdTest() throws JsonProcessingException {
        var dto = new MovieResponseDto();
        dto.setMessage("Success");
        var movieDto = new MovieDto();
        movieDto.setId(2L);
        dto.setItem(movieDto);
        String resp = objectMapper.writeValueAsString(dto);
        wireMockServer.stubFor(get(urlEqualTo("/api/movies/2"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withBody(resp)));

        var output = point.getMovie(2);

        Assertions.assertEquals(2L, output.getId());
    }

    @Test
    void getUnrankedMoviesTest() throws JsonProcessingException {
        var movie = new Movie();
        var director = new Director();
        var actor = new Actor();
        var genre = new Genre();

        director.setId(1L);
        actor.setId(1L);
        genre.setId(1L);

        movie.setDirector(director);
        movie.setActors(new Actor[] {actor});
        movie.setGenres(new Genre[] {genre});
        movie.setId(1L);

        mapMovie(movie);
        mapDirectors(1, movie);
        mapActors(1);
        mapGenres(1);

        var unrankedMovies = point.getUnrankedMovies(movie);
        Assertions.assertNotNull(unrankedMovies);
        Assertions.assertEquals(4, unrankedMovies.size());
    }

    private void mapGenres(int i) throws JsonProcessingException {
        var dto = new MovieConstrainedResponseDto();
        dto.setMessage("Success");

        var director = new DirectorDto();
        director.setId(60L);
        var actor = new ActorDto();
        actor.setId(80L);
        var genre = new GenreDto();
        genre.setId((long) i);

        var movieDto = new MovieDto();
        movieDto.setId(2L);
        movieDto.setDirector(director);
        movieDto.setActors(new ActorDto[] {actor});
        movieDto.setGenres(new GenreDto[] {genre});
        MovieDto[] dtoArray;
        dtoArray = new MovieDto[1];
        dtoArray[0] = movieDto;

        dto.setContent(dtoArray);
        String resp = objectMapper.writeValueAsString(dto);
        wireMockServer.stubFor(get(urlEqualTo("/api/movies?genreId=1&directorId=&actorId="))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withBody(resp)));
    }

    private void mapActors(int i) throws JsonProcessingException {
        var dto = new MovieConstrainedResponseDto();
        dto.setMessage("Success");

        var director = new DirectorDto();
        director.setId(60L);
        var actor = new ActorDto();
        actor.setId((long) i);
        var genre = new GenreDto();
        genre.setId(40L);

        var movieDto = new MovieDto();
        movieDto.setId(3L);
        movieDto.setDirector(director);
        movieDto.setActors(new ActorDto[] {actor});
        movieDto.setGenres(new GenreDto[] {genre});

        dto.setContent(new MovieDto[] {movieDto});
        String resp = objectMapper.writeValueAsString(dto);
        wireMockServer.stubFor(get(urlEqualTo("/api/movies?genreId=&directorId=&actorId=1"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withBody(resp)));
    }

    private void mapMovie(Movie movie) throws JsonProcessingException {
        var mapper = new ModelMapper();
        var dto = mapper.map(movie, MovieDto.class);
        var response = new MovieResponseDto();
        response.setMessage("Success");
        response.setItem(dto);

        String resp = objectMapper.writeValueAsString(response);
        wireMockServer.stubFor(get(urlEqualTo("/api/movies/1"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withBody(resp)));
    }

    private void mapDirectors(int directorId, Movie movie) throws JsonProcessingException {
        var dto = new MovieConstrainedResponseDto();
        dto.setMessage("Success");

        var director = new DirectorDto();
        director.setId((long) directorId);
        var actor = new ActorDto();
        actor.setId(30L);
        var genre = new GenreDto();
        genre.setId(40L);

        var movieDto = new MovieDto();
        movieDto.setId(4L);
        movieDto.setDirector(director);
        movieDto.setActors(new ActorDto[] {actor});
        movieDto.setGenres(new GenreDto[] {genre});

        var mapper = new ModelMapper();
        var movieDto2 = mapper.map(movie, MovieDto.class);

        dto.setContent(new MovieDto[] {movieDto, movieDto2});
        String resp = objectMapper.writeValueAsString(dto);
        wireMockServer.stubFor(get(urlEqualTo("/api/movies?genreId=&directorId=1&actorId="))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withBody(resp)));
    }
}
