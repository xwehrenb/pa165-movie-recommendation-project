package integration.cz.muni.fi.pa165;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.WireMockServer;
import cz.muni.fi.pa165.App;
import cz.muni.fi.pa165.dto.RatingDto;
import cz.muni.fi.pa165.exceptions.RatingServiceException;
import cz.muni.fi.pa165.service.RatingPointAccessPoint;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashSet;
import java.util.Objects;
import java.util.Random;
import java.util.UUID;

import static com.github.tomakehurst.wiremock.client.WireMock.*;


@SpringBootTest(classes = App.class)
public class RatingAccessPointTest {
    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    RatingPointAccessPoint point;

    WireMockServer wireMockServer = new WireMockServer( 8087);
    UUID uuid1 = UUID.randomUUID();
    UUID uuid2 = UUID.randomUUID();


    @BeforeEach
    void setupMockAuthServer() throws JsonProcessingException {
        wireMockServer.start();
    }

    @AfterEach
    void stop() {
        wireMockServer.stop();
    }

    @Test
    void getRatingsByUserIdTest() throws JsonProcessingException {
        var dto = new RatingDto();
        dto.setUser_id(uuid1);
        dto.setValue(1);
        dto.setCriterion_id(1);
        dto.setMovie_id(1);
        var dto2 = new RatingDto();
        dto2.setUser_id(uuid1);
        dto2.setValue(9);
        dto2.setCriterion_id(2);
        dto2.setMovie_id(1);
        RatingDto[] c = new RatingDto[] {dto, dto2};
        String resp = objectMapper.writeValueAsString(c);
        wireMockServer.stubFor(get(urlEqualTo("/api/rating?userId="+uuid1))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withBody(resp)));


        var output = point.getRatingsByUserId(uuid1);
        Assertions.assertEquals(2, output.size());
        var firstRate = output.get(0);
        Assertions.assertNotNull(firstRate);
    }

    @Test
    void unavailableServerTest1() {
        wireMockServer.stop();
        Assertions.assertThrows(RatingServiceException.class , () -> {
            point.getRatingsByUserId(UUID.randomUUID());
        });
    }


    @Test
    void unavailableServerTest3() {
        wireMockServer.stop();
        var hashSet = new HashSet<UUID>();
        hashSet.add(UUID.randomUUID());
        hashSet.add(UUID.randomUUID());
        Assertions.assertThrows(RatingServiceException.class , () -> {
            point.getRatingsByUserIds(hashSet);
        });
    }

    @Test
    void getRatingsByMovieIdTest() throws JsonProcessingException {
        var uuid = UUID.randomUUID();
        var dto = new RatingDto();
        dto.setUser_id(uuid);
        dto.setValue(1);
        dto.setCriterion_id(1);
        dto.setMovie_id(1);
        var dto2 = new RatingDto();
        dto2.setUser_id(uuid);
        dto2.setValue(9);
        dto2.setCriterion_id(2);
        dto2.setMovie_id(1);
        RatingDto[] c = new RatingDto[] {dto, dto2};
        String resp = objectMapper.writeValueAsString(c);
        wireMockServer.stubFor(get(urlEqualTo("/api/rating?userId=&movieId=1"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withBody(resp)));

        var output = point.getRatingsByMovieId(1);
        Assertions.assertEquals(2, output.size());
        var firstRate = output.get(0);
        Assertions.assertNotNull(firstRate);
        Assertions.assertEquals( 1, firstRate.getMovie_id());
        var secondRate = output.get(1);
        Assertions.assertNotNull(secondRate);
        Assertions.assertEquals(1, secondRate.getMovie_id());
    }

    @Test
    void getRatingsByUserSetTest() throws JsonProcessingException {
        var dto = new RatingDto();
        dto.setUser_id(uuid1);
        dto.setValue(1);
        dto.setCriterion_id(1);
        dto.setMovie_id(1);
        var dto2 = new RatingDto();
        dto2.setUser_id(uuid2);
        dto2.setValue(9);
        dto2.setCriterion_id(2);
        dto2.setMovie_id(1);

        RatingDto[] userOne = new RatingDto[] {dto};
        RatingDto[] userTwo = new RatingDto[] {dto2};

        String resp1 = objectMapper.writeValueAsString(userOne);
        wireMockServer.stubFor(get(urlEqualTo("/api/rating?userId="+uuid1))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withBody(resp1)));

        String resp2 = objectMapper.writeValueAsString(userTwo);
        wireMockServer.stubFor(get(urlEqualTo("/api/rating?userId="+uuid2))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withBody(resp2)));

        HashSet<UUID> users = new HashSet<>();
        users.add(uuid1);
        users.add(uuid2);
        var output = point.getRatingsByUserIds(users);
        Assertions.assertEquals(2, output.size());
        var firstRate = output.get(0);

        Assertions.assertTrue(firstRate.getUser_id().equals(uuid1) || firstRate.getUser_id().equals(uuid2));
        var secondRate = output.get(1);
        Assertions.assertTrue(secondRate.getUser_id().equals(uuid1) || secondRate.getUser_id().equals(uuid2));
    }


}
