package cz.muni.fi.pa165.dto;

import java.util.UUID;

public class RatingDto {
    private int value;
    private int criterion_id;
    private int movie_id;
    private UUID user_id;

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getCriterion_id() {
        return criterion_id;
    }

    public void setCriterion_id(int criterion_id) {
        this.criterion_id = criterion_id;
    }

    public int getMovie_id() {
        return movie_id;
    }

    public void setMovie_id(int movie_id) {
        this.movie_id = movie_id;
    }

    public UUID getUser_id() {
        return user_id;
    }

    public void setUser_id(UUID user_id) {
        this.user_id = user_id;
    }
}
