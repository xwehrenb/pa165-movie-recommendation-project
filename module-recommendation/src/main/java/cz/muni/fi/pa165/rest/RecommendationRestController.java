package cz.muni.fi.pa165.rest;

import cz.muni.fi.pa165.dto.MovieOutDto;
import cz.muni.fi.pa165.facade.ErrorResponse;
import cz.muni.fi.pa165.facade.RecommendationFacade;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.servers.Server;
import io.swagger.v3.oas.annotations.servers.ServerVariable;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@RestController
@OpenAPIDefinition(
        info = @Info( title = "Movie recommendation service", version = "1.0",
                description = "Service for recommending movies based on their similarity.",
                contact = @Contact( name = "Vojtěch Malý", email = "493319@mail.muni.cz"),
                license = @License( name = "Apache 2.0", url = "https://www.apache.org/licenses/LICENSE-2.0.html")
        ),
        servers = @Server( description = "Localhost server", url = "{scheme}://{server}:{port}",
                variables = {
                        @ServerVariable( name = "scheme", allowableValues = {"http", "https"}, defaultValue = "http"),
                        @ServerVariable( name = "server", defaultValue = "localhost"),
                        @ServerVariable( name = "port", defaultValue = "8085"),
                }
        )
)
@RequestMapping(path = "api/recommend", produces = MediaType.APPLICATION_JSON_VALUE)
public class RecommendationRestController {
    private final RecommendationFacade facade;

    @Autowired
    public RecommendationRestController(RecommendationFacade facade) {
        this.facade = facade;
    }


    @Operation(
            summary = "Get recommendation based on similarity to other movies",
            responses = {
                    @ApiResponse( responseCode = "200",
                            content = @Content( mediaType = "application/json",
                                    array = @ArraySchema(
                                            schema = @Schema(
                                                    implementation = MovieOutDto.class
                                            )
                                    )
                            )
                    ),
                    @ApiResponse( responseCode = "404", description = "Error in accessing other services",
                            content = @Content(
                                    schema = @Schema(
                                            implementation = ErrorResponse.class
                                    )
                            )
                    )
            },
            parameters = {
                    @Parameter(name = "id", description = "Movie ID for which similar movies are returned", example = "1"),
                    @Parameter(name = "size", description = "Number of movies on one page", example = "10"),
                    @Parameter(name = "page", description = "Number of page", example = "1")
            },
            tags = {"Recommendation"}
    )
    @GetMapping(path = "similar/", params = {"id"})
    public ResponseEntity<List<MovieOutDto>> getSimilarMovies(
            @RequestParam(name = "id") Long id,
            @RequestParam(name = "size", defaultValue = "20") Optional<Integer> size,
            @RequestParam(name = "page", defaultValue = "1") Optional<Integer> page
    ) {
        var sim = facade.similarMovies(id);
        var mapper = new ModelMapper();
        var answer = mapper.map(sim.toArray(), MovieOutDto[].class);

        return respond(Arrays.asList(answer), size, page);
    }

    @Operation(
            summary = "Get recommendation based on liked movies",
            responses = {
                    @ApiResponse(responseCode = "200",
                            content = @Content( mediaType = "application/json",
                                    array = @ArraySchema(
                                            schema = @Schema(
                                                    implementation = MovieOutDto.class
                                            )
                                    )
                            )
                    ),
                    @ApiResponse(responseCode = "404", description = "Error in accessing other services",
                            content = @Content(
                                    schema = @Schema(
                                            implementation = ErrorResponse.class
                                    )
                            )
                    )
            },
            parameters = {
                    @Parameter(name = "id", description = "Movie ID for which most liked movies are returned", example = "1"),
                    @Parameter(name = "size", description = "Number of movies on one page", example = "10"),
                    @Parameter(name = "page", description = "Number of page", example = "1")
            },
            tags = {"Recommendation"}
    )
    @GetMapping(path = "liked/", params = {"id"})
    public ResponseEntity<List<MovieOutDto>> getLikedMovies(
            @RequestParam(name = "id") Long id,
            @RequestParam(name = "size", defaultValue = "20") Optional<Integer> size,
            @RequestParam(name = "page", defaultValue = "1") Optional<Integer> page
    ) {
        var liked = facade.likedMovies(id, page, size);
        var mapper = new ModelMapper();
        var answer = mapper.map(liked.toArray(), MovieOutDto[].class);

        return respond(Arrays.asList(answer), size, page);
    }

    private ResponseEntity<List<MovieOutDto>> respond(List<MovieOutDto> movies, Optional<Integer> size, Optional<Integer> page) {
        if (size.isEmpty() || page.isEmpty()) {
            return ResponseEntity.ok(movies);
        }

        PagedListHolder<MovieOutDto> listHolder = new PagedListHolder<>(movies);
        listHolder.setPageSize(size.get());
        listHolder.setPage(page.get());

        return ResponseEntity.ok(listHolder.getPageList());
    }
}
