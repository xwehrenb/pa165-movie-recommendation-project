package cz.muni.fi.pa165.model.repository;

import cz.muni.fi.pa165.model.data.Rating;

import java.util.HashSet;
import java.util.List;
import java.util.UUID;

public interface RatingPointService {

    /**
     * Returns ratings for given movie
     * @param id id of movie
     * @return rating list
     */
    List<Rating> getRatingsByMovieId(long id);

    /**
     * Returns ratings for given user
     * @param id id of user
     * @return rating list
     */
    List<Rating> getRatingsByUserId(UUID id);

    /**
     * Returns ratings for given users
     * @param users set of user ids
     * @return rating list
     */
    List<Rating> getRatingsByUserIds(HashSet<UUID> users);
}
