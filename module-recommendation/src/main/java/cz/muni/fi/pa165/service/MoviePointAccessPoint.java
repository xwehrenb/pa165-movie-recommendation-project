package cz.muni.fi.pa165.service;

import cz.muni.fi.pa165.config.AdditionalConfiguration;
import cz.muni.fi.pa165.dto.MovieCriteriaResponseDto;
import cz.muni.fi.pa165.dto.MovieResponseDto;
import cz.muni.fi.pa165.exceptions.MovieServiceException;
import cz.muni.fi.pa165.model.data.Movie;
import cz.muni.fi.pa165.model.repository.MoviePointService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.*;

@Service
public class MoviePointAccessPoint implements MoviePointService {

    @Autowired
    private AdditionalConfiguration movieConfig;

    public Movie getMovie(long id) throws MovieServiceException {
        var restTemplate = new RestTemplate();
        var url = movieConfig.getAccessPointMovieConfig().getUrl();
        var port = movieConfig.getAccessPointMovieConfig().getPort();
        var path = movieConfig.getAccessPointMovieConfig().getPath();

        StringBuilder sb = new StringBuilder();
        sb.append(url).append(":").append(port).append(path).append("/").append(id);

        MovieResponseDto response;
        try {
            response = restTemplate.getForObject(sb.toString(), MovieResponseDto.class);
        } catch (Exception e) {
            throw new MovieServiceException("Movie microservice is not available.");
        }

        if (response == null) {
            throw new MovieServiceException("Movie microservice returned empty body");
        }

        if (!Objects.equals(response.getMessage(), "Success")) {
            throw new MovieServiceException("Movie microservice returned invalid response.");
        }

        var mapper = new ModelMapper();

        return mapper.map(response.getItem(), Movie.class);
    }

    private HashMap<Long, Movie> getMoviesWithSameGenres(Movie movie) {
        var possibleMovies = new HashMap<Long, Movie>();

        for (var genre : movie.getGenres()) {
            var movies = getMoviesByCriteria(genre.getId().describeConstable(), Optional.empty(), Optional.empty());

            for (var movie1 : movies) {
                possibleMovies.putIfAbsent(movie1.getId(), movie1);
            }
        }

        return possibleMovies;
    }

    private HashMap<Long, Movie> getMoviesWithSameDirector(Movie movie) {
        var possibleMovies = new HashMap<Long, Movie>();
        var movies = getMoviesByCriteria(Optional.empty(), movie.getDirector().getId().describeConstable(), Optional.empty());

        for (var movie1 : movies) {
            possibleMovies.putIfAbsent(movie1.getId(), movie1);
        }

        return possibleMovies;
    }

    private HashMap<Long, Movie> getMoviesWithSameActors(Movie movie) {
        var possibleMovies = new HashMap<Long, Movie>();

        for (var actor : movie.getActors()) {
            var movies = getMoviesByCriteria(Optional.empty(), Optional.empty(), actor.getId().describeConstable());

            for (var movie1 : movies) {
                possibleMovies.putIfAbsent(movie1.getId(), movie1);
            }

        }

        return possibleMovies;
    }

    public HashMap<Long, Movie> getUnrankedMovies(Movie movie) {
        var genreMovie = getMoviesWithSameGenres(movie);
        var directorMovie = getMoviesWithSameDirector(movie);
        var actorMovie = getMoviesWithSameActors(movie);
        var possibleMovies = new HashMap<Long, Movie>();

        genreMovie.forEach(possibleMovies::putIfAbsent);
        directorMovie.forEach(possibleMovies::putIfAbsent);
        actorMovie.forEach(possibleMovies::putIfAbsent);

        return possibleMovies;
    }
    private List<Movie> getMoviesByCriteria(Optional<Long> genreId, Optional<Long> directorId, Optional<Long> actorId) throws MovieServiceException {
        var restTemplate = new RestTemplate();
        var url = movieConfig.getAccessPointMovieConfig().getUrl();
        var port = movieConfig.getAccessPointMovieConfig().getPort();
        var path = movieConfig.getAccessPointMovieConfig().getPath();

        StringBuilder pathBuilder = new StringBuilder();

        pathBuilder.append(url).append(":").append(port).append(path).append("?");

        pathBuilder.append("genreId=");
        genreId.ifPresent(pathBuilder::append);
        pathBuilder.append("&directorId=");
        directorId.ifPresent(pathBuilder::append);
        pathBuilder.append("&actorId=");
        actorId.ifPresent(pathBuilder::append);

        MovieCriteriaResponseDto movieList;

        try {
            movieList = restTemplate.getForObject(pathBuilder.toString(), MovieCriteriaResponseDto.class);
        } catch (Exception e) {
            throw new MovieServiceException("Movie microservice is not available");
        }

        if (movieList == null) {
            throw new MovieServiceException("Movie microservice returned empty body");
        }

        if (!Objects.equals(movieList.getMessage(), "Success")) {
            throw new MovieServiceException("Movie microservice returned invalid response.");
        }

        var mapper = new ModelMapper();

        return Arrays.stream(movieList.getContent()).map(movieDto -> mapper.map(movieDto, Movie.class)).toList();
    }
}
