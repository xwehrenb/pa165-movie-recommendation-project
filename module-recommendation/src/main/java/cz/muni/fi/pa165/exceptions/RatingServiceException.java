package cz.muni.fi.pa165.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Rating not found")
public class RatingServiceException extends RuntimeException {
    public RatingServiceException(String message) {
        super(message);
    }
}
