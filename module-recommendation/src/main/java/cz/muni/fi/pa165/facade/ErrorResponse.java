package cz.muni.fi.pa165.facade;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema(
        title = "Error response",
        description = "Error response"
)
public record ErrorResponse(
        @Schema(
                format = "date-time",
                description = "time in ISO format",
                example = "2022-12-21T18:52:10.757+00:00"
        )
        String timestamp,

        @Schema(
                description = "HTTP status code",
                example = "404"
        )
        int status,

        @Schema(
                description = "HTTP status text",
                example = "Not Found"
        )
        String error,

        @Schema(
                description = "reason for error",
                example = "entity not found"
        )
        String message,

        @Schema(
                description = "URL path",
                example = "/api/message/1"
        )
        String path
){

}
