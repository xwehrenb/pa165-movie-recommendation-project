package cz.muni.fi.pa165.dto;

import jakarta.validation.constraints.NotNull;
import jdk.jfr.Description;

import java.io.Serializable;

public class MovieDto implements Serializable {
    @NotNull
    @Description("Movie ID")
    private Long id;
    private String title;
    private DirectorDto director;
    private GenreDto[] genreDtos;
    private ActorDto[] actors;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public DirectorDto getDirector() {
        return director;
    }

    public void setDirector(DirectorDto director) {
        this.director = director;
    }

    public GenreDto[] getGenres() {
        return genreDtos;
    }

    public void setGenres(GenreDto[] genreDtos) {
        this.genreDtos = genreDtos;
    }

    public ActorDto[] getActors() {
        return actors;
    }

    public void setActors(ActorDto[] actors) {
        this.actors = actors;
    }

}
