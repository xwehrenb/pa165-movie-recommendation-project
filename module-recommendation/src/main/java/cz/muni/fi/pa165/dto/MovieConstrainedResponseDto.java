package cz.muni.fi.pa165.dto;

public class MovieConstrainedResponseDto {
    public MovieDto[] getContent() {
        return content;
    }

    public void setContent(MovieDto[] content) {
        this.content = content;
    }

    private MovieDto[] content;
    private String message;


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
