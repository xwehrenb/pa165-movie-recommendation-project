package cz.muni.fi.pa165.model.repository;


import cz.muni.fi.pa165.model.data.Movie;

import java.util.HashMap;

public interface MoviePointService {
    /**
     * Returns movie with given id
     * @param id id of movie
     * @return movie with given id
     */
    Movie getMovie(long id);

    /**
     * Returns movies with same genres, directors or actors as given movie
     * @param movie movie
     * @return movies with same genres, directors or actors as given movie
     */
    HashMap<Long, Movie> getUnrankedMovies(Movie movie);
}
