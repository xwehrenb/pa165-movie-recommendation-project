package cz.muni.fi.pa165.dto;

public class MovieCriteriaResponseDto {
    private MovieDto[] content;
    private String message;


    public MovieDto[] getContent() {
        return content;
    }

    public void setContent(MovieDto[] content) {
        this.content = content;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
