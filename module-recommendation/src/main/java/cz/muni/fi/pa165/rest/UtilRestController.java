package cz.muni.fi.pa165.rest;

import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class UtilRestController {

        @Operation(
                summary = "Check if service is running",
                tags = {"Healthcheck"}
        )
        @ResponseStatus(HttpStatus.OK)
        @GetMapping("/healthcheck")
        public String all() {
            return "\"OK\"";
        }
}
