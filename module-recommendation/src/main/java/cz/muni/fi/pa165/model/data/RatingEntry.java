package cz.muni.fi.pa165.model.data;

import java.util.HashSet;
import java.util.UUID;

public class RatingEntry {
    private int id;
    private HashSet<UUID> users;
    private double average;

    public RatingEntry(int id, Integer initialAverage) {
        this.id = id;
        users = new HashSet<>();
        average = initialAverage.doubleValue();
    }

    public int getId() {
        return id;
    }

    public HashSet<UUID> getUsers() {
        return users;
    }

    public double getAverage() {
        return average;
    }

    public void addRating(Integer rating) {
        average = (average + rating.doubleValue()) / 2;
    }

    public double getFinalScore() {
        return average * users.size();
    }
}
