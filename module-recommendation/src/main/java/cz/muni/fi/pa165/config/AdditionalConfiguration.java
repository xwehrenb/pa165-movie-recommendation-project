package cz.muni.fi.pa165.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
@EnableConfigurationProperties({AccessPointRatingConfig.class, AccessPointMovieConfig.class})
public class AdditionalConfiguration {
    private AccessPointRatingConfig accessPointsConfig;
    private AccessPointMovieConfig accessPointMovieConfig;

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Autowired
    public AdditionalConfiguration(AccessPointMovieConfig accessPointMovieConfig, AccessPointRatingConfig accessPointsConfig) {
        this.accessPointMovieConfig = accessPointMovieConfig;
        this.accessPointsConfig = accessPointsConfig;
    }

    public AccessPointRatingConfig getAccessPointsRatingConfig() {
        return accessPointsConfig;
    }

    public void setAccessPointsConfig(AccessPointRatingConfig accessPointsConfig) {
        this.accessPointsConfig = accessPointsConfig;
    }

    public AccessPointMovieConfig getAccessPointMovieConfig() {
        return accessPointMovieConfig;
    }

    public void setAccessPointMovieConfig(AccessPointMovieConfig accessPointMovieConfig) {
        this.accessPointMovieConfig = accessPointMovieConfig;
    }
}
