package cz.muni.fi.pa165.facade;

import cz.muni.fi.pa165.model.data.Movie;
import cz.muni.fi.pa165.service.RecommendationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class RecommendationFacade {
    private final RecommendationService service;

    @Autowired
    public RecommendationFacade(RecommendationService service) {
        this.service = service;
    }

    public List<Movie> similarMovies(Long id) {
        return service.getSimilarMovies(id);
    }

    public List<Movie> likedMovies(Long id, Optional<Integer> page, Optional<Integer> pageSize) {
        return service.likedMovies(id, page, pageSize);
    }
}
