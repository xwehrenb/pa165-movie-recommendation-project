package cz.muni.fi.pa165.service;

import cz.muni.fi.pa165.model.data.Movie;
import cz.muni.fi.pa165.model.data.Rating;
import cz.muni.fi.pa165.model.data.RatingEntry;
import cz.muni.fi.pa165.model.repository.MoviePointService;
import cz.muni.fi.pa165.model.repository.RatingPointService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class RecommendationService {
    private static final  int GENRE_WEIGHT = 2;
    private static final int ACTOR_WEIGHT = 1;
    private static final int DIRECTOR_WEIGHT = 3;

    private MoviePointService moviePointService;
    private RatingPointService ratingService;

    @Autowired
    public RecommendationService(MoviePointService moviePointService, RatingPointService ratingService) {
        this.moviePointService = moviePointService;
        this.ratingService = ratingService;
    }

    public ArrayList<Movie> getSimilarMovies(Long id) {
        var movie = moviePointService.getMovie(id);

        if (movie == null) {
            return new ArrayList<>();
        }

        var possibleMovies = moviePointService.getUnrankedMovies(movie);
        ArrayList<Movie> sortedSimilar = new ArrayList<>();

        rankSimilarity(possibleMovies, movie);

        PriorityQueue<Movie> pq = new PriorityQueue<>(Comparator.comparing(Movie::getSimilarityRank));
        pq.addAll(possibleMovies.values());

        while (!pq.isEmpty()) {
            sortedSimilar.add(0, pq.poll());
        }

        sortedSimilar.remove(0);

        return sortedSimilar;
    }

    private HashSet<UUID> getEligibleUsers(List<Rating> ratings) {
        HashSet<UUID> eligibleUsers = new HashSet<>();

        ratings.forEach(ratingDto -> eligibleUsers.add(ratingDto.getUser_id()));

        return eligibleUsers;
    }

    public void rankSimilarity(HashMap<Long, Movie> movies, Movie movieInput) {
        movies.forEach((aLong, movieDto) -> {
            var similarity = 0;

            for (var genre : movieDto.getGenres()) {
                if (Arrays.stream(movieInput.getGenres())
                        .anyMatch(genreDto -> genreDto.getId().equals(genre.getId()))) {
                    similarity += GENRE_WEIGHT;
                }
            }

            if (movieDto.getDirector().getId().equals(movieInput.getDirector().getId())) {
                similarity += DIRECTOR_WEIGHT;
            }

            for (var actor : movieDto.getActors()) {
                if (Arrays.stream(movieInput.getActors()).anyMatch(actorDto -> actorDto.getId().equals(actor.getId()))) {
                    similarity += ACTOR_WEIGHT;
                }
            }

            movieDto.setSimilarityRank(similarity);
        });
    }

    public ArrayList<RatingEntry> filterMoviesByRating(List<Rating> usersRating, long movieId) {
        HashMap<Integer, RatingEntry> ratings = new HashMap<>();

        for(Rating rate : usersRating) {
            if(ratings.containsKey(rate.getMovie_id())) {
                var ratingEntry = ratings.get(rate.getMovie_id());
                ratingEntry.getUsers().add(rate.getUser_id());
                ratingEntry.addRating(rate.getValue());
            } else {
                var ratingEntry = new RatingEntry(rate.getMovie_id(), rate.getValue());
                ratingEntry.getUsers().add(rate.getUser_id());
                ratings.put(rate.getMovie_id(), ratingEntry);
            }
        }

        PriorityQueue<RatingEntry> pq = new PriorityQueue<>(Comparator.comparing(RatingEntry::getFinalScore));
        pq.addAll(ratings.values());

        ArrayList<RatingEntry> sortedLiked = new ArrayList<>();

        while (!pq.isEmpty()) {
            sortedLiked.add(0, pq.poll());
        }

        sortedLiked.removeIf(ratingEntry -> ratingEntry.getId() == movieId);

        return sortedLiked;
    }

    public List<Movie> likedMovies(Long id, Optional<Integer> page, Optional<Integer> pageSize) {
        var rating = ratingService.getRatingsByMovieId(id);
        var users = getEligibleUsers(rating);
        var usersRating = ratingService.getRatingsByUserIds(users);
        var sortedRatings = filterMoviesByRating(usersRating, id);

        var movies = new ArrayList<Movie>();

        if (pageSize.isPresent() && page.isPresent()) {
            PagedListHolder<RatingEntry> listHolder = new PagedListHolder<>(sortedRatings);
            listHolder.setPageSize(pageSize.get());
            listHolder.setPage(page.get());

            sortedRatings = new ArrayList<>(listHolder.getPageList());
        }

        sortedRatings.forEach(ratingEntry -> {
            var movie = moviePointService.getMovie(ratingEntry.getId());

            movie.setUserRatingScore(ratingEntry.getFinalScore());
            movies.add(movie);
        });

        return movies;
    }
}
