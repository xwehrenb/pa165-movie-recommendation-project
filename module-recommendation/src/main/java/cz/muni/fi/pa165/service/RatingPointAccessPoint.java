package cz.muni.fi.pa165.service;

import cz.muni.fi.pa165.config.AdditionalConfiguration;
import cz.muni.fi.pa165.dto.RatingDto;
import cz.muni.fi.pa165.exceptions.RatingServiceException;
import cz.muni.fi.pa165.model.data.Rating;
import cz.muni.fi.pa165.model.repository.RatingPointService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.*;

@Service
public class RatingPointAccessPoint implements RatingPointService {

    @Autowired
    private AdditionalConfiguration ratingConfig;

    @Autowired
    private RestTemplate restTemplate;

    public List<Rating> getRatingsByMovieId(long id) {
        var url = ratingConfig.getAccessPointsRatingConfig().getUrl();
        var port = ratingConfig.getAccessPointsRatingConfig().getPort();
        var path = ratingConfig.getAccessPointsRatingConfig().getPath();

        RatingDto[] rateList;

        StringBuilder pathBuilder = new StringBuilder();
        pathBuilder.append(url).append(":").append(port).append(path).append("?").append("userId=&movieId=").append(id);

        try {
            rateList = restTemplate.getForObject(pathBuilder.toString(), RatingDto[].class);
        } catch (Exception e) {
            throw new RatingServiceException("Rating service is not available");
        }

        var mapper = new ModelMapper();
        if (rateList == null) {
            throw new RatingServiceException("Rating service didn't return valid response");
        }

        return Arrays.stream(rateList).map(ratingDto -> mapper.map(ratingDto, Rating.class)).toList();
    }

    public List<Rating> getRatingsByUserId(UUID id) {
        var url = ratingConfig.getAccessPointsRatingConfig().getUrl();
        var port = ratingConfig.getAccessPointsRatingConfig().getPort();
        var path = ratingConfig.getAccessPointsRatingConfig().getPath();

        StringBuilder pathBuilder = new StringBuilder();
        pathBuilder.append(url).append(":").append(port).append(path).append("?").append("userId=").append(id);

        RatingDto[] rateList;
        try {
            rateList = restTemplate.getForObject(pathBuilder.toString(), RatingDto[].class);
        } catch (Exception e) {
            throw new RatingServiceException("Rating service is not available");
        }

        var mapper = new ModelMapper();

        if (rateList == null){
            throw new RatingServiceException("Rating service didn't return valid response.");
        }

        return Arrays.stream(rateList).map(ratingDto -> mapper.map(ratingDto, Rating.class)).toList();
    }

    public ArrayList<Rating> getRatingsByUserIds(HashSet<UUID> users) {
        ArrayList<Rating> ratings = new ArrayList<>();
        users.forEach(user -> ratings.addAll(getRatingsByUserId(user)));

        return ratings;
    }
}
