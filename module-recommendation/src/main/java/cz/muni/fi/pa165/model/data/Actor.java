package cz.muni.fi.pa165.model.data;

public class Actor {
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
