package cz.muni.fi.pa165.dto;

import jakarta.validation.constraints.NotNull;
import jdk.jfr.Description;

import java.io.Serializable;

public class MovieOutDto implements Serializable {
    @NotNull
    @Description("Movie ID")
    private Long id;

    @NotNull
    @Description("Movie title")
    private String title;

    @Description("User rating score")
    private double userRatingScore;

    @Description("Similarity rank")
    private int similarityRank;

    public double getUserRatingScore() {
        return userRatingScore;
    }

    public void setUserRatingScore(double userRatingScore) {
        this.userRatingScore = userRatingScore;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getSimilarityRank() {
        return similarityRank;
    }

    public void setSimilarityRank(int similarityRank) {
        this.similarityRank = similarityRank;
    }
}
