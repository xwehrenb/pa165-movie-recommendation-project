package cz.muni.fi.pa165.model.data;

public class Movie {

    private Long id;
    private String title;
    private int year;
    private Director director;
    private Genre[] genres;
    private Actor[] actors;
    private Double userRatingScore;
    private int similarityRank = 0;

    public Double getUserRatingScore() {
        return userRatingScore;
    }

    public void setUserRatingScore(Double userRatingScore) {
        this.userRatingScore = userRatingScore;
    }

    public int getSimilarityRank() {
        return similarityRank;
    }

    public void setSimilarityRank(int similarityRank) {
        this.similarityRank = similarityRank;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public Director getDirector() {
        return director;
    }

    public void setDirector(Director director) {
        this.director = director;
    }

    public Genre[] getGenres() {
        return genres;
    }

    public void setGenres(Genre[] genres) {
        this.genres = genres;
    }

    public Actor[] getActors() {
        return actors;
    }

    public void setActors(Actor[] actors) {
        this.actors = actors;
    }
}
