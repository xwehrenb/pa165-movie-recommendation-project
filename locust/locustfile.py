import requests
import itertools
from locust import HttpUser, task, between
from json import JSONDecodeError
from credentials import user_info_list
from random import randint, randrange

credential_iterator = itertools.cycle(user_info_list)

class RegularUser(HttpUser):
    tasks = []
    host = "http://localhost:8080"
    wait_time = between(1, 3)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.info = next(credential_iterator)
        self.host = "http://localhost:8080"
        self.keycloak_service = "http://localhost:8080"
        self.movie_service = "http://localhost:8089"
        self.rating_service = "http://localhost:8087"
        self.recommendation_service = "http://localhost:8085"
        self.movies = []
        self.criteria = []
        self.headers = {}

    def on_start(self):
        self.login()

    def login(self):
        payload = {
            "grant_type": "password",
            "client_id": "movie-app",
            "username": self.info[0],
            "password": "pass",
            "client_secret": "**********"
        }
        self.client.base_url = self.client
        response = self.client.post("http://keycloak:8080/auth/realms/JavaProject/protocol/openid-connect/token", data=payload)
        if response.status_code == 200:
            access_token = response.json()["access_token"]
            self.headers.update({"Authorization": f"Bearer {access_token}"})
        else:
            print(f"Failed to log in as user '{self.info[0]}': {response.text}")

    @task
    def get_random_movies(self):
        self.client.base_url = self.movie_service
        response = self.client.get("http://localhost:8089/api/movies/random?count=20")
        if (response.status_code != 200):
            print("Movie fetch failed")
        else:
            self.movies = response.json()["content"]

    @task
    def get_criterion(self):
        self.client.base_url = self.rating_service
        with self.client.get("http://localhost:8087/api/criterion") as response:
            try:
                if(response.status_code != 200):
                    print("Movie fetch failed")
                self.criteria = response.json()
            except JSONDecodeError:
                print("Json could not be decoded")

    @task
    def rate_movies(self):
        self.client.base_url = self.rating_service
        for movie in self.movies:
            for c in self.criteria:
                crit_id = c["id"]
                movie_id = movie["id"]
                random_rating = randint(1, 10)
                payload = {
                    "value": random_rating,
                    "criterionId": crit_id,
                    "movieId": movie_id
                }
                response = self.client.put("http://localhost:8087/api/rating", json=payload, headers=self.headers)
                # print(response.status_code)
                # print(response.text)
                
    @task
    def get_recommendation(self):
        self.client.base_url = self.recommendation_service
        movie_idx = randint(1, 100) # generate random movie index
        self.client.get("http://localhost:8085/api/recommend/liked/?id=" + str(movie_idx))

    @task
    def get_similar(self):
        self.client.base_url = self.recommendation_service
        movie_id = randint(1, 100)
        self.client.get("http://localhost:8085/api/recommend/similar/?id=" + str(movie_id))

