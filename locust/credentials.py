user_info_list = [
    ("jdoe", "jdoe@example.com", "John", "Doe"),
    ("jsmith", "jsmith@example.com", "Jane", "Smith"),
    ("arogers", "arogers@example.com", "Alice", "Rogers"),
    ("bwilliams", "bwilliams@example.com", "Bob", "Williams"),
    ("ctaylor", "ctaylor@example.com", "Charlie", "Taylor"),
    ("djohnson", "djohnson@example.com", "Diana", "Johnson"),
    ("ewilson", "ewilson@example.com", "Ella", "Wilson"),
    ("fjones", "fjones@example.com", "Frank", "Jones"),
    ("gwhite", "gwhite@example.com", "Grace", "White"),
    ("hmartin", "hmartin@example.com", "Hannah", "Martin")
]