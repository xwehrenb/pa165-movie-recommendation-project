# Test scenario with locust

The goal of this scenario is to showcase nominal usage
of the Movie Recommendation service. The loads are simulated according to
how the service would be used in real production, so they showcase high
get request cadence on recommendation service and also a lot of post requests
on rating service.
This also results in higher number of request to movie service, since recommendation
needs information about movies in detail. 

Rating service is barely seeded before this scenario, simulating real-time addition of
new ratings, therefore changing results of calling /liked endpoint with same movie id. 

## How an ordinary usage for user would look like

- user logs in
- user requests a random set of movies for rating
- user queries the criteria for rating
- user rates each movie in received criteria
- user can request movie similar to a movie they liked
- user can request popular movies

## Setup

First, you need to have Python 3 installed. (It is advised to have a virtual environment set up)

- run `pip install -r requirements.txt` to install requirements
- run `python3 keycloak-user-seed.py` to initialize keycloak with user accounts

After this setup is done, you can run locust using  

```
locust --headless --users 10 --spawn-rate 1
```

It is important to use --headless, because we have a service running on default locust port.

Note: Use `python -m locust` in case `locust` doesn't work.

In order to use the web UI, you need to use

```
locust --web-port 8091 --web-host localhost --users 10 --spawn-rate 1
```

To try higher loads, you can increase the number of users.
