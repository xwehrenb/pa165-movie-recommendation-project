import requests
from credentials import user_info_list

def get_keycloak_access_token(base_url, username, password):
    token_endpoint = f"{base_url}/auth/realms/master/protocol/openid-connect/token"
    payload = {
        "client_id": "admin-cli",
        "username": username,
        "password": password,
        "grant_type": "password"
    }

    response = requests.post(token_endpoint, data=payload)
    if response.status_code == 200:
        return response.json()["access_token"]
    else:
        raise Exception(f"Failed to get access token: {response.text}")


def create_keycloak_user(base_url, realm, access_token, user_info):
    users_endpoint = f"{base_url}/auth/admin/realms/{realm}/users"
    headers = {
        "Content-Type": "application/json",
        "Authorization": f"Bearer {access_token}"
    }

    response = requests.post(users_endpoint, json=user_info, headers=headers)
    if response.status_code == 201:
        print("User created successfully!")
        print(response.content)
        return response.headers["Location"].split("/")[-1]
    else:
        raise Exception(f"Failed to create user: {response.text}")


def assign_role_to_user(base_url, realm, access_token, user_id, role_name):
    role_mapping_endpoint = f"{base_url}/auth/admin/realms/{realm}/users/{user_id}/role-mappings/realm"
    headers = {
        "Content-Type": "application/json",
        "Authorization": f"Bearer {access_token}"
    }

    # Get available realm roles
    response = requests.get(f"{role_mapping_endpoint}/available", headers=headers)
    if response.status_code != 200:
        raise Exception(f"Failed to get available realm roles: {response.text}")

    available_roles = response.json()

    # Find the desired role
    role = next((r for r in available_roles if r["name"] == role_name), None)
    if not role:
        raise Exception(f"Realm role '{role_name}' not found")

    # Assign the role to the user
    response = requests.post(role_mapping_endpoint, json=[role], headers=headers)
    if response.status_code == 204:
        print(f"Realm role '{role_name}' assigned to user!")
    else:
        raise Exception(f"Failed to assign realm role: {response.text}")

def produce_user_info(name: str, email: str, first_name: str, last_name: str):
    return {
        "username": name,
        "email": email,
        "enabled": True,
        "firstName": first_name,
        "lastName": last_name,
        "emailVerified": True,
        "credentials": [
            {
                "type": "password",
                "value": "pass",
                "temporary": False
            }
        ]
    }

def main():
    base_url = "http://localhost:8080"
    realm = "JavaProject"
    admin_username = "admin"
    admin_password = "admin"

    role_name = "app-user"
    access_token = get_keycloak_access_token(base_url, admin_username, admin_password)
    for user_info in user_info_list:
        info = produce_user_info(user_info[0], user_info[1], user_info[2], user_info[3])
        user_id = create_keycloak_user(base_url, realm, access_token, info)
        assign_role_to_user(base_url, realm, access_token, user_id, role_name)


if __name__ == "__main__":
    main()