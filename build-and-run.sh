#!/usr/bin/bash
podman pull docker.io/prom/prometheus
podman pull docker.io/grafana/grafana
podman pull registry.hub.docker.com/library/eclipse-temurin:17-jdk-alpine
mvn clean install
podman-compose down
podman-compose up --build -d